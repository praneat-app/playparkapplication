var tabVideoBool = 0;

var PlayParkMobileAPI = require("/api/PlayParkMobileAPI");
var DateTime = require("/utils/DateTime");
var FormatCurrency = require("utils/FormatCurrency");
var _progressBar = null;
var ProgressBar = require("utils/ProgressBar");
_progressBar = new ProgressBar({
	parentView : $.contentContainer
});

if (Alloy.Globals.OS_IOS) {
	var MAX_TITLE_LENGTH = 80;
} else {
	var MAX_TITLE_LENGTH = Alloy.Globals.MAX_TITLE_LENGTH;
}

var category;
var limit = PlayParkMobileAPI.LIMIT;
var limitLength = 10;
var _isFirstLoadJson = true;

function init() {

	var GoogleAnalytics = require("/api/GoogleAnalytics");
	var googleAnalytics = new GoogleAnalytics();
	googleAnalytics.trackScreen("Video");

	Alloy.Globals.SideMenuButton = Alloy.Globals.ds.SIDEMENU_VIDEO;
	Alloy.Globals.ds.rightButton.setBackgroundImage("/images/video/video_navview_search.png");
	Alloy.Globals.ds.rightButton.setHeight(40);
	Alloy.Globals.ds.rightButton.setWidth(40);

	showProgressBar();
	getContent();

	$.tabVideoGame.addEventListener("click", function(e) {
		tabVideoBool = 0;
		refreshTabMenu();
	});

	$.tabVideoOther.addEventListener("click", function(e) {
		tabVideoBool = 1;
		refreshTabMenu();
	});

	/*Video*/
	$.videoListView.addEventListener("itemclick", function(e) {
		if (category) {
			var object = category[e.itemIndex];
			var videoDetailActivity = Alloy.createController('detail/VideoDetailActivity', {
				videoObject : object
			});
			videoDetailActivity.getView().open();
		}
	});

	////setMaker////
	$.videoListView.setMarker({
		sectionIndex : 0,
		itemIndex : (limit - 1)
	});

	$.videoListView.addEventListener('marker', function(e) {

		var max = limit + limitLength;
		limit = limit + 1;
		if (tabVideoBool == 0) {
			getVideoMaker(limit, max);
		} else {
			getOtherVideoMaker(limit, max);
		}

	});

}

var refreshTabMenu = function() {
	showProgressBar();

	limit = PlayParkMobileAPI.LIMIT;
	_isFirstLoadJson = true;

	$.videoListView.setMarker({
		sectionIndex : 0,
		itemIndex : (limit - 1)
	});
	//////////Ti.API.info('$.videoListView.removeAllChildren()');

	$.videoListView.removeAllChildren();
	//////////Ti.API.info('tabVideoBoo : ' + tabVideoBool);

	if (tabVideoBool == 0) {
		$.tabMenuLeftView.setOpacity(1.0);
		$.tabMenuRightView.setOpacity(0.5);
		getVideo();
	} else {
		$.tabMenuLeftView.setOpacity(0.5);
		$.tabMenuRightView.setOpacity(1.0);
		getOtherVideo();
	}
};

var refreshControl = function(e) {
	showProgressBar();
	$.ptr.show();

	if (tabVideoBool == 0) {
		//getVideo();
		tabVideoBool = 0;
		refreshTabMenu();
	} else {
		//getOtherVideo();
		tabVideoBool = 1;
		refreshTabMenu();
	}

	setTimeout(function() {
		$.ptr.hide();
		hideProgressBar();
	}, 2000);
};

var getContent = function() {
	$.tabMenuLeftView.setOpacity(1.0);
	$.tabMenuRightView.setOpacity(0.5);
	$.tabMenuLeftLabel.setText(Ti.Locale.getString("video_tab_game_video_" + Alloy.Globals.LANGUAGE));
	$.tabMenuRightLabel.setText(Ti.Locale.getString("video_tab_other_" + Alloy.Globals.LANGUAGE));

	getVideo();
};

var getOtherVideo = function() {
	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		PlayParkMobileAPI.getOtherVideo(getOtherVideoCallback);
	} else {
		hideProgressBar();
		alert(Ti.Locale.getString("network_none_" + Alloy.Globals.LANGUAGE));
	}
};

var getOtherVideoCallback = function(_data) {
	if (_data) {
		if (_data.code == PlayParkMobileAPI.SUCCESS) {
			var message = _data.result.message;
			category = _data.result.category;
			createListview(category);
			hideProgressBar();
		} else {
			hideProgressBar();
			alert(Ti.Locale.getString("data_not_found_" + Alloy.Globals.LANGUAGE));
		}
	} else {
		hideProgressBar();
	}

};
/////videoMaker/////
var getOtherVideoMaker = function(from, to) {
	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		PlayParkMobileAPI.getOtherVideo(getOtherVideoMakerCallback, from, to);
	} else {
		limit = limit - 1;
	}
};

var getOtherVideoMakerCallback = function(_data) {
	if (_data) {
		if (_data.code == PlayParkMobileAPI.SUCCESS) {
			var message = _data.result.message;
			var categoryMaker = _data.result.category;
			createListview(categoryMaker);
		} else {
			limit = limit - 1;
		}
	} else {
		limit = limit - 1;
	}

};

var getVideoMaker = function(from, to) {
	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		PlayParkMobileAPI.getVideoList(getVideoMakerCallback, null, from, to);
	} else {
		limit = limit - 1;
	}
};

var getVideoMakerCallback = function(_data) {
	if (_data) {
		if (_data.code == PlayParkMobileAPI.SUCCESS) {
			var message = _data.result.message;
			var categoryMaker = _data.result.category;
			createListview(categoryMaker);
		} else {
			limit = limit - 1;
			//////////////////Ti.API.info('getVideoMakerCallback ==> limit : ' + limit);
		}
	} else {
		limit = limit - 1;
		//////////////////Ti.API.info('getVideoMakerCallback ==> limit : ' + limit);
	}

};
////////////////

var getVideo = function() {

	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		PlayParkMobileAPI.getVideoList(getVideoCallback, null);
	} else {
		hideProgressBar();
		alert(Ti.Locale.getString("network_none_" + Alloy.Globals.LANGUAGE));
	}
};

var getVideoCallback = function(_data) {
	//////////////////Ti.API.info('_data : ' + _data);
	if (_data) {
		if (_data.code == PlayParkMobileAPI.SUCCESS) {
			//////////Ti.API.info('$.videoListView.removeAllChildren()');
			var message = _data.result.message;
			category = _data.result.category;
			createListview(category);
			hideProgressBar();
		} else {
			hideProgressBar();
			alert(Ti.Locale.getString("data_not_found_" + Alloy.Globals.LANGUAGE));
		}
	} else {
		hideProgressBar();
	}

};

var createListview = function(_data) {
	//////////////////Ti.API.info('createListview >> data ' + _data);
	var items = [];

	for (var i in _data) {
		if (!_isFirstLoadJson) {
			category[category.length] = _data[i];
			//////////////////Ti.API.info('for >>> if >>> category.length : ' + category.length);
		};

		var object = JSON.stringify(_data[i]);
		var data = JSON.parse(object);
		var date = DateTime.getEventDateTime(data.date, DateTime.MODE_DATE);
		var views = FormatCurrency.getFormatCurrency(data.views);
		var imageEncoded = encodeURI(data.images);

		items.push({
			template : "template", // set the template
			videoListViewNameLabel : {
				text : data.game
			},
			videoListViewTimeLabel : {
				text : date
			},
			videoListViewImageView : {
				image : imageEncoded
			},
			videoListViewTitleLabel : {
				text : data.title
			},
			videoListViewViewerLabel : {
				text : views
			}
		});

	}

	//////////////////Ti.API.info('_isFirstLoadJson : ' + _isFirstLoadJson);
	if (_isFirstLoadJson) {
		_isFirstLoadJson = false;
		//////////Ti.API.info('$.videoListView.setItem()');
		//////////////////Ti.API.info('createListview >>> if _isFirstLoadJson');
		$.section.setItems(items);
		setTimeout(function() {
			$.videoListView.scrollToItem(0, 0, Titanium.UI.iPhone.RowAnimationStyle.TOP);
		}, 500);

	} else {
		//////////////////Ti.API.info('createListview >>> else _isFirstLoadJson = items.length : ' + items.length);
		$.section.appendItems(items);

		limit = limit + limitLength - 1;
		//////////////////Ti.API.info('createListview >>> limit : ' + limit);

		$.videoListView.setMarker({
			sectionIndex : 0,
			itemIndex : (limit - 1)
		});
	}

};

var showProgressBar = function() {
	_progressBar.show();
};

var hideProgressBar = function() {
	if (_progressBar != null) {
		setTimeout(function() {
			_progressBar.hide();
		}, 1000);
	}
};

init();
