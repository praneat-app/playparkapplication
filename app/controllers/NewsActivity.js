this.name = Alloy.Globals.NEWS_ACTIVITY;

var isLoadNewsList = false;
var tabGameViewStart = Alloy.Globals.LISTVIEW_DOUBLETAB_TOP - (Alloy.Globals.LISTVIEW_DOUBLETAB_TOP * 6);
var tabGameBool = 0;
var tabMenuBool = 0;
var menugameArray;
var menuCateNewsArray;
var tabGameNumberSelect = 0;

if (Alloy.Globals.OS_IOS) {
	var MAX_TITLE_LENGTH = 80;
} else {
	var MAX_TITLE_LENGTH = Alloy.Globals.MAX_TITLE_LENGTH;
}

var PlayParkMobileAPI = require("/api/PlayParkMobileAPI");
var DateTime = require("/utils/DateTime");
var ProgressBar = require("/utils/ProgressBar");
var _progressBar = new ProgressBar({
	parentView : $.contentContainer
});

var category;
var idSelect;

var gameId = null;
var limit = PlayParkMobileAPI.LIMIT;
var limitLength = 10;
var _isFirstLoadJson = true;

function init() {

	var GoogleAnalytics = require("/api/GoogleAnalytics");
	var googleAnalytics = new GoogleAnalytics();
	googleAnalytics.trackScreen("News");

	showProgressBar();
	getContent();

	Alloy.Globals.SideMenuButton = Alloy.Globals.ds.SIDEMENU_OTHER;
	Alloy.Globals.ds.rightButton.setBackgroundImage("/images/other/rightButton_background_default.png");

	//tabGame
	$.tabGameOnline.addEventListener("click", function(e) {
		//tabMenu
		tabMenuBool = 0;
		$.tabGameListView.removeAllChildren();
		refreshTabGameListview(null);

		$.tabGame.setTitle(Ti.Locale.getString("other_choose_game_title_" + Alloy.Globals.LANGUAGE));
		$.tabMenuLeftView.setOpacity(1.0);
		$.tabMenuRightView.setOpacity(0.5);
	});

	$.tabGameWorld.addEventListener("click", function(e) {

		tabMenuBool = 1;
		$.tabGameListView.removeAllChildren();
		var idDefault = 0;
		refreshTabGameListview(idDefault);
		//tabMenu
		$.tabMenuLeftView.setOpacity(0.5);
		$.tabMenuRightView.setOpacity(1.0);
	});

	$.tabGameListView.setTop(tabGameViewStart);
	$.tabGame.addEventListener("click", function(e) {
		onCheckTabGame();
	});

	$.tabGameListView.addEventListener("itemclick", function(e) {
		refreshTabGameListview(e.itemIndex);
	});

	if (Alloy.Globals.OS_ANDROID) {
		//tabGame
		$.tabGameImage.addEventListener("click", function(e) {
			onCheckTabGame();
		});
	}

	$.newsListView.addEventListener('itemclick', function(e) {
		if (category) {
			if (e.bindId == "newsListViewNameLabel") {
				var gameId = category[e.itemIndex].gameid;
				onCheckRowNews(gameId);
				showProgressBar();
			} else {
				var html = category[e.itemIndex].html;
				var title = category[e.itemIndex].title;
				var link = category[e.itemIndex].link;

				var promotionDetail = Alloy.createController('detail/NewsDetail', {
					htmlObject : html,
					titleShare : title,
					linkShare : link
				});
				promotionDetail.getView().open();
			}
		}
	});

	////setMaker////
	$.newsListView.setMarker({
		sectionIndex : 0,
		itemIndex : (limit - 1)
	});

	$.newsListView.addEventListener('marker', function(e) {
		var max = limit + limitLength;
		limit = limit + 1;

		//////////////////Ti.API.info('marker max : ' + max);
		//////////////////Ti.API.info('marker limit : ' + limit);
		getNewsListMaker(idSelect, limit, max);
	});

}

var getContent = function() {
	//tabMenu when start
	$.tabMenuLeftView.setOpacity(1.0);
	$.tabMenuRightView.setOpacity(0.5);
	$.tabMenuLeftLabel.setText(Ti.Locale.getString("news_tab_game_online_" + Alloy.Globals.LANGUAGE));
	$.tabMenuRightLabel.setText(Ti.Locale.getString("news_tab_game_world_news_" + Alloy.Globals.LANGUAGE));
	//tabGame when start
	var tab = Ti.Locale.getString("other_choose_game_title_" + Alloy.Globals.LANGUAGE);
	$.tabGame.setTitle(tab);
	getMenugame();
};
var getMenugame = function() {
	////////////////////////////////////Ti.API.info('getMenugame');
	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		PlayParkMobileAPI.getMenugame(getMenugameCallback);
	} else {
		alert(Ti.Locale.getString("network_none_" + Alloy.Globals.LANGUAGE));
		hideProgressBar();
	}
};

var getMenugameCallback = function(data) {
	if (data.code == PlayParkMobileAPI.SUCCESS) {
		getMenuCateNews();
		var message = data.result.message;
		menugameArray = data.result.menugame;
		//save menugameArray
		if (!isLoadNewsList) {
			refreshTabGameListview(null);
		};
	} else {
		menugameArray = null;
		var dialog = Ti.UI.createAlertDialog({
			cancel : 0,
			buttonNames : ['Cancel', 'OK'],
			message : Ti.Locale.getString("api_loading_failed_title"),
			title : Ti.Locale.getString("api_loading_failed_try_message")
		});
		dialog.addEventListener('click', function(e) {
			if (e.index !== e.source.cancel) {
				showProgressBar();
				getMenugame();
			}
		});
		dialog.show();
		hideProgressBar();
	}
};

var refreshTabGameListview = function(itemIndex) {
	//tab 1.showProgress 2.check animation tab game 3.getdata 4.checktabmenu ใส่ข้อมูลที่เลือก
	showProgressBar();
	tabGameBool = 1;
	onCheckTabGame();
	getDataNewsListview(itemIndex);
	onCheckTabMenu(itemIndex);

	limit = PlayParkMobileAPI.LIMIT;
	_isFirstLoadJson = true;

	$.newsListView.setMarker({
		sectionIndex : 0,
		itemIndex : (limit - 1)
	});
};

var onCheckTabGame = function() {//check tab game animtaion
	if (tabGameBool != 0) {
		$.tabGameListView.animate({
			top : tabGameViewStart,
			duration : Alloy.Globals.TIME_DURATION
		});
		$.tabGameImage.setImage("/images/other/other_tabgame_image.png");
		tabGameBool = 0;
	} else {
		$.tabGameListView.animate({
			top : Alloy.Globals.LISTVIEW_DOUBLETAB_TOP,
			duration : Alloy.Globals.TIME_DURATION
		});
		$.tabGameImage.setImage("/images/other/other_tabgame_image_select.png");
		tabGameBool = 1;
	}
};

var getDataNewsListview = function(itemIndex) {
	if (itemIndex != undefined && menugameArray) {
		if (tabMenuBool == 0) {//menugameArray
			getNewsList(menugameArray[itemIndex].id);

		} else {//menuCateNewsArray
			getNewsList(menuCateNewsArray[itemIndex].id);
		}
	} else {
		getNewsList();
	}
};
//////news setMaker//////
var getNewsListMaker = function(_itemId, _to, _from) {
	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		if (tabMenuBool == 0) {
			PlayParkMobileAPI.getNewsList(getNewsMakerCallback, _itemId, null, _to, _from);
		} else {
			PlayParkMobileAPI.getOtherNewList(getNewsMakerCallback, _itemId, _to, _from);
		}
	} else {
		limit = limit - 1;
	}
};

var getNewsMakerCallback = function(data) {
	////////////////////////////////////Ti.API.info('getNewsCallback: ' + data.code);
	if (data.code == PlayParkMobileAPI.SUCCESS) {
		isLoadNewsList = true;
		var message = data.result.message;
		var categoryAppendItem = data.result.category;
		createListview(categoryAppendItem);
	} else {
		limit = limit - 1;
	}
};

//////news///////
var getNewsList = function(_itemId) {
	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		if (tabMenuBool == 0) {
			PlayParkMobileAPI.getNewsList(getNewsCallback, _itemId, null);
		} else {
			PlayParkMobileAPI.getOtherNewList(getNewsCallback, _itemId);
		}
	} else {
		alert(Ti.Locale.getString("network_none_" + Alloy.Globals.LANGUAGE));
		hideProgressBar();
	}
};

var getNewsCallback = function(data) {
	////////////////////////////////////Ti.API.info('getNewsCallback: ' + data.code);
	if (data.code == PlayParkMobileAPI.SUCCESS) {
		isLoadNewsList = true;
		var message = data.result.message;
		category = data.result.category;
		createListview(category);
		hideProgressBar();
	} else {
		alert(Ti.Locale.getString("data_not_found_" + Alloy.Globals.LANGUAGE));
		category = null;
		hideProgressBar();
	}
};

var createListview = function(_data) {
	if (!_data) {
		return;
	}

	var items = [];
	for (var i in _data) {

		if (!_isFirstLoadJson) {
			category[category.length] = _data[i];
		};

		var object = JSON.stringify(_data[i]);
		var data = JSON.parse(object);
		var date = DateTime.getNormalDate(data.date);
		var title = Titanium.Network.decodeURIComponent(data.title);

		if (title) {
			if (title.length > MAX_TITLE_LENGTH) {
				title = title.substr(0, MAX_TITLE_LENGTH).trim() + "...";
			}
		}
		items.push({
			template : "template", // set the template
			newsListViewNameLabel : {
				text : data.gametype
			},
			newsListViewTimeLabel : {
				text : date
			},
			newsListViewImageView : {
				image : data.image
			},
			newsListViewDescipteLabel : {
				text : title
			}
		});
	}

	if (_isFirstLoadJson) {
		$.section.setItems(items);
		setTimeout(function() {
			$.newsListView.scrollToItem(0, 0, Titanium.UI.iPhone.RowAnimationStyle.TOP);
		}, 500);
		_isFirstLoadJson = false;

	} else {

		//////////////////Ti.API.info('createListview >>> items.length : ' + items.length);
		//////////////////Ti.API.info('createListview >>> limit : ' + limit);
		$.section.appendItems(items);
		limit = limit + limitLength - 1;
		$.newsListView.setMarker({
			sectionIndex : 0,
			itemIndex : (limit - 1)
		});

	}

};

var refreshControl = function(e) {
	showProgressBar();
	$.ptr.show();
	refreshTabGameListview(idSelect);
	setTimeout(function() {
		$.ptr.hide();
		hideProgressBar();
	}, 2000);
};

var onCheckRowNews = function(_id) {
	//เลือกประเภทเกมแล้วไปยังประเภทเกมที่เลือก
	var row = null;
	var array = [];

	if (tabMenuBool == 0) {//menugameArray
		array = menugameArray;
	} else {
		array = menuCateNewsArray;
	}

	for (var i in array) {//menuCateNewsArray
		var object = JSON.stringify(array[i]);
		var data = JSON.parse(object);
		if (data.id == _id) {
			row = i;
		}
	}
	$.tabGameListView.removeAllChildren();
	refreshTabGameListview(row);
};

var getMenuCateNews = function() {
	////////////////////Ti.API.info('getMenuCateNews');
	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		PlayParkMobileAPI.getMenuCateNews(getMenuCateNewsCallback);
	} else {
		alert(Ti.Locale.getString("network_none_" + Alloy.Globals.LANGUAGE));
		hideProgressBar();
	}
};

var getMenuCateNewsCallback = function(data) {
	////////////////////Ti.API.info('getMenuCateNewsCallback: ' + data.code);
	if (data.code == PlayParkMobileAPI.SUCCESS) {
		var message = data.result.message;
		menuCateNewsArray = data.result.category;
	} else {
		menuCateNewsArray = null;
		var dialog = Ti.UI.createAlertDialog({
			cancel : 0,
			buttonNames : ['Cancel', 'OK'],
			message : Ti.Locale.getString("api_loading_failed_title"),
			title : Ti.Locale.getString("api_loading_failed_try_message")
		});

		dialog.addEventListener('click', function(e) {
			if (e.index !== e.source.cancel) {
				showProgressBar();
				getMenuCateNews();
			}
		});
		dialog.show();
		hideProgressBar();
	}
};

var onCheckTabMenu = function(_tabGameSelect) {
	if (tabMenuBool == 0) {// if  0 = game online .. 1 = game world (selected)
		createGameListview(menugameArray, tabMenuBool, _tabGameSelect);
	} else {
		createGameListview(menuCateNewsArray, tabMenuBool, _tabGameSelect);
	}
};

var createGameListview = function(_data, _tabMenuBool, _idSelect) {

	idSelect = _idSelect;
	if (!_data) {
		return;
	}
	var arrayLength = _data.length;
	if (arrayLength >= 6) {
		arrayLength = 6;
	};

	var lisviewHeight = (Alloy.Globals.CONTAINER_HIGTH * arrayLength);
	var tabGameViewStart = Alloy.Globals.LISTVIEW_DOUBLETAB_TOP - lisviewHeight;

	$.tabGameListView.setTop(tabGameViewStart);
	$.tabGameListView.setHeight(lisviewHeight);

	var backgroudSelect = "/images/other/other_tabgame_toggle_bg_select.png";
	var textColor = "white";
	var iconTabgameArray = new Array("/images/news/news_gameworld_icon1.png", "/images/news/news_gameworld_icon2.png", "/images/news/news_gameworld_icon3.png", "/images/news/news_gameworld_icon4.png", "/images/news/news_gameworld_icon5.png");
	var iconTabgameselectArray = new Array("/images/news/news_gameworld_icon1_select.png", "/images/news/news_gameworld_icon2_select.png", "/images/news/news_gameworld_icon3_select.png", "/images/news/news_gameworld_icon4_select.png", "/images/news/news_gameworld_icon5_select.png");
	var iconTabArray = iconTabgameselectArray;

	var newsIconWH = null;

	if (Alloy.Globals.OS_ANDROID) {
		newsIconWH = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.news_icon_height_width);
	} else {
		newsIconWH = Ti.UI.SIZE;
	}

	var items = [];
	for (var i in _data) {
		var object = JSON.stringify(_data[i]);
		var data = JSON.parse(object);

		var title = data.name;

		if (_tabMenuBool != 0) {

			if (Alloy.Globals.LANGUAGE == Alloy.Globals.LANGUAGE_TH) {
				title = data.title_th;
			} else {
				title = data.title_en;
			}
		}

		if (_idSelect == i) {///ทำให้ id ที่เลือกเปลี่ยนสี
			backgroudSelect = "/images/other/other_tabgame_toggle_bg_select.png";
			textColor = "white";
			iconTabArray = iconTabgameselectArray;
			$.tabGame.setTitle(title);

		} else {
			backgroudSelect = "transparent";
			textColor = "#707070";
			iconTabArray = iconTabgameArray;
		}

		if (_tabMenuBool == 0) {
			items.push({
				template : "tabMenuGameTemplate",
				tabGameTitle : {
					text : title,
					color : textColor
				},
				tabGameListViewBg : {
					backgroundImage : backgroudSelect
				}
			});

		} else {
			items.push({
				template : "tabMenuCateNewTemplate",
				tabGameTitle : {
					text : title,
					color : textColor
				},
				tabGameIcon : {
					image : iconTabArray[i],
					width : newsIconWH,
					height : newsIconWH
				},
				tabGameListViewBg : {
					backgroundImage : backgroudSelect
				}
			});
		}
	}
	$.tabGameSection.setItems(items);
};

var showProgressBar = function() {
	_progressBar.show();
};

var hideProgressBar = function() {
	if (_progressBar != null) {
		setTimeout(function() {
			_progressBar.hide();
		}, 1000);
	}
};

init();
