var _highLight = arguments[0] || {};
var self = this;
var _parentView = _highLight.parentView;
this.name = Alloy.Globals.HIGHLIGHT_ACTIVITY;

var MODE_CONTENT_NEWS = 0;
var MODE_CONTENT_PROMOTION = 1;
var MODE_CONTENT_EVENT = 2;
var MODE_SELECT = MODE_CONTENT_NEWS;

var PlayParkMobileAPI = require("/api/PlayParkMobileAPI");
var DateTime = require("/utils/DateTime");
var ProgressBar = require("utils/ProgressBar");
var _progressBar = new ProgressBar({
	parentView : $.contentContainer
});

var isBanner = false;
var isApps = false;
var isNews = false;

function init() {

	//GoogleAnalytics
	var GoogleAnalytics = require("/api/GoogleAnalytics");
	var googleAnalytics = new GoogleAnalytics();
	googleAnalytics.trackScreen("Hightlight");

	Alloy.Globals.SideMenuButton = Alloy.Globals.ds.SIDEMENU_COUNTRY;
	var countryImage = "/images/setting/setting_country_" + Alloy.Globals.countryCode + ".png";
	Alloy.Globals.ds.rightButton.setBackgroundImage(countryImage);

	showProgressBar();
	getContent();

	var newPageArray = ["NewsActivity", "PromotionActivity", "EventActivity", "GameAppsActivity"];
	var newPageCount = ["1", "2", "3", "6"];

	$.newPageButton.addEventListener('click', function(e) {
		openNewPage(newPageArray[MODE_SELECT]);
		Alloy.Globals.addNavTitle(newPageCount[MODE_SELECT]);
	});

	$.tabNewsButton.addEventListener("click", function(e) {
		showProgressBar();
		MODE_SELECT = MODE_CONTENT_NEWS;
		getListView(MODE_CONTENT_NEWS);
	});

	$.tabPromotionButton.addEventListener("click", function(e) {
		showProgressBar();
		MODE_SELECT = MODE_CONTENT_PROMOTION;
		getListView(MODE_CONTENT_PROMOTION);
	});

	$.tabActiButton.addEventListener("click", function(e) {
		showProgressBar();
		MODE_SELECT = MODE_CONTENT_EVENT;
		getListView(MODE_CONTENT_EVENT);
	});

	$.appIconAll.addEventListener("click", function(e) {
		var appNumber = 3;
		openNewPage(newPageArray[appNumber]);
		Alloy.Globals.addNavTitle(newPageCount[appNumber]);
	});

};

function openNewPage(_page) {
	var newCurrentView = Alloy.createController(_page).getView();
	if (newCurrentView != Alloy.Globals.currentViewRight) {
		Alloy.Globals.ds.contentview.remove(Alloy.Globals.currentViewRight);
		Alloy.Globals.currentViewRight = newCurrentView;
		Alloy.Globals.ds.contentview.add(Alloy.Globals.currentViewRight);
	}
}

var getContent = function() {

	$.titleLabel.setText(Ti.Locale.getString("highlight_download_" + Alloy.Globals.LANGUAGE));
	if (Alloy.Globals.OS_ANDROID) {
		$.bannerScrollView.setHeight((Alloy.Globals.SCREEN_WIDTH * 320) / 640);
	}

	if (Alloy.Globals.LANGUAGE == "th") {
		$.newPageImage.setImage("/images/highlight/highlight_news_all_button_th.png");
		$.tabNews.setImage("/images/highlight/highlight_news_menu_news.png");
		$.tabPromotion.setImage("/images/highlight/highlight_news_menu_promotion.png");
		$.tabActi.setImage("/images/highlight/highlight_news_menu_acti.png");
	} else {
		$.newPageImage.setImage("/images/highlight/highlight_news_all_button_en.png");
		$.tabNews.setImage("/images/highlight/highlight_news_menu_news_en.png");
		$.tabPromotion.setImage("/images/highlight/highlight_news_menu_promotion_en.png");
		$.tabActi.setImage("/images/highlight/highlight_news_menu_acti_en.png");
	}

	getBanner();
	getGameApp();
	MODE_SELECT = MODE_CONTENT_NEWS;
	getListView(MODE_SELECT);

};
var getListView = function(_mode) {
	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		var count = 2;
		if (_mode == MODE_CONTENT_NEWS) {
			PlayParkMobileAPI.getNewsList(getListViewCallback, null, count);
		} else if (_mode == MODE_CONTENT_PROMOTION) {
			PlayParkMobileAPI.getPromotion(getListViewCallback, null, count);
		} else if (_mode == MODE_CONTENT_EVENT) {
			PlayParkMobileAPI.getEvent(getEventListViewCallback, null, count);
		}
	} else {
		//////////////////////////////////////////Ti.API.info('getListView ==> offline');
		alert(Ti.Locale.getString("network_none_" + Alloy.Globals.LANGUAGE));
		isNews = true;
		checkHideProgress();
	}
};
var getListViewCallback = function(_data) {
	//////////////////////Ti.API.info('HighlightActivity  >>>>>>  data : ' + _data.code);

	if (_data) {
		if (_data.code == PlayParkMobileAPI.SUCCESS) {
			var message = _data.result.message;
			var newsObject = _data.result.category;
			$.newsContainer.removeAllChildren();
			getContentListView(newsObject);

		} else {
			isNews = true;
			checkHideProgress();
		}
	}
};
var getEventListViewCallback = function(_data) {
	//////////////////////////////////////////Ti.API.info('getListViewCallback  >>>>>>  data : ' + _data.code);
	if (_data.code == PlayParkMobileAPI.SUCCESS) {
		var message = _data.result.message;
		var eventObject = _data.result.category;
		$.newsContainer.removeAllChildren();
		getEventListView(eventObject);

	} else {
		isNews = true;
		checkHideProgress();
	}
};
var getBanner = function() {
	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		PlayParkMobileAPI.getBanner(getBannerCallback);
	} else {
		isBanner = true;
		checkHideProgress();
	}
};
var getBannerCallback = function(_data) {
	////////////////////////Ti.API.info('getBannerCallback  >>>>>>  data : ' + _data.code);
	if (_data.code == PlayParkMobileAPI.SUCCESS) {
		var message = _data.result.message;
		var banner = _data.result.category;
		createBanner(banner);
	} else {
		isBanner = true;
		checkHideProgress();
	}
};
var createBanner = function(_bannerArray) {
	var viewArray = [];

	for (var i in _bannerArray) {
		////////////////////////Ti.API.info('_bannerArray[i].image : ' + _bannerArray[i].images);
		var imageEncoded = Ti.Network.encodeURIComponent(_bannerArray[i].images);

		var view = Ti.UI.createImageView({
			width : Ti.UI.FILL,
			height : Ti.UI.FILL,
			image : _bannerArray[i].images,
			_link : _bannerArray[i].link,
			top : 0
		});
		viewArray[i] = view;

		view.addEventListener("click", function(e) {
			Titanium.Platform.openURL(e.source._link);
		});

	}
	$.bannerScrollView.setViews(viewArray);

	isBanner = true;
	checkHideProgress();
};
var getGameApp = function() {
	//////////////////////////////////////////Ti.API.info('getGameApp');
	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		PlayParkMobileAPI.getGameApp(getGameAppCallback);
	} else {
		//////////////////////////////////////////Ti.API.info('getGameApp ==> offline');
		isApps = true;
		checkHideProgress();
	}
};
var getGameAppCallback = function(_data) {
	//////////////////////////////////////////Ti.API.info('getGameAppCallback  >>>>>>  data : ' + _data.code);
	if (_data.code == PlayParkMobileAPI.SUCCESS) {
		var message = _data.result.message;
		var gameApp = _data.result.category;
		createGameApp(gameApp);
	} else {
		isApps = true;
		checkHideProgress();
	}
};
var createGameApp = function(_gameApp) {

	var iconViewWidth = 73;
	var iconViewStart = 15;
	var iconViewLeftPadding = 21;

	if (Alloy.Globals.OS_IOS_IPAD) {
		iconViewWidth = 90;
		iconViewStart = 20;
		iconViewLeftPadding = 30;
	}

	var iconLength = _gameApp.length;
	var scrollViewWidth = iconViewStart + (iconViewWidth * iconLength) + (iconViewLeftPadding * iconLength);
	$.appSampleAppScrollView.setContentWidth(scrollViewWidth);

	for (var j in _gameApp) {

		var iconPaddingLeft = iconViewStart + (iconViewWidth * j) + (iconViewLeftPadding * j);
		if (Alloy.Globals.OS_IOS) {
			var iconGameButton = Ti.UI.createButton({
				left : iconPaddingLeft,
				_link : _gameApp[j].ios
			});
		} else {

			var iconGameButton = Ti.UI.createView({
				left : iconPaddingLeft,
				_link : _gameApp[j].android,
				right : 15
			});
		}

		$.addClass(iconGameButton, "appsGameButton");

		if (Alloy.Globals.OS_IOS) {
			var iconGameImageViewButton = Ti.UI.createImageView({
				image : _gameApp[j].image
			});

			$.addClass(iconGameImageViewButton, "appsGameImageView");
		} else {
			var iconGameImageViewButton = Ti.UI.createImageView({
				_link : _gameApp[j].android,
				image : _gameApp[j].image
			});

			$.addClass(iconGameImageViewButton, "appsGameImageView");
		}

		//_gameApp[j].title
		var title = _gameApp[j].title;
		var titleArray = title.split(" ");

		var iconGameLabel = Ti.UI.createLabel({
			text : titleArray[0]
		});
		$.addClass(iconGameLabel, "appsGameLabel");

		var text;
		if (!titleArray[2]) {
			text = titleArray[1];
		} else {
			text = titleArray[1]; +" " + titleArray[2];
		}
		////////////////////////Ti.API.info('text :' + text);

		var iconGameLabel2 = Ti.UI.createLabel({
			text : text ,
		});
		$.addClass(iconGameLabel2, "appsGameLabel");

		iconGameButton.add(iconGameImageViewButton);
		iconGameButton.add(iconGameLabel);
		iconGameButton.add(iconGameLabel2);

		$.appSampleAppScrollView.add(iconGameButton);

		iconGameButton.addEventListener("click", function(e) {
			//////////////////////Ti.API.info('e.source._link :' + e.source._link);
			if (e.source._link) {
				Titanium.Platform.openURL(e.source._link);
			} else {
				alert("Coming Soon");
			}
		});
	}
	isApps = true;
	checkHideProgress();

};

var getEventListView = function(_contentArray) {

	for (var i in _contentArray) {

		var object = JSON.stringify(_contentArray[i]);
		var data = JSON.parse(object);

		var dateStart = DateTime.getEventDateTime(data.date_start, DateTime.MODE_DATE);
		var timeStart = DateTime.getEventDateTime(data.date_start, DateTime.MODE_TIME);

		var dateEnd = DateTime.getEventDateTime(data.date_end, DateTime.MODE_DATE);
		var timeEnd = DateTime.getEventDateTime(data.date_end, DateTime.MODE_TIME);

		//////////////////////////////////////////Ti.API.info('_content : ' + _contentArray[i]);
		var eventListViewBoxView = Ti.UI.createView({
			_content : _contentArray[i]
		});
		$.addClass(eventListViewBoxView, "highLightlistViewBoxView");

		var listViewInBoxView = Ti.UI.createView();
		$.addClass(listViewInBoxView, "listViewInBoxView");
		eventListViewBoxView.add(listViewInBoxView);

		var listViewTitleContainer = Ti.UI.createView({
			_content : _contentArray[i]
		});
		$.addClass(listViewTitleContainer, "listViewTitleContainer");
		listViewInBoxView.add(listViewTitleContainer);

		var listViewTitleLeft = Ti.UI.createView();
		$.addClass(listViewTitleLeft, "listViewTitleLeft");

		var listViewTitleIcon = Ti.UI.createImageView();
		$.addClass(listViewTitleIcon, "listViewTitleIcon");

		if (Alloy.Globals.OS_IOS) {
			var eventListViewNameLabel = Ti.UI.createLabel({
				text : data.gametype,
			});
			$.addClass(eventListViewNameLabel, "listViewNameLabel");
		} else {

			var eventListViewNameLabel = Ti.UI.createLabel({
				text : data.gametype,
				width : Ti.UI.SIZE,
				height : Ti.UI.SIZE,
				textAlign : "left",
				color : "#444444",
				font : {
					fontSize : Alloy.Globals.FONT_SIZE_NAME_LISTVIEW,
					fontFamily : Alloy.Globals.FONT_DEFAULT
				}
			});

		}

		listViewTitleLeft.add(listViewTitleIcon);
		listViewTitleLeft.add(eventListViewNameLabel);

		listViewTitleContainer.add(listViewTitleLeft);
		listViewTitleContainer.addEventListener("click", function(e) {
			var object = e.source._content;
			openEventDetail(object);
		});

		var eventListViewImageView = Ti.UI.createImageView({
			image : data.image,
			_content : _contentArray[i]

		});
		$.addClass(eventListViewImageView, "listViewImageView");

		listViewInBoxView.add(eventListViewImageView);

		eventListViewImageView.addEventListener("click", function(e) {
			var object = e.source._content;
			////////////////Ti.API.info('object : ' + object);
			openEventDetail(object);
		});

		var eventTimeContainer = Ti.UI.createView();
		$.addClass(eventTimeContainer, "eventTimeContainer");
		listViewInBoxView.add(eventTimeContainer);

		var eventTimeBox = Ti.UI.createView();
		$.addClass(eventTimeBox, "eventTimeBox");
		eventTimeContainer.add(eventTimeBox);

		///////
		var eventTitleBox = Ti.UI.createView();
		$.addClass(eventTitleBox, "eventTitleBox");

		var eventClock = Ti.UI.createImageView();
		$.addClass(eventClock, "eventClock");

		//23
		var imageClockLeft = 23;
		if (Alloy.Globals.OS_IOS) {
			if (Alloy.Globals.OS_IOS_IPHONE) {
				imageClockLeft = 23 + 2;
			} else {
				imageClockLeft = 23 + 5;
			}
		} else {
			var StringAdapter = require("/utils/StringAdapter");
			imageClockLeft = StringAdapter.getDimension(Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.itemlistview_eventListviewContainer_clock_height_width)) + 5;
		}

		////////////Ti.API.info("imageClockLeft : " + imageClockLeft);
		//TODO
		var eventTimeText = Ti.UI.createLabel({
			text : Ti.Locale.getString("event_datetime_" + Alloy.Globals.LANGUAGE),
			width : Ti.UI.FILL,
			height : Ti.UI.SIZE,
			left : imageClockLeft,
			color : "#ffffff",
			font : {
				fontSize : Alloy.Globals.FONT_SIZE_16,
				fontFamily : Alloy.Globals.FONT_DEFAULT
			},
			_content : _contentArray[i]
		});

		eventTimeText.addEventListener("click", function(e) {
			var object = e.source._content;
			openEventDetail(object);
		});
		//$.addClass(eventTimeText, "eventTimeText");

		eventTitleBox.add(eventClock);
		eventTitleBox.add(eventTimeText);

		///////

		var eventTimeStartContainer = Ti.UI.createView();
		$.addClass(eventTimeStartContainer, "eventTimeStartContainer");

		var eventDateStart = Ti.UI.createLabel({
			text : dateStart,
			_content : _contentArray[i]
		});
		$.addClass(eventDateStart, "eventTimeBoxText");

		eventDateStart.addEventListener("click", function(e) {
			var object = e.source._content;
			openEventDetail(object);
		});

		var eventTimeStart = Ti.UI.createLabel({
			text : timeStart,
			_content : _contentArray[i]
		});
		$.addClass(eventTimeStart, "eventTimeBoxText");

		eventTimeStart.addEventListener("click", function(e) {
			var object = e.source._content;
			openEventDetail(object);
		});

		eventTimeStartContainer.add(eventDateStart);
		eventTimeStartContainer.add(eventTimeStart);

		////
		var eventToBox = Ti.UI.createView();
		$.addClass(eventToBox, "eventToBox");

		var eventTimeText = Ti.UI.createLabel({
			text : Ti.Locale.getString("event_datetime_to_" + Alloy.Globals.LANGUAGE),
			width : Ti.UI.SIZE,
			height : Ti.UI.SIZE,
			color : "#ffffff",
			font : {
				fontSize : Alloy.Globals.FONT_SIZE_16,
				fontFamily : Alloy.Globals.FONT_DEFAULT
			},
			_content : _contentArray[i]
		});
		//$.addClass(eventTimeText, "eventTimeText");

		eventToBox.add(eventTimeText);
		eventTimeText.addEventListener("click", function(e) {
			var object = e.source._content;
			openEventDetail(object);
		});
		///
		var eventTimeFinishContainer = Ti.UI.createView();
		$.addClass(eventTimeFinishContainer, "eventTimeFinishContainer");

		var eventDateFinish = Ti.UI.createLabel({
			text : dateEnd,
			_content : _contentArray[i]
		});
		$.addClass(eventDateFinish, "eventTimeBoxText");

		eventDateFinish.addEventListener("click", function(e) {
			var object = e.source._content;
			openEventDetail(object);
		});
		//$.addClass(eventDateFinish, "eventTimeBoxText");

		var eventTimeFinish = Ti.UI.createLabel({
			text : timeEnd,
			_content : _contentArray[i]
		});
		$.addClass(eventTimeFinish, "eventTimeBoxText");

		eventTimeFinish.addEventListener("click", function(e) {
			var object = e.source._content;
			openEventDetail(object);
		});

		eventTimeFinishContainer.add(eventDateFinish);
		eventTimeFinishContainer.add(eventTimeFinish);

		var listviewAlertImage = Ti.UI.createImageView({
			_content : _contentArray[i],

		});
		$.addClass(listviewAlertImage, "listviewAlertImage");
		listviewAlertImage.addEventListener("click", function(e) {
			var object = e.source._content;
			////////////////////////////////////Ti.API.info('object : ' + object.id);

			var idEventDatabase = null;
			var rowEventDatabase = null;

			var Event = require("/utils/Event");
			var eventDatabase = Event.getIdEventFromDatabase(object.id);

			if (eventDatabase) {
				idEventDatabase = eventDatabase._dataIdEvent;
				rowEventDatabase = eventDatabase._row;
			};

			if (idEventDatabase) {
				var alertDialog = Titanium.UI.createAlertDialog({
					title : Ti.Locale.getString("Alert_event_delete_title_" + Alloy.Globals.LANGUAGE),
					message : Ti.Locale.getString("Alert_event_delete_message_" + Alloy.Globals.LANGUAGE),
					buttonNames : [Ti.Locale.getString("Alert_event_ok_" + Alloy.Globals.LANGUAGE), Ti.Locale.getString("Alert_event_cancel_" + Alloy.Globals.LANGUAGE)],
					cancel : 1,
					_row : rowEventDatabase,
					_dataIdEvent : idEventDatabase
				});

				alertDialog.show();
				alertDialog.addEventListener('click', function(e) {
					if (e.index == 0) {
						var dataIdEvent = e.source._dataIdEvent;
						Event.deleteEventFromIdEvent(dataIdEvent, e.source._row);
					}
				});

			} else {

				var alertDialog = Titanium.UI.createAlertDialog({
					title : Ti.Locale.getString("Alert_event_title_" + Alloy.Globals.LANGUAGE),
					message : Ti.Locale.getString("Alert_event_message_" + Alloy.Globals.LANGUAGE),
					buttonNames : [Ti.Locale.getString("Alert_event_ok_" + Alloy.Globals.LANGUAGE), Ti.Locale.getString("Alert_event_cancel_" + Alloy.Globals.LANGUAGE)],
					cancel : 1,
					_row : e.itemIndex,
				});

				alertDialog.show();
				alertDialog.addEventListener('click', function(e) {
					if (e.index == 0) {
						if (object) {
							var Event = require("/utils/Event");
							Event.addEvent(object);
						};
					};
				});
			}

		});

		eventTimeBox.add(eventTitleBox);
		eventTimeBox.add(eventTimeStartContainer);
		eventTimeBox.add(eventToBox);
		eventTimeBox.add(eventTimeFinishContainer);
		eventTimeContainer.add(listviewAlertImage);

		var eventListViewDescipteLabel = Ti.UI.createLabel({
			text : data.title,
			width : Ti.UI.SIZE,
			height : Ti.UI.SIZE,
			textAlign : "left",
			color : "#444444",
			font : {
				fontSize : Alloy.Globals.FONT_SIZE_TITLE_LISTVIEW, //"20sp"
				fontFamily : Alloy.Globals.FONT_DEFAULT
			},
			left : 1,
			wordWrap : true,
			_content : _contentArray[i]
		});

		eventListViewDescipteLabel.addEventListener("click", function(e) {
			var object = e.source._content;
			openEventDetail(object);
		});

		listViewInBoxView.add(eventListViewDescipteLabel);
		$.newsContainer.add(eventListViewBoxView);
	}

	isNews = true;
	checkHideProgress();

};

var openEventDetail = function(_object) {
	////////////////////////////////////Ti.API.info('openEventDetail : ' + _object);
	if (_object) {
		//////////////////////////////////////Ti.API.info('object : ' + object);
		var title = _object.title;
		var link = _object.link;
		var html = _object.html;
		////////////////////////////////////Ti.API.info('object , : ' + html + ",title : " + title + ",link : " + link);
		var eventDetail = Alloy.createController('detail/EventDetail', {
			object : _object,
			htmlObject : html,
			titleShare : title,
			linkShare : link
		});
		eventDetail.getView().open();
	}
};

var addEvent = function(_data) {

	if (_data) {

		var id = _data.id;
		var dateStart = _data.date_start;
		var dateEnd = _data.date_end;
		var title = _data.gametype;
		var des = _data.title;
		var object = _data;
		////////////////////////////////////Ti.API.info('addEvent ===> id , : ' + id + ",dateStart : " + dateStart + ",dateEnd : " + dateEnd + ", title : " + title + " , des : " + des);

		dateStart = DateTime.getDateForAddEvent(dateStart);
		dateEnd = DateTime.getDateForAddEvent(dateEnd);
		var Calendar = require("/utils/Calendar");

		Calendar.addEvent(id, dateStart, dateEnd, title, des, object);
	}
};

var getContentListView = function(_contentArray) {

	//////////////////////////////////////////Ti.API.info('_contentArray : ' + _contentArray.lenght);
	for (var k in _contentArray) {
		var newsListViewBoxView;

		if (Alloy.Globals.OS_IOS) {
			newsListViewBoxView = Ti.UI.createButton({
				_content : _contentArray[k]
			});
		} else {
			newsListViewBoxView = Ti.UI.createView({
				_content : _contentArray[k]
			});
		}

		$.addClass(newsListViewBoxView, "highLightlistViewBoxView");

		var newsListViewInBoxView = Ti.UI.createView({
			_content : _contentArray[k]
		});
		$.addClass(newsListViewInBoxView, "listViewInBoxView");

		var newsListViewTitleContainer = Ti.UI.createView({
			_content : _contentArray[k]
		});
		$.addClass(newsListViewTitleContainer, "listViewTitleContainer");

		var newsListViewTitleLeft = Ti.UI.createView({
			_content : _contentArray[k]
		});
		$.addClass(newsListViewTitleLeft, "listViewTitleLeft");

		var newsListViewTitleRight = Ti.UI.createView();
		$.addClass(newsListViewTitleRight, "listViewTitleRight");

		var newsListViewTitleIcon = Ti.UI.createImageView();
		$.addClass(newsListViewTitleIcon, "listViewTitleIcon");
		if (Alloy.Globals.OS_IOS) {
			var newsListViewTitleLabel = Ti.UI.createLabel({
				text : _contentArray[k].gametype,
			});
			$.addClass(newsListViewTitleLabel, "listViewNameLabel");
		} else {
			var newsListViewTitleLabel = Ti.UI.createLabel({
				text : _contentArray[k].gametype,
				width : Ti.UI.SIZE,
				height : Ti.UI.SIZE,
				textAlign : "left",
				color : "#444444",
				font : {
					fontSize : Alloy.Globals.FONT_SIZE_NAME_LISTVIEW,
					fontFamily : Alloy.Globals.FONT_DEFAULT
				}
			});
		}

		newsListViewTitleLeft.add(newsListViewTitleIcon);
		newsListViewTitleLeft.add(newsListViewTitleLabel);

		var time = DateTime.getNormalDate(_contentArray[k].date);

		if (Alloy.Globals.OS_IOS) {
			var newsListViewTimeLabel = Ti.UI.createLabel({
				text : time,
			});
			$.addClass(newsListViewTimeLabel, "listViewTimeLabel");

		} else {
			var newsListViewTimeLabel = Ti.UI.createLabel({
				text : time,
				width : Ti.UI.SIZE,
				height : Ti.UI.SIZE,
				textAlign : "right",
				color : "#444444",
				font : {
					fontSize : Alloy.Globals.FONT_SIZE_NAME_LISTVIEW,
					fontFamily : Alloy.Globals.FONT_DEFAULT
				}
			});

		}

		newsListViewTitleRight.add(newsListViewTimeLabel);

		newsListViewTitleContainer.add(newsListViewTitleLeft);
		newsListViewTitleContainer.add(newsListViewTitleRight);

		newsListViewInBoxView.add(newsListViewTitleContainer);

		var newsListViewImageView = Ti.UI.createImageView({
			image : _contentArray[k].image,
			_content : _contentArray[k]
		});
		$.addClass(newsListViewImageView, "listViewImageView");

		newsListViewInBoxView.add(newsListViewImageView);

		var newsListViewDescipteLabel = Ti.UI.createLabel({
			text : _contentArray[k].title,
			width : Ti.UI.SIZE,
			height : Ti.UI.SIZE,
			textAlign : "left",
			color : "#444444",
			font : {
				fontSize : Alloy.Globals.FONT_SIZE_TITLE_LISTVIEW, // Alloy.Globals.FONT_SIZE_20, //"20sp"
				fontFamily : Alloy.Globals.FONT_DEFAULT
			},
			left : 1,
			wordWrap : true,
			_content : _contentArray[k]

		});
		//$.addClass(newsListViewDescipteLabel, "listViewDescipteLabel");

		newsListViewInBoxView.add(newsListViewDescipteLabel);

		newsListViewBoxView.add(newsListViewInBoxView);
		$.newsContainer.add(newsListViewBoxView);

		newsListViewBoxView.addEventListener("click", function(e) {
			//////////////////////Ti.API.info('e.source._content : ' + e.source._content);

			var object = e.source._content;
			if (object) {
				var html = object.html;
				var title = object.title;
				var link = object.link;
				//////////////////////////////////////////Ti.API.info('MODE_SELECT : '+MODE_SELECT);
				if (MODE_SELECT == MODE_CONTENT_PROMOTION) {
					var promotionDetail = Alloy.createController('detail/PromotionDetail', {
						htmlObject : html,
						titleShare : title,
						linkShare : link
					});
					promotionDetail.getView().open();
				} else if (MODE_SELECT == MODE_CONTENT_NEWS) {
					var newsDetail = Alloy.createController('detail/NewsDetail', {
						htmlObject : html,
						titleShare : title,
						linkShare : link
					});
					newsDetail.getView().open();
				}
			}
		});
	}

	isNews = true;
	checkHideProgress();
};

var checkHideProgress = function() {
	////////////////////Ti.API.info('isBanner : ' + isBanner + "isApps : " + isApps + "isNews : " + isNews);
	if (isBanner && isApps && isNews) {
		hideProgressBar();
	};

};
var showProgressBar = function() {
	_progressBar.show();
};
var hideProgressBar = function() {
	if (_progressBar != null) {
		setTimeout(function() {
			_progressBar.hide();
		}, 1000);
	}
};

init();
