//var PushNotification = require("utils/Pushnotification");

this.name = Alloy.Globals.SETTING_ACTIVITY;

this.onFocus = function(e) {

	////////////////////////////////////Ti.API.info('aaaaa Alloy.Globals.currentViewRight : ' + Alloy.Globals.currentViewRight);
};

function init() {

	//////////////////////////////////////Ti.API.info('SettingsActivity.init ==> this.countryLabel: ' + $.countryLabel);
	var GoogleAnalytics = require("/api/GoogleAnalytics");
	var googleAnalytics = new GoogleAnalytics();
	googleAnalytics.trackScreen("Setting");

	Alloy.Globals.SideMenuButton = Alloy.Globals.ds.SIDEMENU_OTHER;
	Alloy.Globals.ds.rightButton.setBackgroundImage("/images/other/rightButton_background_default.png");

	var boolNoti = Ti.App.Properties.getBool('noti');

	if (boolNoti == undefined) {
		boolNoti = false;
	}
	$.notiSwitch.setValue(boolNoti);
	$.contryText.setText(Ti.Locale.getString("setting_country_select_" + Alloy.Globals.LANGUAGE));
	$.notiText.setText(Ti.Locale.getString("setting_notification_" + Alloy.Globals.LANGUAGE));
	//$.reminderText.setText(Ti.Locale.getString("setting_event_reminder_" + Alloy.Globals.LANGUAGE));

	$.countryView.addEventListener("click", function(e) {
		var SettingCountryActivity = Alloy.createController('detail/SettingCountryActivity').getView();
		SettingCountryActivity.open();
	});

	/*
	 $.remindingView.addEventListener("click", function(e) {
	 var SettingRemindingActivity = Alloy.createController('detail/SettingRemindingActivity').getView();
	 SettingRemindingActivity.open();
	 });
	 */
	
	$.notiSwitch.addEventListener('change', function(e) {
		//////////////////////////////////Ti.API.info('Switch value: ' + $.notiSwitch.value);
		var PushNotification = require("utils/Pushnotification");

		if ($.notiSwitch.value) {
			PushNotification.registerPushNotification(PushNotification.MODE_SUBSCRIBETOKEN);
		} else {
			PushNotification.registerPushNotification(PushNotification.MODE_UNSUBSCRIBETOKEN);

		}

		Ti.App.Properties.setBool('noti', $.notiSwitch.value);
	});


	var Country = require("utils/Country");
	var countryText = Country.getCountry();
	$.countryLabel.setText(countryText);

}

init();
