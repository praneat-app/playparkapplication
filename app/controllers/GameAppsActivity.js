this.name = Alloy.Globals.GAME_ACTIVITY;

var tabAppsBool = 0;

var PlayParkMobileAPI = require("/api/PlayParkMobileAPI");

var gameArray = [];
var appArray = [];

var _progressBar = null;
var ProgressBar = require("utils/ProgressBar");
_progressBar = new ProgressBar({
	parentView : $.contentContainer
});

if (Alloy.Globals.OS_ANDROID) {
	var packageManager = require("com.praneat.module.android.packagemanager", Ti.Android.currentActivity);
}

function init() {

	var GoogleAnalytics = require("/api/GoogleAnalytics");
	var googleAnalytics = new GoogleAnalytics();
	googleAnalytics.trackScreen("Mobile Game & Applcation");

	Alloy.Globals.SideMenuButton = Alloy.Globals.ds.SIDEMENU_OTHER;
	Alloy.Globals.ds.rightButton.setBackgroundImage("/images/other/rightButton_background_default.png");

	showProgressBar();
	getContent();

	$.tabMenuLeftView.setOpacity(1.0);
	$.tabMenuRightView.setOpacity(0.5);
	$.tabMenuLeftLabel.setText(Ti.Locale.getString("game_tab_game_apps_" + Alloy.Globals.LANGUAGE));
	$.tabMenuRightLabel.setText(Ti.Locale.getString("game_tab_apps_" + Alloy.Globals.LANGUAGE));

	$.tabGames.addEventListener("click", function(e) {
		showProgressBar();
		$.tabMenuLeftView.setOpacity(1.0);
		$.tabMenuRightView.setOpacity(0.5);
		tabAppsBool = 1;
		getGameApp();
	});

	$.tabApps.addEventListener("click", function(e) {
		showProgressBar();
		$.tabMenuLeftView.setOpacity(0.5);
		$.tabMenuRightView.setOpacity(1.0);
		tabAppsBool = 0;
		getGameApp();

		//onCheckTabMenu();
		/*if (appArray) {
		 createListview(appArray);

		 }*/
	});
	///refresh control

	if (Alloy.Globals.OS_IOS) {
		var control = Ti.UI.createRefreshControl({
			tintColor : 'gray'
		});
		$.appsListView.setRefreshControl(control);
		control.addEventListener('refreshstart', function(e) {
			showProgressBar();
			getGameApp();
			setTimeout(function() {
				Ti.API.debug('Timeout');
				hideProgressBar();
				control.endRefreshing();
			}, 2000);
		});

	}

	$.appsListView.addEventListener("itemclick", function(e) {
		//////////////////////////////////////////////Ti.API.info('gameArray : ' + [e.itemIndex]);
		//////////////////////Ti.API.info('e.bindId : ' + e.bindId);
		////////////////////Ti.API.info('appArray : ' + appArray);
		////////////////////Ti.API.info('gameArray : ' + gameArray);
		////////////////////Ti.API.info('gameArray.length : '+gameArray.length);

		if (tabAppsBool == 0) {
			if (gameArray.length > 0) {
				////////////////////Ti.API.info('if appsListView ==> appArray');
				if (e.bindId == "installImages") {
					openApplication(appArray, [e.itemIndex]);
				} else {
					openUrl(appArray, [e.itemIndex]);
				}
			} else {
				////////////////////Ti.API.info('else appsListView ==> appArray');
			}
		} else {
			if (gameArray.length > 0) {
				////////////////////Ti.API.info('if appsListView ==> gameArray');
				if (e.bindId == "installImages") {
					openApplication(gameArray, [e.itemIndex]);
				} else {
					openUrl(gameArray, [e.itemIndex]);
				}
			} else {
				////////////////////Ti.API.info('else appsListView ==> gameArray');

			}
		}
	});

}

var refreshControl = function(e) {
	showProgressBar();
	$.ptr.show();
	getGameApp();
	setTimeout(function() {
		$.ptr.hide();
		hideProgressBar();
	}, 2000);
};

var openApplication = function(_array, _id) {
	var object = JSON.stringify(_array[_id]);
	var data = JSON.parse(object);

	////////////////////////Ti.API.info('openApplication : ' + _array + _id);
	var androidScheme = data.android_scheme;

	if (androidScheme == undefined || androidScheme == "-") {
		if (data.android) {
			Titanium.Platform.openURL(data.android);
		} else {
			alert("Coming Soon");
		}

	} else {
		////////////////////////Ti.API.info('androidScheme2 : ' + androidScheme);
		var application = packageManager.canOpenApplication(androidScheme);
		////////////////////////Ti.API.info('application : ' + application);

		if (application) {
			////////////////////////Ti.API.info('index ==> openApp:  ' + data.android + ' = ' + application);
			application = packageManager.openApplication(androidScheme);
		} else {
			Titanium.Platform.openURL(data.android);
		}

	}

};
var openUrl = function(_array, _id) {
	////////////////////////////////////////Ti.API.info("_array : " + _array.length + "_id : " + _id);
	var object = JSON.stringify(_array[_id]);
	var data = JSON.parse(object);

	////////////////////////Ti.API.info('data ios : ' + data.ios);
	////////////////////////Ti.API.info('data android : ' + data.android);

	if (Alloy.Globals.OS_IOS) {
		if (data.ios) {
			Titanium.Platform.openURL(data.ios);
		} else {
			alert("Coming Soon");
		}
	} else {
		if (data.android) {
			Titanium.Platform.openURL(data.android);
		} else {
			alert("Coming Soon");
		}
	}

};

var getContent = function() {
	tabAppsBool = 1;
	getGameApp();

};

var createListview = function(_data) {
	////////////////////////////////////////Ti.API.info('data : ' + _data + ", length : " + _data.length);
	var items = [];
	var arrayLength = _data.length;

	for (var i in _data) {
		var object = JSON.stringify(_data[i]);
		var data = JSON.parse(object);
		var tamplateStyle = null;

		if (i == 0) {
			tamplateStyle = "templateStart";
		} else if (i == (arrayLength - 1)) {
			tamplateStyle = "templateEnd";
		} else {
			tamplateStyle = "template";
		}

		var imageToggle = "/images/game/game_install_button.png";

		var androidScheme = data.android_scheme;

		if (androidScheme == undefined || androidScheme == "-") {
			imageToggle = "/images/game/game_install_button.png";
		} else {
			var application = packageManager.canOpenApplication(androidScheme);
			//	//////////////////////Ti.API.info('index ==> application: ' + application);
			if (application) {
				imageToggle = "/images/game/game_play_button.png";
			} else {
				imageToggle = "/images/game/game_install_button.png";
			}
		}
		////////////////////////////////////////Ti.API.info('data : ' + data.title);

		var detail = data.detail;
		//////////////////////Ti.API.info('detail : ' + detail);

		if (detail) {
			if (detail.length > Alloy.Globals.MAX_TITLE_LENGTH) {
				detail = detail.substr(0, Alloy.Globals.MAX_TITLE_LENGTH).trim() + "...";
			}
		}
		var imageEncoded = encodeURI(data.image);

		items.push({
			template : tamplateStyle, // set the template
			appsGameImageView : {
				//image : data.image
				image : imageEncoded
			},
			appsNameLabel : {
				text : data.title
			},
			appsTypeLabel : {
				text : detail
			},
			installImages : {
				image : imageToggle
			}
		});
	}
	$.section.setItems(items);
	hideProgressBar();
};

var getGameApp = function() {
	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		PlayParkMobileAPI.getGameApp(getGameAppCallback);
	} else {
		hideProgressBar();
		alert(Ti.Locale.getString("network_none_" + Alloy.Globals.LANGUAGE));
	}
};
var getGameAppCallback = function(_data) {
	////////////////////////Ti.API.info('getGameAppCallback  >>>>>>  data : ' + _data.code);
	if (_data) {
		if (_data.code == PlayParkMobileAPI.SUCCESS) {
			var message = _data.result.message;
			var gameAppArray = _data.result.category;
			checkGameOrApp(gameAppArray);
			hideProgressBar();
		} else {
			hideProgressBar();
			alert(Ti.Locale.getString("data_not_found_" + Alloy.Globals.LANGUAGE));
		}
	} else {
		hideProgressBar();
	}

};

var checkGameOrApp = function(_data) {

	var arrayLength = _data.length;
	////////////////////////////////////////Ti.API.info('arrayLength : ' + arrayLength);
	gameArray = [];
	appArray = [];
	for (var i in _data) {
		var object = JSON.stringify(_data[i]);
		var data = JSON.parse(object);

		if (data.type == 1) {
			gameArray[gameArray.length] = _data[i];

		} else {
			appArray[appArray.length] = _data[i];
		}
	}

	$.appsListView.removeAllChildren();
	//////////////////////////////////////////Ti.API.info('tabAppsBool = ' + tabAppsBool);
	if (tabAppsBool == 0) {
		createListview(appArray);
	} else {
		createListview(gameArray);
	}

};

var showProgressBar = function() {
	_progressBar.show();
};

var hideProgressBar = function() {
	if (_progressBar != null) {
		setTimeout(function() {
			_progressBar.hide();
		}, 1000);
	}
};

init();
