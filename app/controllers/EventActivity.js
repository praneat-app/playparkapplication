this.name = Alloy.Globals.EVENT_ACTIVITY;

var isLoadEventList = false;

var PlayParkMobileAPI = require("/api/PlayParkMobileAPI");
var DateTime = require("/utils/DateTime");
var _progressBar = null;
var ProgressBar = require("utils/ProgressBar");
_progressBar = new ProgressBar({
	parentView : $.contentContainer
});

var MAX_TITLE_LENGTH = 40;
var tabGameViewStart = Alloy.Globals.LISTVIEW_DOUBLETAB_TOP - (Alloy.Globals.LISTVIEW_DOUBLETAB_TOP * 6);
var tabGameBool = 0;
var istabMenuBool = true;
//tabMenuLeft = true and tabMenuRight = false

var menugameArray;
var category;
var idSelect;

var Event = require("/utils/Event");

var gameId;
var limit = PlayParkMobileAPI.LIMIT;
var limitLength = 10;
var _isFirstLoadJson = true;

function init() {

	var GoogleAnalytics = require("/api/GoogleAnalytics");
	var googleAnalytics = new GoogleAnalytics();
	googleAnalytics.trackScreen("Event");

	Alloy.Globals.SideMenuButton = Alloy.Globals.ds.SIDEMENU_OTHER;
	Alloy.Globals.ds.rightButton.setBackgroundImage("/images/other/rightButton_background_default.png");

	if (Alloy.Globals.countryCode == Alloy.Globals.COUNTRY_VIETNAM) {
		MAX_TITLE_LENGTH = 27;
	}

	getContent();
	showProgressBar();

	$.tabGame.addEventListener("click", function(e) {
		onCheckTabGame();
	});

	$.tabGameListView.addEventListener("itemclick", function(e) {
		if (istabMenuBool) {
			refreshTabGameListview(e.itemIndex);
		}
	});

	$.tabMenuLeft.addEventListener("click", function(e) {
		showProgressBar();
		istabMenuBool = true;

		if (Alloy.Globals.OS_ANDROID) {
			$.tabGameView.setOpacity(1.0);
			$.widgetView.setTop(Alloy.Globals.LISTVIEW_DOUBLETAB_TOP);
		} else {
			$.tabGame.setOpacity(1.0);
			$.eventListView.setTop(Alloy.Globals.LISTVIEW_DOUBLETAB_TOP);

		}

		$.tabGameListView.setOpacity(1.0);

		//start
		$.tabMenuLeftView.setOpacity(1.0);
		$.tabMenuRightView.setOpacity(0.5);

		$.eventListView.removeAllChildren();

		tabGameBool = 1;
		onCheckTabGame();

		getMenugame();
	});

	$.tabMenuRight.addEventListener("click", function(e) {

		showProgressBar();

		tabGameBool = 1;
		onCheckTabGame();

		//start
		var eventDatabaseArrayNew = Ti.App.Properties.getList('eventDatabaseArray');
		//////////////////Ti.API.info('eventDatabaseArrayNew : ' + eventDatabaseArrayNew);

		if (eventDatabaseArrayNew != undefined && eventDatabaseArrayNew.length > 0) {
			istabMenuBool = false;

			$.tabMenuLeftView.setOpacity(0.5);
			$.tabMenuRightView.setOpacity(1.0);

			if (Alloy.Globals.OS_ANDROID) {
				$.tabGameView.setOpacity(0);
				$.widgetView.setTop(Alloy.Globals.TITLE_CONTAINER_HIGTH);
			} else {
				$.tabGame.setOpacity(0);
				$.eventListView.setTop(Alloy.Globals.TITLE_CONTAINER_HIGTH);
			}

			$.tabGameListView.setOpacity(0);

			$.eventListView.removeAllChildren();
			category = [];

			limit = PlayParkMobileAPI.LIMIT;
			_isFirstLoadJson = true;

			////setMaker////
			$.eventListView.setMarker({
				sectionIndex : 0,
				itemIndex : (limit - 1)
			});

			createListview(eventDatabaseArrayNew);
			hideProgressBar();

		} else {
			hideProgressBar();

			var alertDialog = Titanium.UI.createAlertDialog({
				title : Ti.Locale.getString("Alert_event_reminder_title_" + Alloy.Globals.LANGUAGE),
				message : Ti.Locale.getString("Alert_event_reminder_message_" + Alloy.Globals.LANGUAGE),
				buttonNames : [Ti.Locale.getString("Alert_event_ok_" + Alloy.Globals.LANGUAGE)],
				_row : e.itemIndex,
			});
			alertDialog.show();
		}

	});

	$.eventListView.addEventListener('itemclick', function(e) {

		if (category) {
			if (e.bindId == "listviewAlertImage") {
				//category[e.itemIndex].id
				var eventDatabaseArray = Ti.App.Properties.getList('eventDatabaseArray');

				var idEventDatabase = null;
				var rowEventDatabase = null;

				if (istabMenuBool) {

					var eventDatabase = Event.getIdEventFromDatabase(category[e.itemIndex].id, e.itemIndex);
					if (eventDatabase) {
						idEventDatabase = eventDatabase._dataIdEvent;
						rowEventDatabase = eventDatabase._row;
					};
					////////////////Ti.API.info('idEventDatabase : ' + idEventDatabase);
					////////////////Ti.API.info('rowEventDatabase : ' + rowEventDatabase);

				}
				else {

					if (eventDatabaseArray) {
						if (eventDatabaseArray.length > 0) {
							idEventDatabase = eventDatabaseArray[e.itemIndex].eventId;
							rowEventDatabase = e.itemIndex;
						}
					};

				}

				if (idEventDatabase) {

					var alertDialog = Titanium.UI.createAlertDialog({
						title : Ti.Locale.getString("Alert_event_delete_title_" + Alloy.Globals.LANGUAGE),
						message : Ti.Locale.getString("Alert_event_delete_message_" + Alloy.Globals.LANGUAGE),
						buttonNames : [Ti.Locale.getString("Alert_event_ok_" + Alloy.Globals.LANGUAGE), Ti.Locale.getString("Alert_event_cancel_" + Alloy.Globals.LANGUAGE)],
						cancel : 1,
						_row : rowEventDatabase,
						_dataIdEvent : idEventDatabase
					});

					alertDialog.show();
					alertDialog.addEventListener('click', function(e) {
						if (e.index == 0) {

							var dataIdEvent = e.source._dataIdEvent;
							Event.deleteEventFromIdEvent(dataIdEvent, e.source._row);

							if (!istabMenuBool) {
								//////////////////Ti.API.info('eventListView >>> deleteItemsAt');
								$.section.deleteItemsAt(e.source._row, 1);
							}
						}
					});

				} else {

					var alertDialog = Titanium.UI.createAlertDialog({
						title : Ti.Locale.getString("Alert_event_title_" + Alloy.Globals.LANGUAGE),
						message : Ti.Locale.getString("Alert_event_message_" + Alloy.Globals.LANGUAGE),
						buttonNames : [Ti.Locale.getString("Alert_event_ok_" + Alloy.Globals.LANGUAGE), Ti.Locale.getString("Alert_event_cancel_" + Alloy.Globals.LANGUAGE)],
						cancel : 1,
						_row : e.itemIndex,
					});

					alertDialog.show();
					alertDialog.addEventListener('click', function(e) {
						if (e.index == 0) {
							if (category) {
								Event.addEvent(category[e.source._row]);
							};
						};
					});
				}

			} else if (e.bindId == "eventListViewNameLabel") {
				var gameId = category[e.itemIndex].gameid;
				checkRowNews(gameId);
			} else {
				//////////////Ti.API.info('category[e.itemIndex] : ' + category[e.itemIndex]);
				var html = category[e.itemIndex].html;
				var title = category[e.itemIndex].title;
				var link = category[e.itemIndex].link;

				var eventDetail = Alloy.createController('detail/EventDetail', {
					object : category[e.itemIndex],
					htmlObject : html,
					titleShare : title,
					linkShare : link
				});
				eventDetail.getView().open();
			}
		}
	});

	////setMaker////
	$.eventListView.setMarker({
		sectionIndex : 0,
		itemIndex : (limit - 1)
	});

	$.eventListView.addEventListener('marker', function(e) {
		var max = limit + limitLength;
		limit = limit + 1;
		getEventMaker(gameId, limit, max);
		//////////////////Ti.API.info('eventListView >>> maker >>> max : ' + max + ", limit : " + limit + ", limitLength : " + limitLength);
	});

}

var getContent = function() {
	$.tabGameListView.setTop(tabGameViewStart);

	$.tabGame.setTitle(Ti.Locale.getString("other_choose_game_title_" + Alloy.Globals.LANGUAGE));
	//tabMenu
	$.tabMenuLeftLabel.setText(Ti.Locale.getString("event_tab_left_" + Alloy.Globals.LANGUAGE));
	$.tabMenuRightLabel.setText(Ti.Locale.getString("event_tab_right_" + Alloy.Globals.LANGUAGE));

	$.tabMenuLeftView.setOpacity(1.0);
	$.tabMenuRightView.setOpacity(0.5);

	getMenugame();
};

var refreshControl = function(e) {
	if (istabMenuBool) {
		$.ptr.show();
		refreshTabGameListview(idSelect);
		showProgressBar();
		setTimeout(function() {
			$.ptr.hide();
			hideProgressBar();
		}, 2000);
	} else {
		$.ptr.hide();

	}
};

var checkRowNews = function(_idSelect) {
	var row = null;
	for (var i in menugameArray) {
		var object = JSON.stringify(menugameArray[i]);
		var data = JSON.parse(object);
		if (data.id == _idSelect) {
			row = i;
		}
	}
	$.eventListView.removeAllChildren();
	refreshTabGameListview(row);
};
var refreshTabGameListview = function(_itemIndex) {
	showProgressBar();
	tabGameBool = 1;
	onCheckTabGame();
	createGameListview(menugameArray, _itemIndex);

	////resetLimit////
	limit = PlayParkMobileAPI.LIMIT;
	_isFirstLoadJson = true;

	////setMaker////
	$.eventListView.setMarker({
		sectionIndex : 0,
		itemIndex : (limit - 1)
	});

	if (_itemIndex != undefined) {
		getEvent(menugameArray[_itemIndex].id);
	} else {
		getEvent();
	}
};

var getMenugame = function() {
	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		PlayParkMobileAPI.getMenugame(getMenugameCallback);
	} else {
		hideProgressBar();
		alert(Ti.Locale.getString("network_none_" + Alloy.Globals.LANGUAGE));
	}
};
var getMenugameCallback = function(_data) {

	if (_data) {
		if (_data.code == PlayParkMobileAPI.SUCCESS) {
			var message = _data.result.message;
			menugameArray = _data.result.menugame;


			if (!isLoadEventList) {
				refreshTabGameListview(null);
			}

		} else {
			menugameArray = null;

			var dialog = Ti.UI.createAlertDialog({
				cancel : 0,
				buttonNames : ['Cancel', 'OK'],
				message : Ti.Locale.getString("api_loading_failed_title"),
				title : Ti.Locale.getString("api_loading_failed_try_message")
			});
			dialog.addEventListener('click', function(e) {
				if (e.index !== e.source.cancel) {
					showProgressBar();
					getMenugame();
				}
			});
			dialog.show();
			hideProgressBar();
		}
	}

};

var getEvent = function(_itemId) {
	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		$.eventListView.removeAllChildren();
		PlayParkMobileAPI.getEvent(getEventCallback, _itemId);
	} else {
		hideProgressBar();
	}
};

var getEventCallback = function(_data) {
	if (_data) {
		if (_data.code == PlayParkMobileAPI.SUCCESS) {
			var message = _data.result.message;
			category = _data.result.category;
			createListview(category);
			hideProgressBar();
		} else {
			hideProgressBar();
			alert(Ti.Locale.getString("data_not_found_" + Alloy.Globals.LANGUAGE));
		}
	}
};
/////event Maker//////
var getEventMaker = function(_gameId, _to, _from) {
	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		PlayParkMobileAPI.getEvent(getEventMakerCallback, _gameId, null, _to, _from);
	} else {
		limit = limit - 1;
	}
};

var getEventMakerCallback = function(_data) {
	if (_data) {
		if (_data.code == PlayParkMobileAPI.SUCCESS) {
			var message = _data.result.message;
			var categoryMaker = _data.result.category;
			createListview(categoryMaker);
		} else {
			limit = limit - 1;
			//////////////////Ti.API.info('EventActivity ==> getEventMakerCallback limit : ' + limit);

		}
	}
};
//////////////////////////

var createListview = function(_data) {
	//////////////////Ti.API.info('_data length : ' + _data.length);

	if (!_data) {
		return;
	}

	var items = [];
	for (var i in _data) {
		var data = null;
		if (istabMenuBool) {

			if (!_isFirstLoadJson) {
				category[category.length] = _data[i];
			}
			var object = JSON.stringify(_data[i]);
			data = JSON.parse(object);
		} else {
			category[category.length] = _data[i].eventCalendar;
			var object = JSON.stringify(_data[i].eventCalendar);
			data = JSON.parse(object);
		}
		var dateStart;
		var timeStart;
		if (data.date_start) {
			dateStart = DateTime.getEventDateTime(data.date_start, DateTime.MODE_DATE);
			timeStart = DateTime.getEventDateTime(data.date_start, DateTime.MODE_TIME);
		}
		var dateEnd;
		var timeEnd;

		if (data.date_end) {
			dateEnd = DateTime.getEventDateTime(data.date_end, DateTime.MODE_DATE);
			timeEnd = DateTime.getEventDateTime(data.date_end, DateTime.MODE_TIME);
		};

		var title = Titanium.Network.decodeURIComponent(data.title);

		if (title) {
			if (title.length > MAX_TITLE_LENGTH) {
				title = title.substr(0, MAX_TITLE_LENGTH).trim() + "...";
			}
		}

		items.push({
			template : "template", // set the template
			eventListViewNameLabel : {
				text : data.gametype
			},
			eventListViewImageView : {
				image : data.image
			},
			eventDateStart : {
				text : dateStart
			},
			eventTimeStart : {
				text : timeStart
			},
			eventDateFinish : {
				text : dateEnd
			},
			eventTimeFinish : {
				text : timeEnd
			},
			eventListViewDescipteLabel : {
				text : title
			},
			eventDateTimeText : {
				text : Ti.Locale.getString("event_datetime_" + Alloy.Globals.LANGUAGE),
				left : 23
			},
			eventDateTimeTextTo : {
				text : Ti.Locale.getString("event_datetime_to_" + Alloy.Globals.LANGUAGE),
			}

		});
	}

	if (_isFirstLoadJson) {

		$.section.setItems(items);
		setTimeout(function() {
			$.eventListView.scrollToItem(0, 0, Titanium.UI.iPhone.RowAnimationStyle.TOP);
		}, 500);

		_isFirstLoadJson = false;

	} else {
		$.section.appendItems(items);
		limit = limit + limitLength - 1;
		//////////////////Ti.API.info('limit : ' + limit);

		$.eventListView.setMarker({
			sectionIndex : 0,
			itemIndex : (limit - 1)
		});
	}

};

var onCheckTabGame = function() {
	if (tabGameBool != 0) {
		$.tabGameListView.animate({
			top : tabGameViewStart,
			duration : Alloy.Globals.TIME_DURATION
		});
		$.tabGameImage.setImage("/images/other/other_tabgame_image.png");
		tabGameBool = 0;
	} else {
		$.tabGameListView.animate({
			top : Alloy.Globals.LISTVIEW_DOUBLETAB_TOP,
			duration : Alloy.Globals.TIME_DURATION
		});
		$.tabGameImage.setImage("/images/other/other_tabgame_image_select.png");
		tabGameBool = 1;
	}
};

var createGameListview = function(_data, _idSelect) {
	idSelect = _idSelect;
	////////////////////////////////////Ti.API.info('createGameListview  idSelect : ' + idSelect + ", idSelect : " + _idSelect);
	if (!_data) {
		return;
	}

	var arrayLength = _data.length;
	if (arrayLength >= 6) {
		arrayLength = 6;
	};

	var lisviewHeight = (Alloy.Globals.CONTAINER_HIGTH * arrayLength);
	var tabGameViewStart1 = Alloy.Globals.TITLE_CONTAINER_HIGTH - lisviewHeight;

	$.tabGameListView.setTop(tabGameViewStart1);
	$.tabGameListView.setHeight(lisviewHeight);

	var backgroudSelect = "/images/other/other_tabgame_toggle_bg_select.png";
	var textColor = "white";

	var items = [];
	for (var i in _data) {
		var object = JSON.stringify(_data[i]);
		var data = JSON.parse(object);

		if (_idSelect == i) {
			$.tabGame.setTitle(data.name);
			gameId = data.id;

			backgroudSelect = "/images/other/other_tabgame_toggle_bg_select.png";
			textColor = "white";

		} else {
			backgroudSelect = "transparent";
			textColor = "#707070";
		}

		items.push({
			template : "tabMenuGameTemplate",
			tabGameTitle : {
				text : data.name,
				color : textColor
			},
			tabGameListViewBg : {
				backgroundImage : backgroudSelect
			}
		});

	}
	$.tabGameSection.setItems(items);

};

var showProgressBar = function() {
	_progressBar.show();
};

var hideProgressBar = function() {
	if (_progressBar != null) {
		setTimeout(function() {
			_progressBar.hide();
		}, 1000);
	}
};

init();
