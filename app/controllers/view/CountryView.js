var countrySelected = Alloy.Globals.COUNTRY_THAILAND;
var buttonImage;

var PlayParkMobileAPI = require("/api/PlayParkMobileAPI");

var _progressBar = null;
var ProgressBar = require("utils/ProgressBar");
_progressBar = new ProgressBar({
	parentView : $.contentContainer
});

var onOpen = function() {
	//////////////////////////////////Ti.API.info("----HomeActivity open-----");
	getCountry();
};

function init() {

	showProgressBar();

	$.windowDetail.addEventListener("open", onOpen);
	$.windowDetail.addEventListener('close', function(e) {
		$.windowDetail.removeEventListener("open", onOpen);
	});

	if (Alloy.Globals.OS_ANDROID) {
		var StringAdapter = require("/utils/StringAdapter");
		//Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.itemlistview_margin_tb)
		var logoImageViewWidth = Alloy.Globals.SCREEN_WIDTH - (StringAdapter.getDimension(Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.sidemenu_logo_padding_rl)) * 2);
		var logoImageViewHeight = (logoImageViewWidth * 140 ) / 500;

		////////////////////Ti.API.info(' Alloy.Globals.SCREEN_WIDTH : ' + Alloy.Globals.SCREEN_WIDTH);
		////////////////////Ti.API.info(' StringAdapter.getDimension(Ti.App.Android.R.dimen.sidemenu_logo_padding_rl) : ' + StringAdapter.getDimension(Ti.App.Android.R.dimen.sidemenu_logo_padding_rl));
		////////////////////Ti.API.info(' Alloy.Globals.SCREEN_HEIGH : ' + Alloy.Globals.SCREEN_WIDTH);

		////////////////////Ti.API.info('logoImageViewWidth : ' + logoImageViewWidth);
		////////////////////Ti.API.info('logoImageViewHeight : ' + logoImageViewHeight);

		$.logoImageView.setHeight(logoImageViewHeight);
		$.logoImageView.setWidth(logoImageViewWidth);
	};

	$.countryListview.addEventListener("itemclick", function(e) {

		//set language
		if (Alloy.Globals.CountryArray[e.itemIndex].id == Alloy.Globals.COUNTRY_THAILAND) {
			Alloy.Globals.LANGUAGE = Alloy.Globals.LANGUAGE_TH;

		} else {
			Alloy.Globals.LANGUAGE = Alloy.Globals.LANGUAGE_EN;
		}

		Alloy.Globals.countryCode = Alloy.Globals.CountryArray[e.itemIndex].id;
		Ti.App.Properties.setString('country', Alloy.Globals.CountryArray[e.itemIndex].id);

		if (Alloy.Globals.OS_IOS) {

			var Country = require("utils/Country");
			Country.setVietnameFontSize();

		}
		var PushNotification = require("utils/Pushnotification");
		PushNotification.registerPushNotification(PushNotification.MODE_SUBSCRIBETOKEN);
		Ti.App.Properties.setBool('noti', true);
		
		Alloy.Globals.initHighlightActivity(false);
	});
};

var getCountry = function() {
	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		PlayParkMobileAPI.getCountry(getCountryCallback);
	} else {
		alert(Ti.Locale.getString("network_none_" + Alloy.Globals.LANGUAGE));
		hideProgressBar();
	}
};

var getCountryCallback = function(_data) {
	//////////////////////////////////Ti.API.info('getCountryCallback  >>>>>>  data : ' + _data.code);

	if (_data.code == PlayParkMobileAPI.SUCCESS) {
		var message = _data.result.message;
		Alloy.Globals.CountryArray = _data.result.country;
		createCountryListView(Alloy.Globals.CountryArray);
		hideProgressBar();
	} else {
		Alloy.Globals.CountryArray = null;
		var dialog = Ti.UI.createAlertDialog({
			cancel : 0,
			buttonNames : ['Cancel', 'OK'],
			message : Ti.Locale.getString("api_loading_failed_title"),
			title : Ti.Locale.getString("api_loading_failed_try_message")
		});
		dialog.addEventListener('click', function(e) {
			if (e.index !== e.source.cancel) {
				showProgressBar();
				getCountry();
			}
		});
		dialog.show();

		//alert(Ti.Locale.getString("data_not_found_" + Alloy.Globals.LANGUAGE));
		hideProgressBar();
	}
};

var createCountryListView = function(_data) {
	//////////////////////////////////Ti.API.info('_data : ' + _data);

	var items = [];
	for (var i in _data) {
		var object = JSON.stringify(_data[i]);
		var data = JSON.parse(object);
		items.push({
			template : "template", // set the template
			settingIcon : {
				image : "/images/setting/setting_country_" + _data[i].id + ".png"//_data[i].images//countryImageArray[i]
			},
			countryText : {
				text : _data[i].name_en
			}
		});

	}
	$.section.setItems(items);

};

var showProgressBar = function() {
	_progressBar.show();
};

var hideProgressBar = function() {
	if (_progressBar != null) {
		setTimeout(function() {
			_progressBar.hide();
		}, 1000);
	}
};

init();

