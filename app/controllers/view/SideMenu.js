this.name = Alloy.Globals.SIDEMENU_ACTIVITY;

var PlayParkMobileAPI = require("/api/PlayParkMobileAPI");
var sideMenuObject = [];
var items = [0, 0, 0, 0, 0];

function init() {

	Alloy.Globals.createSideMenu();

	var StringAdapter = require("/utils/StringAdapter");
	var navLogoLeft;

	if (Alloy.Globals.OS_IOS) {
		navLogoLeft = (Alloy.Globals.menuLeftWidth / 2 ) - (153 / 2);
	} else {
		navLogoLeft = (Alloy.Globals.menuLeftWidth - Math.floor(StringAdapter.getDimension(Alloy.Globals.NAV_LOGO_WIDTH))) / 2;
	}

	$.navLogo.setLeft(navLogoLeft);

}

Alloy.Globals.createSideMenu = function() {

	var lastTimeArray = Ti.App.Properties.getList('lastTimeArray');
	////////////////////Ti.API.info('itemclick  >>>  lastTimeArray : ' + lastTimeArray);

	for (var i in lastTimeArray) {
		////////////////////Ti.API.info('createSideMenu >>> lastTimeArray i : ' + i + " , lastTimeArray :" + lastTimeArray[i]);
	}
	getNotification(lastTimeArray);

};

var getNotification = function(_dateTimeArray) {
	/*
	 if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
	 PlayParkMobileAPI.getNotification(getNotificationCallback, _dateTimeArray);
	 } else {
	 if (Alloy.Globals.LANGUAGE == Alloy.Globals.LANGUAGE_TH) {
	 createJsonObjectTH(items);
	 } else {
	 createJsonObjectEN(items);
	 }
	 }*/

	if (Alloy.Globals.LANGUAGE == Alloy.Globals.LANGUAGE_TH) {
		createJsonObjectTH(items);
	} else {
		createJsonObjectEN(items);
	}
};

var getNotificationCallback = function(data) {
	////////////////////Ti.API.info('data : ' + data);

	if (data.code == PlayParkMobileAPI.SUCCESS) {
		var message = data.result.message;
		var sideMenuArray = data.result.sidemenu;
		if (sideMenuArray) {
			for (var i in sideMenuArray) {
				var object = JSON.stringify(sideMenuArray[i]);
				var data = JSON.parse(object);
				items[i - 1] = data.count;
				////////////////////Ti.API.info('i : ' + i);
			}

			if (Alloy.Globals.LANGUAGE == Alloy.Globals.LANGUAGE_TH) {
				createJsonObjectTH(items);
			} else {
				createJsonObjectEN(items);
			}
		};
	} else {
		if (Alloy.Globals.LANGUAGE == Alloy.Globals.LANGUAGE_TH) {
			createJsonObjectTH(items);
		} else {
			createJsonObjectEN(items);
		}
	}
};

var createJsonObjectTH = function(_items) {

	////////////////////Ti.API.info('_items[0] : ' + _items[0]);
	////////////////////Ti.API.info('_items[1] : ' + _items[1]);
	////////////////////Ti.API.info('_items[2] : ' + _items[2]);
	////////////////////Ti.API.info('_items[3] : ' + _items[3]);
	////////////////////Ti.API.info('_items[4] : ' + _items[4]);

	var jsonObject = {
		"menu" : [{
			"thmb" : "/images/sideMenu/sidemenu_icon1.png",
			"title" : Ti.Locale.getString("highlight_" + Alloy.Globals.LANGUAGE),
			"page" : "HighLightActivity",
			"count" : 0

		}, {
			"thmb" : "/images/sideMenu/sidemenu_icon2.png",
			"title" : Ti.Locale.getString("news_" + Alloy.Globals.LANGUAGE),
			"page" : "NewsActivity",
			"count" : _items[0]
		}, {
			"thmb" : "/images/sideMenu/sidemenu_icon3.png",
			"title" : Ti.Locale.getString("promotion_" + Alloy.Globals.LANGUAGE),
			"page" : "PromotionActivity",
			"count" : _items[1]
		}, {
			"thmb" : "/images/sideMenu/sidemenu_icon4.png",
			"title" : Ti.Locale.getString("event_" + Alloy.Globals.LANGUAGE),
			"page" : "EventActivity",
			"count" : _items[2],
		}, {
			"thmb" : "/images/sideMenu/sidemenu_icon5.png",
			"title" : Ti.Locale.getString("video_" + Alloy.Globals.LANGUAGE),
			"page" : "VideoActivity",
			"count" : _items[3]
		}, {
			"thmb" : "/images/sideMenu/sidemenu_icon6.png",
			"title" : Ti.Locale.getString("live_" + Alloy.Globals.LANGUAGE),
			"page" : "StreamingActivity",
			"count" : _items[4],
		}, {
			"thmb" : "/images/sideMenu/sidemenu_icon7.png",
			"title" : Ti.Locale.getString("game_" + Alloy.Globals.LANGUAGE),
			"page" : "GameAppsActivity",
			"count" : 0

		}, {
			"thmb" : "/images/sideMenu/sidemenu_icon8.png",
			"title" : Ti.Locale.getString("setting_" + Alloy.Globals.LANGUAGE),
			"page" : "SettingActivity",
			"count" : 0

		}, {
			"thmb" : "/images/sideMenu/sidemenu_icon1.png",
			"title" : Ti.Locale.getString("contact_" + Alloy.Globals.LANGUAGE),
			"page" : "ContactActivity",
			"count" : 0

		}]
	};
	var myObjectString = JSON.stringify(jsonObject);
	var myNewObject = JSON.parse(myObjectString);
	var newsArray = myNewObject.menu;
	createSideMenu(newsArray);
};
var createJsonObjectEN = function(_items) {

	var jsonObject = {
		"menu" : [{
			"thmb" : "/images/sideMenu/sidemenu_icon1.png",
			"title" : Ti.Locale.getString("highlight_" + Alloy.Globals.LANGUAGE),
			"page" : "HighLightActivity",
			"count" : 0

		}, {
			"thmb" : "/images/sideMenu/sidemenu_icon2.png",
			"title" : Ti.Locale.getString("news_" + Alloy.Globals.LANGUAGE),
			"page" : "NewsActivity",
			"count" : _items[0]
		}, {
			"thmb" : "/images/sideMenu/sidemenu_icon3.png",
			"title" : Ti.Locale.getString("promotion_" + Alloy.Globals.LANGUAGE),
			"page" : "PromotionActivity",
			"count" : _items[1]
		}, {
			"thmb" : "/images/sideMenu/sidemenu_icon4.png",
			"title" : Ti.Locale.getString("event_" + Alloy.Globals.LANGUAGE),
			"page" : "EventActivity",
			"count" : _items[2],
		}, {
			"thmb" : "/images/sideMenu/sidemenu_icon5.png",
			"title" : Ti.Locale.getString("video_" + Alloy.Globals.LANGUAGE),
			"page" : "VideoActivity",
			"count" : _items[3]
		}, {
			"thmb" : "/images/sideMenu/sidemenu_icon7.png",
			"title" : Ti.Locale.getString("game_" + Alloy.Globals.LANGUAGE),
			"page" : "GameAppsActivity",
			"count" : 0
		}, {
			"thmb" : "/images/sideMenu/sidemenu_icon8.png",
			"title" : Ti.Locale.getString("setting_" + Alloy.Globals.LANGUAGE),
			"page" : "SettingActivity",
			"count" : 0
		}, {
			"thmb" : "/images/sideMenu/sidemenu_icon1.png",
			"title" : Ti.Locale.getString("contact_" + Alloy.Globals.LANGUAGE),
			"page" : "ContactActivity",
			"count" : 0

		}]
	};

	var myObjectString = JSON.stringify(jsonObject);
	var myNewObject = JSON.parse(myObjectString);
	var newsArray = myNewObject.menu;
	createSideMenu(newsArray);
};

function rowSelect(_row) {
	////////////////////Ti.API.info('sideMenuObject : ' + sideMenuObject + ", _row : " + _row);

	var controller = Alloy.createController(sideMenuObject[_row].page);
	var newCurrentView = controller.getView();

	if (newCurrentView != Alloy.Globals.currentViewRight) {
		Alloy.Globals.ds.contentview.removeAllChildren();
		Alloy.Globals.currentViewRight.removeAllChildren();
		Alloy.Globals.currentViewRight = null;

		Alloy.Globals.currentController = controller;
		Alloy.Globals.currentViewRight = newCurrentView;
		Alloy.Globals.ds.contentview.add(Alloy.Globals.currentViewRight);
	}
}

Alloy.Globals.addNavTitle = function(_row) {

	Alloy.Globals.ds.navTitle.removeAllChildren();
	////////////////Ti.API.info('addNavTitle >>>> row : ' + _row);

	if (_row == 0) {
		Alloy.Globals.SideMenuButton = Alloy.Globals.ds.SIDEMENU_COUNTRY;
		Alloy.Globals.ds.navTitle.add(Alloy.Globals.navLogo);

		var countryImage = "/images/setting/setting_country_" + Alloy.Globals.countryCode + ".png";
		//Alloy.Globals.ds.rightButton.setImage(countryImage);

		if (Alloy.Globals.OS_IOS) {
			if (Alloy.Globals.OS_IOS_IPAD) {
				Alloy.Globals.ds.rightButton.setWidth(31);
				Alloy.Globals.ds.rightButton.setHeight(21);
			} else {
				Alloy.Globals.ds.rightButton.setWidth(25);
				Alloy.Globals.ds.rightButton.setHeight(17);
			}
		} else {
			Alloy.Globals.ds.rightButton.setWidth(25);
			Alloy.Globals.ds.rightButton.setHeight(17);
		}

	} else {
		Alloy.Globals.navLabel.setText(sideMenuObject[_row].title);
		Alloy.Globals.ds.navTitle.add(Alloy.Globals.navLabel);
		Alloy.Globals.ds.rightButton.setImage("");
	}
};

$.menuListView.addEventListener('itemclick', function(e) {
	////////////////////Ti.API.info('menuListView >>> e.itemIndex : ' + e.itemIndex);

	Alloy.Globals.ds.toggleLeftSlider();
	rowSelect(e.itemIndex);
	Alloy.Globals.addNavTitle(e.itemIndex);

	/////////////////////////////////////////

	var lastTimeArray = Ti.App.Properties.getList('lastTimeArray');

	var dateTimeNow = new Date();
	var DateTime = require("/utils/DateTime");
	dateTimeNow = DateTime.getDateTimeForNotification(dateTimeNow);
	lastTimeArray[e.itemIndex - 1] = dateTimeNow;
	Ti.App.Properties.setList('lastTimeArray', lastTimeArray);

	Alloy.Globals.createSideMenu();

	////////////////////////////////////////
	/*var item = e.section.getItemAt(e.itemIndex);
	item.circle.height = 0;
	item.circle.width = 0;
	item.localNotiLabel.text = "";
	e.section.updateItemAt(e.itemIndex, item);*/

});

var createSideMenu = function(_data) {
	sideMenuObject = _data;
	////////////////////Ti.API.info('sideMenuObject : ' + sideMenuObject);

	if (Alloy.Globals.OS_ANDROID) {
		var sidemenuIconWidth = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.sidemenu_icon_width);
	}

	var items = [];
	var circlePadding = Alloy.Globals.menuLeftWidth - 30 - 10;

	for (var i in _data) {
		var object = JSON.stringify(_data[i]);
		var data = JSON.parse(object);
		var circleColor = "transparent";
		var count = data.count;

		if (count == undefined || count == 0) {
			circleColor = "transparent";
			count = "";
		} else {
			circleColor = "#f94242";
		}

		if (Alloy.Globals.OS_IOS) {

			items.push({
				template : "template", // set the template
				thumbnail : {
					image : data.thmb
				},
				title : {
					text : data.title
				},
				circle : {
					backgroundColor : circleColor,
					left : circlePadding
				},
				localNotiLabel : {
					text : count
				}
			});
		} else {

			items.push({
				template : "template", // set the template
				thumbnail : {
					image : data.thmb
				},
				title : {
					left : sidemenuIconWidth,
					text : data.title
				},
				circle : {
					backgroundColor : circleColor,
					left : circlePadding
				},
				localNotiLabel : {
					text : count
				}
			});
		}
	}

	$.sectionSideMenu.setItems(items);
};

init();
