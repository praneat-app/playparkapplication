var args = arguments[0] || {};
var titleShare = args.titleShare || '';
var linkShare = args.linkShare || '';

var win = Ti.UI.createWindow({
	barColor : '#000',
	backgroundColor : "transparent"
});

var textToShare = encodeURIComponent(titleShare);
var urlToShare = encodeURIComponent(linkShare);

var webViewHeigth;
if (Alloy.Globals.OS_IOS) {
	webViewHeigth = 350;
	if (Alloy.Globals.OS_IOS_IPAD) {
		webViewHeigth = 800;
	}
} else {
	webViewHeigth = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.social_googleplus_height);
	////////////////////Ti.API.info('social_googleplus_height : ' + webViewHeigth);
}

var webView = Ti.UI.createWebView({
	url : 'https://plus.google.com/share?client_id=378795929539-mdi8fvsaccfjtbvqrhu4ksqtc57747re.apps.googleusercontent.com&continue=' + Ti.App.id + '%3A%2F%2Fshare%2F&text=' + textToShare + '&url=' + urlToShare + '&bundle_id=' + Ti.App.id + '&gpsdk=1.0.0',
	width : Ti.UI.FILL,
	height : webViewHeigth
});

win.add(webView);

var close = Ti.UI.createButton({
	title : ' x ',
	right : 0,
	top : 20,
	backgroundColor : "transparent",
	textAlign : "left",
	color : "white",
	font : {
		fontSize : "40sp",
		fontFamily : Alloy.Globals.customFontThaiSansNeue,
		fontWeight : "bold"
	},
});

close.addEventListener('click', function() {
	win.close();
});
win.open({
	modal : true
});
win.add(close);

webView.addEventListener('load', function(e) {

	if (e.url.indexOf('https://accounts.google.com') == -1) {
		if (Alloy.Globals.OS_IOS) {
			win.hideNavBar();
		}
		//////////////////////////////////////Ti.API.info('https://accounts.google.com');
	}
	else {
		if (Alloy.Globals.OS_IOS) {
			win.showNavBar();
		}
		win.setLeftNavButton(close);
		//////////////////////////////////////Ti.API.info('close');
	}
});

webView.addEventListener('error', function(e) {
	win.close();
});
