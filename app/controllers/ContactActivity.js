this.name = Alloy.Globals.CONTACT_ACTIVITY;

var linkFacebookScheme = "fb://profile/283221407682";
var linkFacebook = "https://www.facebook.com/PlayparkNews";
var linkTwitter = "https://twitter.com/playpark";
var linkYoutube = "https://www.youtube.com/user/PlayparkTV";
var linkLine = "http://line.naver.jp/ti/p/%40playpark";
var linkEmail = "http://1750.asiasoft.co.th/support/dp/dp.html";
var linkWeb = "http://www.playpark.com";

//http://1750.asiasoft.co.th/support/dp/dp.html

function init() {

	var GoogleAnalytics = require("/api/GoogleAnalytics");
	var googleAnalytics = new GoogleAnalytics();
	googleAnalytics.trackScreen("Contact");

	////////Point/////

	Alloy.Globals.SideMenuButton = Alloy.Globals.ds.SIDEMENU_OTHER;
	Alloy.Globals.ds.rightButton.setBackgroundImage("/images/other/rightButton_background_default.png");

	var companyName = Ti.Locale.getString("Contact_company_name_" + Alloy.Globals.countryCode);
	var companyAddress = Ti.Locale.getString("Contact_company_address_" + Alloy.Globals.countryCode);
	var companyTel = Ti.Locale.getString("Contact_company_tel_" + Alloy.Globals.countryCode);
	var latitudePoint = Ti.Locale.getString("Contact_company_latitude_point_" + Alloy.Globals.countryCode);
	var longitudePoint = Ti.Locale.getString("Contact_company_longitude_point_" + Alloy.Globals.countryCode);

	////////////////Ti.API.info('latitudePoint : ' + latitudePoint);
	////////////////Ti.API.info('longitudePoint : ' + longitudePoint);

	$.contactCompany.setText(companyName);
	$.contactAddress.setText(companyAddress);
	$.contactTelephone.setText(companyTel);
	$.contactVersion.setText(Ti.Locale.getString("Contact_company_ver"));

	var itemListviewMarginLeftRight = 8;
	if (Alloy.Globals.OS_IOS) {
		itemListviewMarginLeftRight = 8;
		if (Alloy.Globals.OS_IOS_IPAD) {
			itemListviewMarginLeftRight = 15;
		}
	}

	var MapModule = require('ti.map');

	if (Alloy.Globals.OS_ANDROID) {

		var StringAdapter = require("/utils/StringAdapter");
		var contactMapHeight = ((Alloy.Globals.SCREEN_WIDTH - (StringAdapter.getDimension(Alloy.Globals.itemlistview_margin_lr) * 4)) * 395) / 570;
		var contactLogoWidth = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.contact_logo_width);
		var contactLogoHeight = ((StringAdapter.getDimension(contactLogoWidth) * 63) / 144);

		$.contactMap.setHeight(contactMapHeight);
		$.contactLogo.setWidth(contactLogoWidth);
		$.contactLogo.setHeight(contactLogoHeight);

		var image = "/images/contact/contact_map_" + Alloy.Globals.countryCode + ".png";
		////////////Ti.API.info('image : ' + image);
		$.contactMap.setBackgroundImage(image);

		$.contactMap.addEventListener("touchstart", function(e) {
			var mapWindow = Alloy.createController('detail/MapView', {
				latitude : latitudePoint,
				longitude : longitudePoint,
				title : companyName,
				subtitle : companyName,
			});
			mapWindow.getView();
		});

	} else {

		var contactMapWidth = (Alloy.Globals.SCREEN_WIDTH - (itemListviewMarginLeftRight * 2));
		var contactMapHeight = (contactMapWidth * 395) / 570;
		$.contactMap.setHeight(contactMapHeight);
		//////////////////Ti.API.info('contactMapHeight : ' + contactMapHeight);
		/*
		var imagePin = Ti.UI.createImageView({
		height : 20,
		width : 20,
		image : "/images/contact/contact_pin.png"
		});*/

		var bridge = MapModule.createAnnotation({
			latitude : latitudePoint,
			longitude : longitudePoint,
			//customView : imagePin,
			pincolor : MapModule.ANNOTATION_RED,
			title : companyName,
			subtitle : companyName,
			leftButton : Ti.UI.iPhone.SystemButton.ADD
		});

		var mapview = MapModule.createView({
			mapType : MapModule.NORMAL_TYPE,
			region : {
				latitude : latitudePoint,
				longitude : longitudePoint,
				latitudeDelta : 0,
				longitudeDelta : 0
			},
			annotations : [bridge]
		});

		$.contactMap.add(mapview);
	}

	$.tabFacebookButtonC.addEventListener("click", function() {
		if (Alloy.Globals.OS_IOS) {
			if (Ti.Platform.canOpenURL(linkFacebookScheme)) {
				Ti.Platform.openURL(linkFacebookScheme);
			} else {
				Ti.Platform.openURL(linkFacebook);
			}
		} else {
			Titanium.Platform.openURL(linkFacebook);
		}
	});

	$.tabTwitterButtonC.addEventListener("click", function() {
		Titanium.Platform.openURL(linkTwitter);
	});

	$.tabYoutubeButton.addEventListener("click", function() {
		Titanium.Platform.openURL(linkYoutube);
	});

	$.tabLineButtonC.addEventListener("click", function() {
		Titanium.Platform.openURL(linkLine);
	});

	$.tabMailButton.addEventListener("click", function() {
		/*var emailDialog = Titanium.UI.createEmailDialog();
		 emailDialog.subject = "";
		 emailDialog.toRecipients = [linkEmail];
		 emailDialog.open();*/

		Titanium.Platform.openURL(linkEmail);
	});

	$.tabWebButton.addEventListener("click", function() {
		Titanium.Platform.openURL(linkWeb);
	});

	if (Alloy.Globals.countryCode == Alloy.Globals.COUNTRY_THAILAND) {
		var contactTelButton = Titanium.UI.createView();
		$.addClass(contactTelButton, "contactTelButton");

		var contactTelImage = Titanium.UI.createImageView();
		$.addClass(contactTelImage, "contactTelImage");

		contactTelImage.addEventListener("click", function(e) {
			Titanium.Platform.openURL('tel:1750');
		});

		contactTelButton.add(contactTelImage);
		$.contentContainer.add(contactTelButton);

		if (Alloy.Globals.OS_IOS) {
			$.contactContentView.setBottom(58);
		} else {

			var contactHeight = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.contact_tel_button_height);
			var contactBottom = (StringAdapter.getDimension(Alloy.Globals.contact_tel_button_height) * 2) + (StringAdapter.getDimension(Alloy.Globals.itemlistview_margin_lr) * 2);

			//////////////Ti.API.info('contactHeight : ' + contactHeight);
			//////////////Ti.API.info('contactBottom : ' + contactBottom);
			contactTelButton.setHeight(contactHeight);
			contactTelButton.setBottom(0);
			$.contactContentView.setBottom(contactHeight);

		}

	}
};

init();
