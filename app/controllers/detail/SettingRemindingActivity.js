var Calendar = require("/utils/Calendar");
var DateTime = require("/utils/DateTime");

function init() {

	$.navBarText.setText(Ti.Locale.getString("setting_event_reminder_" + Alloy.Globals.LANGUAGE));
	$.backButton.addEventListener("click", function(e) {
		$.windowDetail.close();
	});

	var events = Calendar.getEventForCalendar();
	////////////////////Ti.API.info('events : ' + events.length);
	createTableView(events);

	$.table.addEventListener('click', function(e) {
		var eventDatabaseArray = Ti.App.Properties.getList('eventDatabaseArray');
		if (e.source.id == "btn") {
			var row = e.index;
			////////////////////Ti.API.info('row : ' + row);
			////////////////////Ti.API.info('e.rowData._objectid : ' + e.rowData._objectid);
			////////////////////Ti.API.info('e.rowData._object.eventId : ' + e.rowData._object.eventId);

			$.table.deleteRow(row, 0);
			//ok

			if (Alloy.Globals.OS_IOS) {
				Calendar.deleteEvent(row);

			} else {
				//var _eventId = e.rowData._object.eventId;
				Calendar.deleteEvent(row);
			}
			eventDatabaseArray.splice(e.rowData._objectid, 1);

		} else {
			var category = e.rowData._object;

			if (category) {

				var categoryEvent = category.eventCalendar;
				var html = categoryEvent.html;
				var title = categoryEvent.title;
				var link = categoryEvent.link;

				var eventDetail = Alloy.createController('detail/EventDetail', {
					htmlObject : html,
					titleShare : title,
					linkShare : link
				});
				eventDetail.getView().open();

			}
		}
		////////////////////Ti.API.info('setList eventDatabaseArray ');
		Ti.App.Properties.setList("eventDatabaseArray", eventDatabaseArray);

	});

};

createTableView = function(events) {
	////////////////////////////////Ti.API.info('createTableView events : ' + events);

	var eventDatabaseArray = Ti.App.Properties.getList('eventDatabaseArray');
	//

	//////////////////////Ti.API.info('eventDatabaseArray.length : ' + eventDatabaseArray.length);
	////////////////////Ti.API.info('events.length : ' + events.length);

	var padding = 10;
	if (Alloy.Globals.OS_IOS_IPAD) {
		padding = 20;
	}
	var eventlength = events.length;
	////////////////////Ti.API.info('eventlength : ' + eventlength);
	if (eventlength > 0) {
		////////////////////Ti.API.info('eventDatabaseArray : ' + eventDatabaseArray);

		if (eventDatabaseArray == undefined) {
			eventDatabaseArray = [];
		}
		////////////////////Ti.API.info('events.length : ' + events.length);

		var tblData = [];
		for (var i = 0; i < events.length; i++) {
			var eventDatabaseNumber = null;
			for (var j = 0; j < eventDatabaseArray.length; j++) {
				if (events[i].id == eventDatabaseArray[j].eventId) {
					eventDatabaseNumber = j;
					////////////////////Ti.API.info('for eventDatabaseNumber : ' + eventDatabaseNumber);
					j = eventDatabaseArray.length;

				}
			}

			if (eventDatabaseNumber != undefined) {

				///createView
				var dateBegin = DateTime.setFormatDateEvent(events[i].begin);

				////////////////////Ti.API.info('eventDatabaseNumber : ' + eventDatabaseNumber);

				var row = Ti.UI.createTableViewRow({
					height : Ti.UI.SIZE,
					width : Ti.UI.FILL,
					backgroundColor : "#333333",
					_objectid : eventDatabaseNumber,
					_object : eventDatabaseArray[eventDatabaseNumber]
				});

				var view = Ti.UI.createView({
					height : Ti.UI.SIZE,
					width : Ti.UI.SIZE,
					layout : "vertical",
					left : padding

				});
				var title = Ti.UI.createLabel({
					id : "title",
					text : events[i].title
				});
				$.addClass(title, "titleText");
				var eventCalendarDes = eventDatabaseArray[eventDatabaseNumber].eventCalendar;
				var description;

				if (eventCalendarDes != undefined) {
					description = Ti.UI.createLabel({
						text : eventCalendarDes.title
					});
					$.addClass(description, "desText");
				} else {
					description = Ti.UI.createLabel({
						height : 0
					});
				}

				var time = Ti.UI.createLabel({
					id : "time",
					text : dateBegin
				});
				$.addClass(time, "timeText");

				var line = Ti.UI.createView();
				$.addClass(line, "settingLine");

				var button = Ti.UI.createButton({
					id : "btn",
					backgroundImage : "/images/setting/setting_reminder.png",
					right : padding,
					height : 23,
					width : 23

				});

				view.add(title);
				view.add(description);
				view.add(time);
				row.add(view);
				row.add(button);
				row.add(line);

				tblData.push(row);

			} else {
				////////////////////////////////Ti.API.info('!= ');
				if (Alloy.Globals.OS_IOS) {

					var dateBegin = DateTime.setFormatDateEvent(events[i].begin);

					var row = Ti.UI.createTableViewRow({
						height : Ti.UI.SIZE,
						width : Ti.UI.FILL,
						backgroundColor : "#333333",

					});

					var view = Ti.UI.createView({
						height : Ti.UI.SIZE,
						width : Ti.UI.SIZE,
						layout : "vertical",
						left : padding

					});
					var title = Ti.UI.createLabel({
						id : "title",
						text : events[i].title
					});
					$.addClass(title, "titleText");

					var time = Ti.UI.createLabel({
						id : "time",
						text : dateBegin
					});
					$.addClass(time, "timeText");

					var line = Ti.UI.createView();
					$.addClass(line, "settingLine");

					var button = Ti.UI.createButton({
						id : "btn",
						backgroundImage : "/images/setting/setting_reminder.png",
						right : padding,
						height : 23,
						width : 23

					});

					view.add(title);
					view.add(time);
					row.add(view);
					row.add(button);
					row.add(line);

					tblData.push(row);
				}

			}

		}
		$.table.setData(tblData);
	} else {

		var eventDatabaseArray = [];
		Ti.App.Properties.setList("eventDatabaseArray", eventDatabaseArray);
		var eventDatabase = Ti.App.Properties.getList("eventDatabaseArray");

		for (var i = 0; i < events.length; i++) {
			Calendar.deleteEvent(1);
		}
	}
};

init();

