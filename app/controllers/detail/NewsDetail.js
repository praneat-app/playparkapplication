var args = arguments[0] || {};
var htmlWebView = args.htmlObject || '';
var titleShare = args.titleShare || '';
var linkShare = args.linkShare || '';

function init() {

	Ti.API.info('htmlWebView : ' + htmlWebView);
	Ti.API.info('titleShare : ' + titleShare);
	Ti.API.info('linkShare : ' + linkShare);


	$.navBarText.setText(Ti.Locale.getString("detail_new_" + Alloy.Globals.LANGUAGE));

	$.htmlView.setUrl(htmlWebView);

	$.backButton.addEventListener("click", function() {
		$.windowDetail.close();

	});

	$.tabFacebookButton.addEventListener("click", function() {
		$.tabFacebookButton.setEnabled(false);
		var Facebook = require("/social/Facebook");
		var facebook = new Facebook();
		facebook.post(linkShare, Ti.Locale.getString("facebook_share_name"), "", titleShare, $.tabFacebookButton);
	});

	$.tabGooglePButton.addEventListener("click", function() {
		var googlePlusView = Alloy.createController('view/GooglePlus', {
			titleShare : titleShare,
			linkShare : linkShare
		});
		googlePlusView.getView();
	});

	$.tabLineButton.addEventListener("click", function() {

		var textToShare = encodeURIComponent(titleShare + " ");
		var linkToShare = encodeURIComponent(linkShare);
		var url = 'line://msg/text/' + textToShare + linkToShare;
		Ti.Platform.openURL(url);
	});

};

init();
