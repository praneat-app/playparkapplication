//set
var PlayParkMobileAPI = require("/api/PlayParkMobileAPI");
var DateTime = require("/utils/DateTime");
var _progressBar = null;
var ProgressBar = require("utils/ProgressBar");
_progressBar = new ProgressBar({
	parentView : $.listviewView
});
var FormatCurrency = require("utils/FormatCurrency");

var categoryObject = [];
var value = "";

var limit = PlayParkMobileAPI.LIMIT;
var limitLength = 10;
var _isFirstLoadJson = true;

function init() {

	$.listviewView.setHeight(0);

	$.textField.addEventListener("return", function(e) {
		searchVideo(e.value);
	});

	$.searchButton.addEventListener("click", function(e) {
		searchVideo($.textField.value);
	});

	$.videoSearchBackground.addEventListener("click", function() {
		$.videoSearchWindow.close();
	});

	$.videoSearchListView.addEventListener("itemclick", function(e) {

		if (categoryObject) {
			if (categoryObject.length > 0) {
				var object = categoryObject[e.itemIndex];
				var videoDetailActivity = Alloy.createController('detail/VideoDetailActivity', {
					videoObject : object
				});
				videoDetailActivity.getView().open();
			}
		}
	});

	////setMaker////
	$.videoSearchListView.setMarker({
		sectionIndex : 0,
		itemIndex : (limit - 1)
	});

	$.videoSearchListView.addEventListener('marker', function(e) {
		var max = limit + limitLength;
		limit = limit + 1;
		getVideoListViewMaker(value, limit, max);
	});
}

var searchVideo = function(_value) {

	limit = PlayParkMobileAPI.LIMIT;
	_isFirstLoadJson = true;

	$.videoSearchListView.setMarker({
		sectionIndex : 0,
		itemIndex : (limit - 1)
	});

	$.videoSearchListView.removeAllChildren();
	////////////////Ti.API.info('_value :' + _value);
	categoryObject = [];

	value = _value;
	showProgressBar();

	$.textField.setValue("");
	$.listviewView.setHeight(Ti.UI.FILL);
	getVideoListView(_value);

	$.navBar.removeAllChildren();

	var backButton = Ti.UI.createButton();
	$.addClass(backButton, "backButton");

	var navBarText = Ti.UI.createLabel({
		text : Ti.Locale.getString("video_" + Alloy.Globals.LANGUAGE)
	});
	$.addClass(navBarText, "navTitle");

	var searchButton = Ti.UI.createImageView({
		backgroundImage : "/images/video/video_navview_search.png",
		width : 40,
		height : 40,
		right : 5
	});

	$.navBar.add(backButton);
	$.navBar.add(navBarText);
	$.navBar.add(searchButton);
	$.navBar.setBackgroundColor("white");

	backButton.addEventListener("click", function(e) {
		$.videoSearchWindow.close();
	});

	searchButton.addEventListener("click", function(e) {
		$.navBar.removeAllChildren();
		$.navBar.setBackgroundColor("#ccc6bd");

		var textFieldView = Ti.UI.createView();
		$.addClass(textFieldView, "textFieldView");

		var textField = Ti.UI.createTextField();
		$.addClass(textField, "textField");

		textFieldView.add(textField);

		var searchButton = Ti.UI.createButton();
		$.addClass(searchButton, "searchButton");

		searchButton.addEventListener("click", function(e) {
			searchVideo(textField.value);
		});

		textField.addEventListener("return", function(e) {
			searchVideo(e.value);
		});

		$.navBar.add(textFieldView);
		$.navBar.add(searchButton);

	});

};

var getVideoListViewMaker = function(_value, _to, _from) {
	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		PlayParkMobileAPI.getSearchVideoList(getVideoListViewMakerCallback, _value, _to, _from);
	} else {
		limit = limit - 1;
	}
};

var getVideoListViewMakerCallback = function(_data) {
	//////////////////Ti.API.info('_data : ' + _data.code);
	if (_data) {
		if (_data.code == PlayParkMobileAPI.SUCCESS) {
			var message = _data.result.message;
			var category = _data.result.category;
			createListview(category);
		}
	} else {
		limit = limit - 1;
	}
};

var getVideoListView = function(_value) {
	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		PlayParkMobileAPI.getSearchVideoList(getVideoListViewCallback, _value);
	} else {
		//TODO alert ok
		hideProgressBar();
		alert(Ti.Locale.getString("network_none_" + Alloy.Globals.LANGUAGE));
	}
};

var getVideoListViewCallback = function(_data) {
	//////////////////////////////////Ti.API.info('_data.code : ' + _data.code);
	//////////////////Ti.API.info('_data : ' + _data.code);
	if (_data) {
		if (_data.code == PlayParkMobileAPI.SUCCESS) {
			var message = _data.result.message;
			var result = _data.result.result;
			categoryObject = _data.result.category;
			////////////////Ti.API.info('getVideoListViewCallback categoryObject : ' + categoryObject);
			////////////////Ti.API.info('getVideoListViewCallback categoryObject length : ' + categoryObject.length);

			createListview(categoryObject, result);
			hideProgressBar();
		} else {
			hideProgressBar();
			createListview();
		}
	} else {
		//TODO alert ok to call getVideoListView() again
		hideProgressBar();
	}
};

var createListview = function(_data, _result) {
	////////////////Ti.API.info('_result : ' + _result);

	if (_result && _result != undefined) {
		_result = " (" + _result + ")";
	} else {
		_result = "";
	}
	////////////////Ti.API.info('_result : ' + _result);

	if (!_data) {
		_data = createJsonObject();
	}
	var items = [];
	var arrayLength = _data.length;

	for (var i in _data) {
		if (_isFirstLoadJson == false) {
			categoryObject[categoryObject.length] = _data[i];
		}

		var object = JSON.stringify(_data[i]);
		var data = JSON.parse(object);
		var tamplateStyle = "";

		if (_isFirstLoadJson) {
			if (i == 0) {
				tamplateStyle = "templateStart";
			} else {
				tamplateStyle = "template";
			}
		} else {
			tamplateStyle = "template";
		}
		/*
		 if (i == 0) {
		 tamplateStyle = "templateStart";
		 } else if (i == (arrayLength - 1)) {
		 tamplateStyle = "templateEnd";
		 } else {
		 tamplateStyle = "template";
		 }
		 */

		var title = Titanium.Network.decodeURIComponent(data.title);
		var views = FormatCurrency.getFormatCurrency(data.views);

		if (!value) {
			value = " ";
		};

		items.push({
			template : tamplateStyle, // set the template
			videoTitleLabel : {
				//text : 'ผลลัพธ์จากการค้นหา "' + value + '"' + _result
				text : Ti.Locale.getString("Search_video_result_" + Alloy.Globals.LANGUAGE) + ' "' + value + '" ' + _result
			},
			videoGameImageView : {
				image : data.images
			},
			videoNameLabel : {
				text : title
			},
			videoViewerLabel : {
				text : views

			}
		});
	}

	if (_isFirstLoadJson) {
		$.section.setItems(items);
		setTimeout(function() {
			$.videoSearchListView.scrollToItem(0, 0, Titanium.UI.iPhone.RowAnimationStyle.TOP);
		}, 500);
		_isFirstLoadJson = false;
	} else {

		$.section.appendItems(items);
		limit = limit + limitLength - 1;
		$.videoSearchListView.setMarker({
			sectionIndex : 0,
			itemIndex : (limit - 1)
		});

	}

};

var showProgressBar = function() {
	_progressBar.show();
};

var hideProgressBar = function() {
	if (_progressBar != null) {
		setTimeout(function() {
			_progressBar.hide();
		}, 1000);
	}
};

var createJsonObject = function() {
	var jsonObject = {
		"data" : [{
			"title" : "No Data"
		}]
	};

	var myObjectString = JSON.stringify(jsonObject);
	var myNewObject = JSON.parse(myObjectString);
	var newsArray = myNewObject.data;
	return newsArray;
};

init();
