var args = arguments[0] || {};
var videoObject = args.videoObject || '';

//set
var PlayParkMobileAPI = require("/api/PlayParkMobileAPI");
var DateTime = require("/utils/DateTime");
var _progressBar = null;
var ProgressBar = require("utils/ProgressBar");
_progressBar = new ProgressBar({
	parentView : $.contentContainer
});

var titleToggle = false;
var imageToggle;
var categoryObejct;

var FormatCurrency = require("utils/FormatCurrency");
var VideoYoutube = require("utils/VideoYoutube");

var videoPlayer = null;
if (Alloy.Globals.OS_ANDROID) {
	var videoOpenView = null;
}

var init = function() {

	////////////////////////////////////Ti.API.info('FormatCurrency' + FormatCurrency);
	showProgressBar();

	$.navBarText.setText(Ti.Locale.getString("video_" + Alloy.Globals.LANGUAGE));
	getContent();

	$.backButton.addEventListener("click", function() {
		$.windowDetail.close();
	});

	if (Alloy.Globals.OS_ANDROID) {
		videoOpenView = Titanium.UI.createView({
			height : Titanium.UI.FILL,
			width : Titanium.UI.FILL,
			backgroundColor : "black",
			zIndex : 10
		});
		$.videoView.add(videoOpenView);
	};

	if (Alloy.Globals.OS_IOS) {
		$.videoTitleToggle.addEventListener("click", function() {
			if (videoObject) {
				if (!titleToggle) {
					var des = Titanium.Network.decodeURIComponent(videoObject.description);
					////////////////////Ti.API.info('des : ' + des);
					$.videoDesciption.setText(des);
					titleToggle = true;
					$.videoTitleViewRight.setImage(imageToggle.toggleOpen);
					$.videoDes.setHeight(Ti.UI.SIZE);
				} else {
					$.videoDesciption.setText("");
					titleToggle = false;
					$.videoTitleViewRight.setImage(imageToggle.toggleClose);
					$.videoDes.setHeight(0);
				}
			}
		});
	}

	$.videoDetailListView.addEventListener("itemclick", function(e) {

		if (Alloy.Globals.OS_IOS) {
			if (categoryObejct) {
				videoObject = categoryObejct[e.itemIndex];
				getVideo();
			}

		} else {
			if (e.itemIndex == 0) {

				var item = e.section.getItemAt(e.itemIndex);
				var dateVideo = videoObject.date;
				if (dateVideo) {
					dateVideo = DateTime.getEventDateTime(videoObject.date, DateTime.MODE_DATE);
				}
				var views = FormatCurrency.getFormatCurrency(videoObject.views);
				var des = Titanium.Network.decodeURIComponent(videoObject.description);
				//////////////////////Ti.API.info('des : ' + des);
				var items = {};

				if (!titleToggle) {
					titleToggle = true;

					items = {
						template : "templateToggle",
						videoTitleText : {
							text : videoObject.title
						},
						videoTitleViewsText : {
							text : dateVideo
						},
						videoViewerLabel : {
							text : views
						},
						videoTitleViewRight : {
							image : imageToggle.toggleOpen
						},
						videoDes : {
							height : Ti.UI.SIZE
						},
						videoDesciption : {
							text : des
						}
					};

				} else {
					titleToggle = false;
					items = {
						template : "templateToggle",
						videoTitleText : {
							text : videoObject.title
						},
						videoTitleViewsText : {
							text : dateVideo
						},
						videoViewerLabel : {
							text : views
						},
						videoTitleViewRight : {
							image : imageToggle.toggleClose
						},
						videoDes : {
							height : 0
						},
						videoDesciption : {
							text : ""

						}
					};

				}

				$.section.updateItemAt(e.itemIndex, items);

			} else {
				if (categoryObejct) {
					if (Alloy.Globals.OS_ANDROID) {
						$.videoView.add(videoOpenView);
					}
					videoObject = categoryObejct[e.itemIndex - 1];
					getVideo();
				}
			}

		}

	});

	$.tabFacebookButton.addEventListener("click", function() {
		$.tabFacebookButton.setEnabled(false);
		var titleShare = videoObject.title;
		var linkShare = videoObject.url;
		var Facebook = require("/social/Facebook");
		var facebook = new Facebook();
		facebook.post(linkShare, Ti.Locale.getString("facebook_share_name"), "", titleShare, $.tabFacebookButton);
	});

	$.tabGooglePButton.addEventListener("click", function() {
		var googlePlusView = Alloy.createController('view/GooglePlus', {
			titleShare : videoObject.title,
			linkShare : videoObject.url
		});
		googlePlusView.getView();
	});

	$.tabLineButton.addEventListener("click", function() {
		var textToShare = encodeURIComponent(videoObject.title + " ");
		var linkToShare = encodeURIComponent(videoObject.url);
		var url = 'line://msg/text/' + textToShare + linkToShare;
		Ti.Platform.openURL(url);
	});

	$.rightButton.addEventListener("click", function(e) {
		////////////////Ti.API.info('pause');
		////////////////Ti.API.info('videoPlayer : ' + videoPlayer);
		if (videoPlayer) {
			videoPlayer.pause();
		};
		var videoSearchActivity = Alloy.createController('detail/VideoSearchActivity');
		videoSearchActivity.getView().open();
	});

};

var getContent = function() {
	getVideo();
	imageToggle = {
		toggleOpen : "/images/video/video_detail_arrow_down.png",
		toggleClose : "/images/video/video_detail_arrow_up.png"
	};

};
var getVideo = function() {

	if (videoObject) {

		if (Alloy.Globals.OS_ANDROID) {
			$.videoView.remove(videoPlayer);
		} else {
			$.videoView.removeAllChildren();
		}

		videoPlayer = null;
		getVideoListView(videoObject.id);

		if (Alloy.Globals.OS_IOS) {
			$.videoTitleText.setText(videoObject.title);

			var views = FormatCurrency.getFormatCurrency(videoObject.views);
			$.socialViewerLabel.setText(views);

			var dateVideo = videoObject.date;
			var date = "";
			if (dateVideo) {
				date = DateTime.getEventDateTime(videoObject.date, DateTime.MODE_DATE);
			}

			var titleView = date;
			$.videoTitleViewsText.setText(titleView);

		}

		var views = FormatCurrency.getFormatCurrency(videoObject.views);
		$.socialViewerLabel.setText(views);
		////////Ti.API.info('VideoYoutube');

		//video
		VideoYoutube.h264videosWithYoutubeURL(videoObject.ref, function(_video, _image) {
			////////Ti.API.info('_video : ' + _video);
			////////Ti.API.info('_image : ' + _image);

			if (_video) {
				videoPlayer = Titanium.Media.createVideoPlayer({
					ns : Ti.Media,
					height : Titanium.UI.FILL,
					width : Titanium.UI.FILL,
					autoplay : false,
					backgroundColor : "black",
					top : 0,
					left : 0,
					scalingMode : Titanium.Media.VIDEO_SCALING_ASPECT_FIT,
					mediaControlMode : Titanium.Media.VIDEO_CONTROL_NONE,
					autoplay : true,
					url : _video,
					zIndex : 9
				});

				//view.add(videoPlayer);
				$.videoView.add(videoPlayer);
				//complete

				if (Alloy.Globals.OS_ANDROID) {
					videoPlayer.addEventListener("load", function(e) {
						setTimeout(function() {
							$.videoView.remove(videoOpenView);
						}, 1000);
					});
				}

			} else {
				if (Alloy.Globals.OS_ANDROID) {
					$.videoView.remove(videoOpenView);
				}

				var image = Ti.UI.createImageView({
					image : _image,
					height : Ti.UI.FILL,
					width : Ti.UI.FILL,
					backgroundColor : "black"

				});
				$.videoView.add(image);

			}

		}, function(e) {
			alert(Ti.Locale.getString("data_not_found_" + Alloy.Globals.LANGUAGE));
		});
	};
};

var getVideoListView = function(_id) {
	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		$.videoDetailListView.removeAllChildren();
		PlayParkMobileAPI.getVideoRelateById(getVideoListViewCallback, _id);
	} else {
		//TODO alert ok
		hideProgressBar();
		alert(Ti.Locale.getString("network_none_" + Alloy.Globals.LANGUAGE));
	}
};

var getVideoListViewCallback = function(_data) {
	//////////////////////////////////Ti.API.info('_data.code : ' + _data.code);
	if (_data) {
		if (_data.code == PlayParkMobileAPI.SUCCESS) {
			var message = _data.result.message;
			categoryObejct = _data.result.category;
			createListview(categoryObejct);
		} else {
			hideProgressBar();
			createListview();
		}
	} else {
		//TODO alert ok to call getVideoListView() again
		hideProgressBar();

	}
};

var createListview = function(_data) {
	//////////////////////Ti.API.info('_data : ' + _data);

	if (!_data) {
		_data = createJsonObject();
	}
	var items = [];
	var arrayLength = _data.length;

	if (Alloy.Globals.OS_ANDROID) {
		if (videoObject) {

			var views = FormatCurrency.getFormatCurrency(videoObject.views);
			var dateVideo = videoObject.date;
			var date = "";
			if (dateVideo) {
				date = DateTime.getEventDateTime(videoObject.date, DateTime.MODE_DATE);
			}
			var des = Titanium.Network.decodeURIComponent(videoObject.description);
			////////////////////Ti.API.info('des : ' + des);
			items.push({
				template : "templateToggle",
				videoTitleText : {
					text : videoObject.title
				},
				videoTitleViewsText : {
					text : date
				},
				videoViewerLabel : {
					text : views
				},
				videoDes : {
					height : 0
				},
				videoDesciption : {
					//height : Ti.UI.SIZE,
					//text : des
				}
			});
		}
	}

	for (var i in _data) {

		var object = JSON.stringify(_data[i]);
		var data = JSON.parse(object);
		var tamplateStyle = "";

		if (i == 0) {
			tamplateStyle = "templateStart";
		} else if (i == (arrayLength - 1)) {
			tamplateStyle = "templateEnd";
		} else {
			tamplateStyle = "template";
		}
		var title = Titanium.Network.decodeURIComponent(data.title);
		var views = FormatCurrency.getFormatCurrency(data.views);

		items.push({
			template : tamplateStyle, // set the template
			videoGameImageView : {
				image : data.images
			},
			videoNameLabel : {
				text : title
			},
			videoViewerLabel : {
				text : views

			}
		});

	}

	$.section.setItems(items);

	setTimeout(function() {
		$.videoDetailListView.scrollToItem(0, 0, Titanium.UI.iPhone.RowAnimationStyle.TOP);
	}, 500);

	hideProgressBar();

};

var showProgressBar = function() {
	_progressBar.show();
};

var hideProgressBar = function() {
	if (_progressBar != null) {
		setTimeout(function() {
			_progressBar.hide();
		}, 1000);
	}
};

var createJsonObject = function() {
	var jsonObject = {
		"data" : [{
			"title" : "No Data"

		}]
	};

	var myObjectString = JSON.stringify(jsonObject);
	var myNewObject = JSON.parse(myObjectString);
	var newsArray = myNewObject.data;
	return newsArray;
};

init();

