var args = arguments[0] || {};
var streamingObject = args.streamingObject || '';

//set
var PlayParkMobileAPI = require("/api/PlayParkMobileAPI");
var DateTime = require("/utils/DateTime");
var _progressBar = null;
var ProgressBar = require("utils/ProgressBar");
_progressBar = new ProgressBar({
	parentView : $.contentContainer
});

var titleToggle = false;
var imageToggle;
var categoryArray;

var description = "";
var imageStatus;

var categorySelect;
var categoryNewSelect;

var streamingObjectSelect;
var FormatCurrency = require("utils/FormatCurrency");
var VideoYoutube = require("utils/VideoYoutube");

var videoOpenView = null;
var videoPlayer = null;

var init = function() {

	////////////////////////////////Ti.API.info('streamingObject : ' + streamingObject);
	showProgressBar();
	$.navBarText.setText(Ti.Locale.getString("live_" + Alloy.Globals.LANGUAGE));
	getContent();
	$.videoImage.setImage(streamingObject.picgame);
	$.backButton.addEventListener("click", function() {
		$.windowDetail.close();
	});

	if (Alloy.Globals.OS_ANDROID) {
		videoOpenView = Titanium.UI.createView({
			height : Titanium.UI.FILL,
			width : Titanium.UI.FILL,
			backgroundColor : "black",
			zIndex : 10
		});
		$.videoView.add(videoOpenView);
	}

	if (Alloy.Globals.OS_IOS) {
		$.videoTitleToggle.addEventListener("click", function() {
			if (!titleToggle) {
				var des = Titanium.Network.decodeURIComponent(description);
				$.videoDesciption.setText(des);
				titleToggle = true;
				$.videoTitleViewRight.setImage(imageToggle.toggleOpen);
				$.videoDes.setHeight(Ti.UI.SIZE);
			} else {
				$.videoDesciption.setText("");
				titleToggle = false;
				$.videoTitleViewRight.setImage(imageToggle.toggleClose);
				$.videoDes.setHeight(0);
			}
		});
	}

	$.videoDetailListView.addEventListener("itemclick", function(e) {

		if (Alloy.Globals.OS_IOS) {
			if (categoryArray) {
				var idSelect = e.itemIndex;
				//Ti.API.info('categoryArray.length : ' + categoryArray.length);
				checkVideoPlayer(categoryArray, idSelect);
			}
		} else {

			if (e.itemIndex == 0) {
				var item = e.section.getItemAt(e.itemIndex);
				var date = categoryArray.date;
				if (date) {
					date = DateTime.getEventDateTime(streamingObjectSelect.date, DateTime.MODE_DATE);
				}
				var views = FormatCurrency.getFormatCurrency(streamingObjectSelect.views);
				var description = streamingObjectSelect.description;
				var des = Titanium.Network.decodeURIComponent(streamingObjectSelect.description);
				var items = {};

				if (!titleToggle) {
					titleToggle = true;
					items = {
						template : "templateToggle",
						videoTitleText : {
							text : streamingObjectSelect.title
						},
						videoViewerLabel : {
							text : views
						},
						videoTitleViewRight : {
							image : imageToggle.toggleOpen
						},
						videoDes : {
							height : Ti.UI.SIZE
						},
						videoDesciption : {
							text : des
						}
					};

				} else {
					titleToggle = false;
					items = {
						template : "templateToggle",
						videoTitleText : {
							text : streamingObjectSelect.title
						},
						videoViewerLabel : {
							text : views
						},
						videoTitleViewRight : {
							image : imageToggle.toggleClose
						},
						videoDes : {
							height : 0
						},
						videoDesciption : {
							text : ""
						}
					};
				}

				$.section.updateItemAt(e.itemIndex, items);

			} else {
				if (categoryArray) {
					if (Alloy.Globals.OS_ANDROID) {
						$.videoView.add(videoOpenView);
					}
					var idSelect = e.itemIndex - 1;
					checkVideoPlayer(categoryArray, idSelect);
				}
			}

		}
	});

	$.tabFacebookButton.addEventListener("click", function() {
		$.tabFacebookButton.setEnabled(false);
		var titleShare = streamingObjectSelect.title;
		var linkShare = streamingObjectSelect.url;
		var Facebook = require("/social/Facebook");
		var facebook = new Facebook();
		facebook.post(linkShare, Ti.Locale.getString("facebook_share_name"), "", titleShare, $.tabFacebookButton);
	});
	$.tabGooglePButton.addEventListener("click", function() {
		if (streamingObjectSelect) {
			//////////////////////////////////Ti.API.info('streamingObjectSelect.url : ' + streamingObjectSelect.url_share);
			//////////////////////////////////Ti.API.info('streamingObjectSelect.title : ' + streamingObjectSelect.title);

			var googlePlusView = Alloy.createController('view/GooglePlus', {
				titleShare : streamingObjectSelect.title,
				linkShare : streamingObjectSelect.url_share
			});
			googlePlusView.getView();
		}

	});

	$.tabLineButton.addEventListener("click", function() {

		if (streamingObjectSelect) {

			//////////////////////////////////Ti.API.info('streamingObjectSelect.url : ' + streamingObjectSelect.url_share);
			//////////////////////////////////Ti.API.info('streamingObjectSelect.title : ' + streamingObjectSelect.title);
			var textToShare = encodeURIComponent(streamingObjectSelect.title + " ");
			var linkToShare = encodeURIComponent(streamingObjectSelect.url_share);
			var url = 'line://msg/text/' + textToShare + linkToShare;
			////////////////////////////////////////////Ti.API.info('url : ' + url);
			Ti.Platform.openURL(url);
		}

	});

};

var getContent = function() {

	if (streamingObject) {
		getVideoListView(streamingObject.id);
		//////////////////////////////////////Ti.API.info('streamingdetail  ===== >  id : ' + streamingObject.id);
	};

	imageToggle = {
		toggleOpen : "/images/video/video_detail_arrow_down.png",
		toggleClose : "/images/video/video_detail_arrow_up.png"
	};

	imageStatus = {
		live : "/images/stream/stream_status_live.png",
		wait : "/images/stream/stream_status_wait.png"
	};

};

var getStreamingVideo = function(_data) {

	//////////////////////////////////////Ti.API.info('_data : '+_data);
	streamingObjectSelect = _data;
	$.videoTitleText.setText(_data.title);
	//////////////////////////////////////Ti.API.info('_data.title  : '+_data.title);
	var views = FormatCurrency.getFormatCurrency(_data.views);

	$.socialViewerLabel.setText(views);
	//////////////////////////////////////////Ti.API.info('data.status : ' + _data.status);
	if (_data.status) {
		if (_data.status == 1) {//Live
			$.videoStatusImage.setImage(imageStatus.live);
			$.videoStatusText.setText("LIVE");
		} else if (_data.status == 2) {//wait
			$.videoStatusImage.setImage(imageStatus.wait);
			$.videoStatusText.setText("WAIT");
		} else {//wait
			$.videoStatusImage.setImage("");
			$.videoStatusText.setText("RERUN");
		}
	};

	description = _data.description;
	var dataUrl = _data.url;
	var url = dataUrl.split("=");

	/*******videoView*********/
	if (Alloy.Globals.OS_ANDROID) {
		$.videoView.remove(videoPlayer);
	} else {
		$.videoView.removeAllChildren();
	}

	VideoYoutube.h264videosWithYoutubeURL(url[1], function(_video, _image) {
		if (_video) {
			videoPlayer = Titanium.Media.createVideoPlayer({
				ns : Ti.Media,
				height : Titanium.UI.FILL,
				width : Titanium.UI.FILL,
				autoplay : false,
				backgroundColor : "black",
				top : 0,
				left : 0,
				scalingMode : Titanium.Media.VIDEO_SCALING_ASPECT_FIT,
				mediaControlMode : Titanium.Media.VIDEO_CONTROL_NONE,
				autoplay : true,
				url : _video,
				zIndex : 8
			});

			$.videoView.add(videoPlayer);
			if (Alloy.Globals.OS_ANDROID) {
				videoPlayer.addEventListener("load", function(e) {
					setTimeout(function() {
						////////Ti.API.info('load');
						$.videoView.remove(videoOpenView);
					}, 1000);
				});
			}
		} else {
			if (Alloy.Globals.OS_ANDROID) {
				$.videoView.remove(videoOpenView);
			}
			var image = Ti.UI.createImageView({
				image : _image,
				height : Ti.UI.FILL,
				width : Ti.UI.FILL,
				backgroundColor : "black"

			});
			$.videoView.add(image);

		}

	}, function(e) {
		alert(Ti.Locale.getString("data_not_found_" + Alloy.Globals.LANGUAGE));
	});

};

var getVideoListView = function(_id) {
	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		$.videoDetailListView.removeAllChildren();
		PlayParkMobileAPI.getGameChannelDetail(getVideoListViewCallback, _id);
	} else {
		//TODO alert ok
		hideProgressBar();
		alert(Ti.Locale.getString("data_not_found_" + Alloy.Globals.LANGUAGE));
	}
};

var getVideoListViewCallback = function(_data) {

	if (_data) {
		if (_data.code == PlayParkMobileAPI.SUCCESS) {
			var message = _data.result.message;
			////////////////////////////////////////////Ti.API.info('message : ' + message);
			var categoryOriginal = _data.result.video;
			checkVideoPlayer(categoryOriginal, 0);
			//createListview(categoryArray, 0);
		} else {
			hideProgressBar();
			alert(Alloy.Globals.ALERT_NOT_FOUND);
		}
	} else {
		hideProgressBar();
	}
};

var checkVideoPlayer = function(_Object, _idPlay) {
	//Ti.API.info("_Object : " + _Object + '_idPlay : ' + _idPlay);

	categoryNewSelect = categorySelect;
	categoryArray = [];
	for (var i in _Object) {
		if (i != _idPlay) {
			var lenght = categoryArray.length;
			categoryArray[lenght] = _Object[i];

		} else {
			//var lenght = categoryArray.length;
			//categoryArray[lenght] = _Object[i];

			categorySelect = _Object[i];

			var object = JSON.stringify(_Object[i]);
			var data = JSON.parse(object);
			getStreamingVideo(data);
		}
	}

	if (categoryNewSelect) {
		var lenght = categoryArray.length;
		categoryArray[lenght] = categoryNewSelect;
		categoryNewSelect = categorySelect;
	}

	createListview(categoryArray);
	hideProgressBar();

};

var createListview = function(_data) {

	var items = [];
	var arrayLength = _data.length;
	//////////////////////Ti.API.info('arrayLength : ' + arrayLength);
	////////////////////Ti.API.info('_data lenght : ' + _data.length);

	if (Alloy.Globals.OS_ANDROID) {
		if (streamingObjectSelect) {

			var views = FormatCurrency.getFormatCurrency(streamingObjectSelect.views);
			var dateVideo = streamingObjectSelect.date;
			var date = "";
			if (dateVideo) {
				date = DateTime.getEventDateTime(streamingObjectSelect.date, DateTime.MODE_DATE);
			}
			var description = streamingObjectSelect.description;
			description = Titanium.Network.decodeURIComponent(streamingObjectSelect.description);

			items.push({
				template : "templateToggle",
				videoTitleText : {
					text : streamingObjectSelect.title
				},
				videoViewerLabel : {
					text : views
				},
				videoDes : {
					height : 0
				}
			});
		}
	}

	for (var i in _data) {
		var object = JSON.stringify(_data[i]);
		var data = JSON.parse(object);
		var statusImage = "";
		var statusText = "";

		Ti.API.info('data.id : ' + data.id);

		if (data.status) {
			if (data.status == 1) {//Live
				statusImage = imageStatus.live;
				statusText = "LIVE";
			} else if (data.status == 1) {//wait
				statusImage = imageStatus.wait;
				statusText = "WAIT";
			} else {//wait
				statusImage = "";
				statusText = "RERUN";
			}
		};

		var tamplateStyle = null;
		if (i == 0) {
			tamplateStyle = "templateStart";
		} else if (i == (arrayLength - 1)) {
			tamplateStyle = "templateEnd";
		} else {
			tamplateStyle = "template";
		}

		var views = FormatCurrency.getFormatCurrency(data.views);

		items.push({
			template : tamplateStyle, //tamplateStyle, // set the template
			videoGameImageView : {
				image : data.image
			},
			videoTabText : {

			},
			videoNameLabel : {
				text : data.title
			},
			videoStatusImage : {
				image : statusImage
			},
			videoStatusText : {
				text : statusText
			},
			videoViewerLabel : {
				text : views
			}
		});
	}
	$.section.setItems(items);
	
	setTimeout(function() {
		$.videoDetailListView.scrollToItem(0, 0, Titanium.UI.iPhone.RowAnimationStyle.TOP);
	}, 500);
	
	setTimeout(function() {
		$.scrollViewContainer.setContentOffset({
			x : 0,
			y : 0
		}, {
			animated : true
		});
	}, 500);

};

var showProgressBar = function() {
	_progressBar.show();
};

var hideProgressBar = function() {
	if (_progressBar != null) {
		setTimeout(function() {
			_progressBar.hide();
		}, 1000);
	}
};

init();

