var args = arguments[0] || {};
var objectSelect = args.object || '';
var htmlObject = args.htmlObject || '';
var titleShare = args.titleShare || '';
var linkShare = args.linkShare || '';

var Event = require("/utils/Event");

function init() {

	////////////////////////////////Ti.API.info('htmlObject : ' + htmlObject + " , titleShare : " + titleShare + ", linkShare : " + linkShare);

	$.navBarText.setText(Ti.Locale.getString("detail_event_" + Alloy.Globals.LANGUAGE));
	$.htmlView.setUrl(htmlObject);

	$.backButton.addEventListener("click", function() {
		$.windowDetail.close();
	});
	//
	$.tabFacebookButton.addEventListener("click", function() {
		$.tabFacebookButton.setEnabled(false);
		var Facebook = require("/social/Facebook");
		var facebook = new Facebook();
		facebook.post(linkShare, Ti.Locale.getString("facebook_share_name"), "", titleShare, $.tabFacebookButton);

	});

	$.tabGooglePButton.addEventListener("click", function() {
		//////////////////////Ti.API.info('titleShare : ' + titleShare + "linkShare : " + linkShare + ", htmlObject :" + htmlObject);
		var googlePlusView = Alloy.createController('view/GooglePlus', {
			titleShare : titleShare,
			linkShare : linkShare
		});
		googlePlusView.getView();
	});

	$.tabLineButton.addEventListener("click", function() {
		////////////////////////Ti.API.info('titleShare : ' + titleShare + "linkShare : " + linkShare + ", htmlObject :" + htmlObject);
		var textToShare = encodeURIComponent(titleShare + " ");
		var linkToShare = encodeURIComponent(linkShare);
		var url = 'line://msg/text/' + textToShare + linkToShare;
		Ti.Platform.openURL(url);
	});

	createEventDetail(objectSelect);

};

var createEventDetail = function(data) {

	var DateTime = require("/utils/DateTime");

	var dateStart = DateTime.getEventDateTime(data.date_start, DateTime.MODE_DATE);
	var timeStart = DateTime.getEventDateTime(data.date_start, DateTime.MODE_TIME);

	var dateEnd = DateTime.getEventDateTime(data.date_end, DateTime.MODE_DATE);
	var timeEnd = DateTime.getEventDateTime(data.date_end, DateTime.MODE_TIME);

	var eventTimeContainer = Ti.UI.createView();
	//h = 34
	$.addClass(eventTimeContainer, "eventTimeContainer");

	var eventTimeBox = Ti.UI.createView();
	$.addClass(eventTimeBox, "eventTimeBox");
	eventTimeContainer.add(eventTimeBox);

	///////
	var eventTitleBox = Ti.UI.createView();
	$.addClass(eventTitleBox, "eventTitleBox");

	var eventClock = Ti.UI.createImageView({
		left : 0
	});
	$.addClass(eventClock, "eventClock");

	var eventTimeText = Ti.UI.createLabel({
		text : Ti.Locale.getString("event_datetime_" + Alloy.Globals.LANGUAGE),
		width : Ti.UI.FILL,
		height : Ti.UI.FILL,
		left : 28,
		color : "#ffffff",
		font : {
			fontSize : Alloy.Globals.FONT_SIZE_16,
			fontFamily : Alloy.Globals.FONT_DEFAULT
		},

	});
	eventTimeText.addEventListener("click", function(e) {
		var object = e.source._content;
		openEventDetail(object);
	});
	//$.addClass(eventTimeText, "eventTimeText");

	eventTitleBox.add(eventClock);
	eventTitleBox.add(eventTimeText);

	///////

	if (Alloy.Globals.OS_IOS) {

		var eventTimeStartContainer = Ti.UI.createView({
			height : 30,
			width : "24%",
			backgroundColor : "#318200",
			borderRadius : 2,
			borderWidth : 0,
			top : 2,
			bottom : 2,
			layout : "vertical"
		});
		//$.addClass(eventTimeStartContainer, "eventTimeStartContainer");

	} else {
		var eventTimeStartContainer = Ti.UI.createView();
		$.addClass(eventTimeStartContainer, "eventTimeStartContainer");
	}

	var eventDateStart = Ti.UI.createLabel({
		text : dateStart,
	});
	$.addClass(eventDateStart, "eventTimeBoxText");

	var eventTimeStart = Ti.UI.createLabel({
		text : timeStart,
	});
	$.addClass(eventTimeStart, "eventTimeBoxText");

	eventTimeStartContainer.add(eventDateStart);
	eventTimeStartContainer.add(eventTimeStart);

	////
	var eventToBox = Ti.UI.createView();

	$.addClass(eventToBox, "eventToBox");

	var eventTimeText = Ti.UI.createLabel({
		text : Ti.Locale.getString("event_datetime_to_" + Alloy.Globals.LANGUAGE),
		width : Ti.UI.SIZE,
		height : Ti.UI.SIZE,
		color : "#ffffff",
		font : {
			fontSize : Alloy.Globals.FONT_SIZE_16,
			fontFamily : Alloy.Globals.FONT_DEFAULT
		},
	});

	//$.addClass(eventTimeText, "eventTimeText");
	eventToBox.add(eventTimeText);
	///

	if (Alloy.Globals.OS_IOS) {
		var eventTimeFinishContainer = Ti.UI.createView({
			height : 30,
			width : "24%",
			backgroundColor : "#c76400",
			borderRadius : 2,
			borderWidth : 0,
			top : 2,
			bottom : 2,
			layout : "vertical"
		});
		//$.addClass(eventTimeFinishContainer, "eventTimeFinishContainer");

	} else {
		var eventTimeFinishContainer = Ti.UI.createView();
		$.addClass(eventTimeFinishContainer, "eventTimeFinishContainer");

	}

	var eventDateFinish = Ti.UI.createLabel({
		text : dateEnd,
	});
	$.addClass(eventDateFinish, "eventTimeBoxText");

	var eventTimeFinish = Ti.UI.createLabel({
		text : timeEnd,
	});
	$.addClass(eventTimeFinish, "eventTimeBoxText");

	eventTimeFinishContainer.add(eventDateFinish);
	eventTimeFinishContainer.add(eventTimeFinish);

	var listviewAlertImage = Ti.UI.createImageView();
	$.addClass(listviewAlertImage, "listviewAlertImage");
	listviewAlertImage.addEventListener("click", function(e) {

		//////////////////Ti.API.info('data : ' + data);

		var idEventDatabase = null;
		var rowEventDatabase = null;

		var Event = require("/utils/Event");
		var eventDatabase = Event.getIdEventFromDatabase(data.id);
		if (eventDatabase) {
			//////////////////Ti.API.info('eventDatabase : ' + eventDatabase);
			idEventDatabase = eventDatabase._dataIdEvent;
			rowEventDatabase = eventDatabase._row;
		};

		if (idEventDatabase) {
			//////////////////Ti.API.info('if idEventDatabase : ' + idEventDatabase);

			var alertDialog = Titanium.UI.createAlertDialog({
				title : Ti.Locale.getString("Alert_event_delete_title_" + Alloy.Globals.LANGUAGE),
				message : Ti.Locale.getString("Alert_event_delete_message_" + Alloy.Globals.LANGUAGE),
				buttonNames : [Ti.Locale.getString("Alert_event_ok_" + Alloy.Globals.LANGUAGE), Ti.Locale.getString("Alert_event_cancel_" + Alloy.Globals.LANGUAGE)],
				cancel : 1,
				_row : rowEventDatabase,
				_dataIdEvent : idEventDatabase
			});

			alertDialog.show();
			alertDialog.addEventListener('click', function(e) {

				var dataIdEvent = e.source._dataIdEvent;
				Event.deleteEventFromIdEvent(dataIdEvent, e.source._row);
				//eventDatabaseArray.splice(e.source._row, 1);
				//Ti.App.Properties.setList("eventDatabaseArray", eventDatabaseArray);
			});

		} else {
			//////////////////Ti.API.info('else idEventDatabase : ' + idEventDatabase);

			var alertDialog = Titanium.UI.createAlertDialog({
				title : Ti.Locale.getString("Alert_event_title_" + Alloy.Globals.LANGUAGE),
				message : Ti.Locale.getString("Alert_event_message_" + Alloy.Globals.LANGUAGE),
				buttonNames : [Ti.Locale.getString("Alert_event_ok_" + Alloy.Globals.LANGUAGE), Ti.Locale.getString("Alert_event_cancel_" + Alloy.Globals.LANGUAGE)],
				cancel : 1,
				_row : e.itemIndex,
			});

			alertDialog.show();
			alertDialog.addEventListener('click', function(e) {
				if (e.index == 0) {
					Event.addEvent(objectSelect);
				};
			});
		}

	});

	eventTimeBox.add(eventTitleBox);
	eventTimeBox.add(eventTimeStartContainer);
	eventTimeBox.add(eventToBox);
	eventTimeBox.add(eventTimeFinishContainer);
	eventTimeContainer.add(listviewAlertImage);
	////addview////
	$.eventDetailView.add(eventTimeContainer);

};

init();
