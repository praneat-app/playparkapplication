var args = arguments[0] || {};
var latitude = args.latitude || '';
var longitude = args.longitude || '';
var title = args.title || '';
var subtitle = args.subtitle || '';

var win = Ti.UI.createWindow({
	backgroundColor : 'white'
});

var MapModule = require('ti.map');
var appc = MapModule.createAnnotation({
	latitude :latitude,
	longitude : longitude,
	title : title,
	subtitle : subtitle,
	pincolor : MapModule.ANNOTATION_RED,
	draggable : true
});
var mapview = MapModule.createView({
	mapType : Titanium.Map.STANDARD_TYPE,//MapModule.NORMAL_TYPE,
	region : {
		latitude : latitude,
		longitude : longitude,
		latitudeDelta : 0.01,
		longitudeDelta : 0.01
	},
	annotations : [appc]
});
win.add(mapview);
win.open(); 