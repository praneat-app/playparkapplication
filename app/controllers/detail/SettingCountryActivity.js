var countrySelected = Alloy.Globals.COUNTRY_THAILAND;
var buttonImage;

var PlayParkMobileAPI = require("/api/PlayParkMobileAPI");

var _progressBar = null;
var ProgressBar = require("utils/ProgressBar");
_progressBar = new ProgressBar({
	parentView : $.contentContainer
});
var PushNotification = require("utils/Pushnotification");

//var countryImageArray;

function init() {

	$.navBarText.setText(Ti.Locale.getString("setting_country_select_" + Alloy.Globals.LANGUAGE));

	$.backButton.addEventListener("click", function(e) {
		$.windowDetail.close();
	});

	$.settingListView.addEventListener("itemclick", function(e) {

		//set language
		if (Alloy.Globals.CountryArray[e.itemIndex].id == Alloy.Globals.COUNTRY_THAILAND) {
			Alloy.Globals.LANGUAGE = Alloy.Globals.LANGUAGE_TH;
			Alloy.Globals.createSideMenu();

		} else {
			Alloy.Globals.LANGUAGE = Alloy.Globals.LANGUAGE_EN;
			Alloy.Globals.createSideMenu();

		}

		Alloy.Globals.countryCode = Alloy.Globals.CountryArray[e.itemIndex].id;
		Ti.App.Properties.setString('country', Alloy.Globals.CountryArray[e.itemIndex].id);

		var resubscribetoken = true;
		PushNotification.registerPushNotification(PushNotification.MODE_UNSUBSCRIBETOKEN, resubscribetoken);
		Ti.App.Properties.setBool('noti', true);

		$.windowDetail.close();

	});

	if (!Alloy.Globals.CountryArray) {
		showProgressBar();
		getCountry();
	} else {
		createCountryListView(Alloy.Globals.CountryArray);
	}

};

var getCountry = function() {
	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		PlayParkMobileAPI.getCountry(getCountryCallback);
	} else {
		alert(Ti.Locale.getString("network_none_" + Alloy.Globals.LANGUAGE));
		hideProgressBar();
	}
};

var getCountryCallback = function(_data) {
	//////////////////////////////////Ti.API.info('getCountryCallback  >>>>>>  data : ' + _data.code);
	if (_data.code == PlayParkMobileAPI.SUCCESS) {
		var message = _data.result.message;
		Alloy.Globals.CountryArray = _data.result.country;
		createCountryListView(Alloy.Globals.CountryArray);
		hideProgressBar();
	} else {
		var dialog = Ti.UI.createAlertDialog({
			cancel : 0,
			buttonNames : ['Cancel', 'OK'],
			message : Ti.Locale.getString("api_loading_failed_title"),
			title : Ti.Locale.getString("api_loading_failed_try_message")
		});
		dialog.addEventListener('click', function(e) {
			if (e.index !== e.source.cancel) {
				showProgressBar();
				getCountry();
			}
		});
		dialog.show();
		hideProgressBar();

	}
};

var createCountryListView = function(_data) {
	//////////////////////////////////Ti.API.info('_data : ' + _data);
	var items = [];
	for (var i in _data) {
		var object = JSON.stringify(_data[i]);
		var data = JSON.parse(object);
		if (Alloy.Globals.countryCode == Alloy.Globals.COUNTRY_THAILAND) {
			items.push({
				template : "template", // set the template
				settingIcon : {
					image : "/images/setting/setting_country_" + _data[i].id + ".png"//_data[i].images//countryImageArray[i]
				},
				countryText : {
					text : _data[i].name_th
				}
			});

		} else {
			items.push({
				template : "template", // set the template
				settingIcon : {
					image : "/images/setting/setting_country_" + _data[i].id + ".png"//_data[i].images//countryImageArray[i]
				},
				countryText : {
					text : _data[i].name_en
				}
			});
		}

	}
	$.section.setItems(items);
};

var showProgressBar = function() {
	_progressBar.show();
};

var hideProgressBar = function() {
	if (_progressBar != null) {
		setTimeout(function() {
			_progressBar.hide();
		}, 1000);
	}
};

init();

