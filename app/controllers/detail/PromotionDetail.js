var args = arguments[0] || {};
var htmlWebView = args.htmlObject || '';
var titleShare = args.titleShare || '';
var linkShare = args.linkShare || '';

function init() {

	$.navBarText.setText(Ti.Locale.getString("detail_promotion_" + Alloy.Globals.LANGUAGE));

	$.htmlView.setUrl(htmlWebView);

	$.backButton.addEventListener("click", function() {
		$.windowDetail.close();
	});

	$.tabFacebookButton.addEventListener("click", function() {
		$.tabFacebookButton.setEnabled(false);
		var Facebook = require("/social/Facebook");
		var facebook = new Facebook();
		facebook.post(linkShare, Ti.Locale.getString("facebook_share_name"), "", titleShare, $.tabFacebookButton);
	});

	$.tabGooglePButton.addEventListener("click", function() {
		//////////////////////////////////////Ti.API.info('titleShare : ' + titleShare);
		//////////////////////////////////////Ti.API.info('linkShare : ' + linkShare);

		var googlePlusView = Alloy.createController('view/GooglePlus', {
			titleShare : titleShare,
			linkShare : linkShare
		});
		googlePlusView.getView();

	});

	$.tabLineButton.addEventListener("click", function() {
		//////////////////////////////////////Ti.API.info('titleShare : ' + titleShare);
		//////////////////////////////////////Ti.API.info('linkShare : ' + linkShare);

		var textToShare = encodeURIComponent(titleShare + " ");
		var linkToShare = encodeURIComponent(linkShare);
		var url = 'line://msg/text/' + textToShare + linkToShare;
		//////////////////////////////////////Ti.API.info('url : ' + url);
		Ti.Platform.openURL(url);
	});

};

/*
 var googleAuth = new GoogleAuth({
 clientId : '378795929539-mdi8fvsaccfjtbvqrhu4ksqtc57747re.apps.googleusercontent.com',
 clientSecret : 'NYp7nJYDmVBpgrcKAoETtaGl',
 propertyName : 'googleToken',
 scope : ['https://www.googleapis.com/auth/tasks', 'https://www.googleapis.com/auth/tasks.readonly'],
 loginHint : 'someuser@gmail.com'
 });
 */
init();
