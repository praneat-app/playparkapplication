this.name = Alloy.Globals.PROMOTION_ACTIVITY;

var isLoadPromotionList = false;
var tabGameViewStart = Alloy.Globals.TITLE_CONTAINER_HIGTH - (Alloy.Globals.CONTAINER_HIGTH * 6);
//= Alloy.Globals.TITLE_CONTAINER_HIGTH - (Alloy.Globals.CONTAINER_HIGTH * 6);

var PlayParkMobileAPI = require("/api/PlayParkMobileAPI");
var DateTime = require("/utils/DateTime");
var _progressBar = null;
var ProgressBar = require("utils/ProgressBar");
_progressBar = new ProgressBar({
	parentView : $.contentContainer
});

if (Alloy.Globals.OS_IOS) {
	var MAX_TITLE_LENGTH = 80;
} else {
	var MAX_TITLE_LENGTH = Alloy.Globals.MAX_TITLE_LENGTH;
}

var tabGameBool = 0;
var menugameArray;
var category;
var idSelect;

var gameId = null;
var limit = PlayParkMobileAPI.LIMIT;
var limitLength = 10;
var _isFirstLoadJson = true;

function init() {
	var GoogleAnalytics = require("/api/GoogleAnalytics");
	var googleAnalytics = new GoogleAnalytics();
	googleAnalytics.trackScreen("Promotion");

	Alloy.Globals.SideMenuButton = Alloy.Globals.ds.SIDEMENU_OTHER;
	Alloy.Globals.ds.rightButton.setBackgroundImage("/images/other/rightButton_background_default.png");

	showProgressBar();

	getContent();
	$.tabGame.addEventListener("click", function(e) {
		onCheckTabGame();
	});

	$.promotionListView.addEventListener('itemclick', function(e) {

		if (category) {
			if (e.bindId == "promotionListViewNameLabel") {
				var gameId = category[e.itemIndex].gameid;
				checkRowNews(gameId);
				showProgressBar();

			} else {
				var html = category[e.itemIndex].html;
				var title = category[e.itemIndex].title;
				var link = category[e.itemIndex].link;

				var promotionDetail = Alloy.createController('detail/PromotionDetail', {
					htmlObject : html,
					titleShare : title,
					linkShare : link

				});
				promotionDetail.getView().open();
			}
		}
	});

	$.tabGameListView.addEventListener("itemclick", function(e) {
		refreshTabGameListview(e.itemIndex);
	});

	////setMaker////
	$.promotionListView.setMarker({
		sectionIndex : 0,
		itemIndex : (limit - 1)
	});

	$.promotionListView.addEventListener('marker', function(e) {
		var max = limit + limitLength;
		limit = limit + 1;
		getPromotionMaker(gameId, limit, max);
	});
}

var refreshControl = function(e) {
	showProgressBar();
	$.ptr.show();
	refreshTabGameListview(idSelect);
	setTimeout(function() {
		$.ptr.hide();
		hideProgressBar();
	}, 2000);
};

var checkRowNews = function(_idSelect) {
	var row = null;
	for (var i in menugameArray) {
		var object = JSON.stringify(menugameArray[i]);
		var data = JSON.parse(object);
		if (data.id == _idSelect) {
			row = i;
		}
	}
	$.promotionListView.removeAllChildren();
	refreshTabGameListview(row);
};

var refreshTabGameListview = function(itemIndex) {
	showProgressBar();
	tabGameBool = 1;
	onCheckTabGame();
	limit = PlayParkMobileAPI.LIMIT;

	$.promotionListView.setMarker({
		sectionIndex : 0,
		itemIndex : (limit - 1)
	});

	_isFirstLoadJson = true;

	createGameListview(menugameArray, itemIndex);
	if (itemIndex != undefined) {
		getPromotion(menugameArray[itemIndex].id);
	} else {
		getPromotion();
	}

};
///////Promotion maker////////////
var getPromotionMaker = function(itemId, to, from) {
	//////////////////Ti.API.info('getPromotionMaker itemId : ' + itemId);
	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		$.promotionListView.removeAllChildren();
		PlayParkMobileAPI.getPromotion(getPromotionMakerCallback, itemId, null, to, from);
	} else {
		//alert(Ti.Locale.getString("network_none_" + Alloy.Globals.LANGUAGE));
		limit = limit - 1;
	}
};

var getPromotionMakerCallback = function(_data) {
	if (_data) {
		if (_data.code == PlayParkMobileAPI.SUCCESS) {
			var message = _data.result.message;
			var categoryMaker = _data.result.category;
			//gameArray = category;
			createListview(categoryMaker);
		} else {
			//alert(Ti.Locale.getString("data_not_found_" + Alloy.Globals.LANGUAGE));
			limit = limit - 1;
			////////////////////Ti.API.info('getPromotionMakerCallback limit : ' + limit);
		}
	}

};

var getMenugame = function() {
	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		PlayParkMobileAPI.getMenugame(getMenugameCallback);
	} else {
		alert(Ti.Locale.getString("network_none_" + Alloy.Globals.LANGUAGE));
		hideProgressBar();
	}
};

var getMenugameCallback = function(_data) {

	if (_data) {
		if (_data.code == PlayParkMobileAPI.SUCCESS) {
			var message = _data.result.message;
			menugameArray = _data.result.menugame;

			if (!isLoadPromotionList) {
				refreshTabGameListview(null);
			};
		} else {
			menugameArray = null;

			var dialog = Ti.UI.createAlertDialog({
				cancel : 0,
				buttonNames : ['Cancel', 'OK'],
				message : Ti.Locale.getString("api_loading_failed_title"),
				title : Ti.Locale.getString("api_loading_failed_try_message")
			});
			dialog.addEventListener('click', function(e) {
				if (e.index !== e.source.cancel) {
					showProgressBar();
					getMenugame();
				}
			});
			dialog.show();
			hideProgressBar();
		}
	}
};

var getContent = function() {
	getMenugame();
	$.tabGameListView.setTop(tabGameViewStart);

	////////////////Ti.API.info('other_choose_game_title : ' + Ti.Locale.getString("other_choose_game_title_" + Alloy.Globals.LANGUAGE));
	$.tabGame.setTitle(Ti.Locale.getString("other_choose_game_title_" + Alloy.Globals.LANGUAGE));
};

var onCheckTabGame = function() {
	if (tabGameBool != 0) {
		$.tabGameListView.animate({
			top : tabGameViewStart,
			duration : Alloy.Globals.TIME_DURATION
		});
		$.tabGameImage.setImage("/images/other/other_tabgame_image.png");
		tabGameBool = 0;
	} else {
		$.tabGameListView.animate({
			top : Alloy.Globals.TITLE_CONTAINER_HIGTH,
			duration : Alloy.Globals.TIME_DURATION
		});
		$.tabGameImage.setImage("/images/other/other_tabgame_image_select.png");
		tabGameBool = 1;
	}
};
var getPromotion = function(itemId) {
	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		$.promotionListView.removeAllChildren();
		PlayParkMobileAPI.getPromotion(getPromotionCallback, itemId);
	} else {
		hideProgressBar();
		alert(Ti.Locale.getString("network_none_" + Alloy.Globals.LANGUAGE));
	}
};

var getPromotionCallback = function(_data) {
	if (_data) {
		if (_data.code == PlayParkMobileAPI.SUCCESS) {
			var message = _data.result.message;
			category = _data.result.category;
			//gameArray = category;
			createListview(category);
			hideProgressBar();
		} else {
			alert(Ti.Locale.getString("data_not_found_" + Alloy.Globals.LANGUAGE));
			hideProgressBar();
		}
	}

};
var createListview = function(_data) {
	//////////////////Ti.API.info('_data length : ' + _data.length);

	if (_data) {
		var items = [];
		for (var i in _data) {

			if (!_isFirstLoadJson) {
				category[category.length] = _data[i];
			}
			var object = JSON.stringify(_data[i]);
			var data = JSON.parse(object);
			var date = DateTime.getNormalDate(data.date);
			var title = Titanium.Network.decodeURIComponent(data.title);

			if (title) {
				if (title.length > MAX_TITLE_LENGTH) {
					title = title.substr(0, MAX_TITLE_LENGTH).trim() + "...";
				}
			}

			items.push({
				template : "template", // set the template
				promotionListViewNameLabel : {
					text : data.gametype
				},
				promotionListViewTimeLabel : {
					text : date
				},
				promotionListViewImageView : {
					image : data.image
				},
				promotionListViewDescipteLabel : {
					text : title
				}
			});
		}

		if (_isFirstLoadJson) {
			$.section.setItems(items);

			setTimeout(function() {
				$.promotionListView.scrollToItem(0, 0, Titanium.UI.iPhone.RowAnimationStyle.TOP);
			}, 500);

			_isFirstLoadJson = false;

		} else {

			$.section.appendItems(items);
			limit = limit + limitLength - 1;

			$.promotionListView.setMarker({
				sectionIndex : 0,
				itemIndex : (limit - 1)
			});
		}

	}

};

var createGameListview = function(_data, _idSelect) {
	idSelect = _idSelect;
	//////////////////////////////////Ti.API.info('createGameListview  idSelect : ' + idSelect + ", idSelect : " + _idSelect);
	if (_data) {
		var arrayLength = _data.length;
		if (arrayLength >= 6) {
			arrayLength = 6;
		};

		var lisviewHeight = (Alloy.Globals.CONTAINER_HIGTH * arrayLength);
		var tabGameViewStart1 = Alloy.Globals.TITLE_CONTAINER_HIGTH - lisviewHeight;

		$.tabGameListView.setTop(tabGameViewStart1);
		$.tabGameListView.setHeight(lisviewHeight);

		var backgroudSelect = "/images/other/other_tabgame_toggle_bg_select.png";
		var textColor = "white";

		var items = [];
		for (var i in _data) {
			var object = JSON.stringify(_data[i]);
			var data = JSON.parse(object);

			///ทำให้ id ที่เลือกเปลี่ยนสี
			if (_idSelect == i) {
				$.tabGame.setTitle(data.name);
				gameId = data.id;
				//////////////////Ti.API.info('gameId : ' + gameId);

				backgroudSelect = "/images/other/other_tabgame_toggle_bg_select.png";
				textColor = "white";
			} else {
				backgroudSelect = "transparent";
				textColor = "#707070";
			}

			items.push({
				template : "tabMenuGameTemplate",
				tabGameTitle : {
					text : data.name,
					color : textColor
				},
				tabGameListViewBg : {
					backgroundImage : backgroudSelect
				}
			});

		}
		$.tabGameSection.setItems(items);
	}

};

var showProgressBar = function() {
	_progressBar.show();
};

var hideProgressBar = function() {
	if (_progressBar != null) {
		setTimeout(function() {
			_progressBar.hide();
		}, 1000);
	}
};

init();
