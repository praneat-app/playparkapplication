this.name = Alloy.Globals.STREAMING_ACTIVITY;

var PlayParkMobileAPI = require("/api/PlayParkMobileAPI");
var DateTime = require("/utils/DateTime");

var _progressBar = null;
var ProgressBar = require("utils/ProgressBar");
_progressBar = new ProgressBar({
	parentView : $.contentContainer
});

var streamingObject = null;
var streammingListViewArray = [];
var newStreammingListViewArray = [];

var streamingObjectIdSelect;

var streammingListViewArraySelect;
var FormatCurrency = require("utils/FormatCurrency");
var VideoYoutube = require("utils/VideoYoutube");

var isTitleToggle = false;
var description = null;
var streammingScrollviewArray = [];
//1
var imageToggle;

var videoOpenView = null;

function init() {

	var GoogleAnalytics = require("/api/GoogleAnalytics");
	var googleAnalytics = new GoogleAnalytics();
	googleAnalytics.trackScreen("Streaming");

	showProgressBar();

	openData();

	Alloy.Globals.SideMenuButton = Alloy.Globals.ds.SIDEMENU_OTHER;
	Alloy.Globals.ds.rightButton.setBackgroundImage("/images/other/rightButton_background_default.png");

	imageToggle = {
		toggleOpen : "/images/video/video_detail_arrow_down.png",
		toggleClose : "/images/video/video_detail_arrow_up.png"
	};

	if (Alloy.Globals.OS_ANDROID) {

		videoOpenView = Titanium.UI.createView({
			height : Titanium.UI.FILL,
			width : Titanium.UI.FILL,
			backgroundColor : "black",
			zIndex : 10
		});
		$.videoView.add(videoOpenView);
	}

	//tab
	$.tabStreaming.addEventListener("click", function(e) {
		//Ti.API.info('Ti.Locale.getString("live_th") : ' + Ti.Locale.getString("live_th"));
		Alloy.Globals.navLabel.setText(Ti.Locale.getString("live_th"));

		showProgressBar();
		$.tabMenuLeftView.setOpacity(1.0);
		$.tabMenuRightView.setOpacity(0.5);

		if (Alloy.Globals.OS_ANDROID) {
			$.videoView.removeAllChildren();
			$.videoView.add(videoOpenView);
		}

		getLiveSteamAll();
		$.tabGameChannelView.setHeight(0);
		$.tabStreamingView.setHeight(Ti.UI.FILL);
		$.gameChannelListview.removeAllChildren();
	});

	$.tabGameChanel.addEventListener("click", function(e) {
		//Ti.API.info('Ti.Locale.getString("game_channel_th") : '+Ti.Locale.getString("game_channel_th"));
		Alloy.Globals.navLabel.setText(Ti.Locale.getString("game_channel_th"));

		showProgressBar();
		$.videoView.removeAllChildren();
		$.tabMenuLeftView.setOpacity(0.5);
		$.tabMenuRightView.setOpacity(1.0);

		getGameChannel();

		if (Alloy.Globals.OS_IOS) {
			$.tabStreamingView.setHeight(0);
			$.videoTitleText.setText("");
			$.socialViewerLabel.setText("");
		}

		$.tabGameChannelView.setHeight(Ti.UI.FILL);
		$.streammingListView.removeAllChildren();
	});

	$.gameChannelListview.addEventListener("itemclick", function(e) {

		if (streamingObject) {
			var streaming = streamingObject[e.itemIndex];
			var streamingDetail = Alloy.createController('detail/StreamingDetailActivity', {
				streamingObject : streaming
			});
			streamingDetail.getView().open();
		}

	});

	$.streammingListView.addEventListener("itemclick", function(e) {
		//////////////////Ti.API.info('streammingListView >>> itemclick >>> e.itemIndex : ' + e.itemIndex);
		//////////////////Ti.API.info('streammingListView >>> itemclick >>> streammingListViewArray : ' + streammingListViewArray);
		//////////////////Ti.API.info('streammingListView >>> itemclick >>> streammingScrollviewArray : ' + streammingScrollviewArray);

		if (Alloy.Globals.OS_IOS) {
			if (streammingListViewArray.length > 0) {
				showProgressBar();
				streamingObjectIdSelect = null;
				if (streammingScrollviewArray) {//2
					createStreamingScrollview(streammingScrollviewArray);
				};
				createVideoPlayer(streammingListViewArray[e.itemIndex]);
				hideProgressBar();
			}
		} else {

			if (streammingListViewArray.length > 0) {
				if (e.itemIndex == 0) {

					var object = JSON.stringify(streammingListViewArraySelect);
					var data = JSON.parse(object);
					var date = DateTime.getStreamingDateTime(data.date, DateTime.MODE_DATE);
					var des = data.detail;
					var items = {};

					if (!isTitleToggle) {
						isTitleToggle = true;

						items = {
							template : "templateToggle",
							videoTitleText : {
								text : data.title
							},
							videoTitleViewsText : {
								text : date
							},
							videoDes : {
								height : Ti.UI.SIZE
							},
							videoDesciption : {
								text : des
							}
						};

					} else {
						isTitleToggle = false;
						items = {
							template : "templateToggle",
							videoTitleText : {
								text : data.title
							},
							videoTitleViewsText : {
								text : date
							},
							videoDes : {
								height : 0
							},
							videoDesciption : {
							}
						};
					}
					$.streamSection.updateItemAt(e.itemIndex, items);
				} else {

					if (Alloy.Globals.OS_ANDROID) {
						$.videoView.removeAllChildren();
						$.videoView.add(videoOpenView);
					}
					showProgressBar();
					streamingObjectIdSelect = null;
					var itemIndex = e.itemIndex - 1;
					createStreamingListview(newStreammingListViewArray, itemIndex);
					hideProgressBar();
				}
			}
		}

	});

	$.tabGooglePButton.addEventListener("click", function() {
		if (streammingListViewArraySelect) {
			var linkShare = streammingListViewArraySelect.url_share;
			var titleShare = streammingListViewArraySelect.title;
			var googlePlusView = Alloy.createController('view/GooglePlus', {
				titleShare : titleShare,
				linkShare : linkShare
			});
			googlePlusView.getView();
		}
	});

	$.tabFacebookButton.addEventListener("click", function() {
		if (streammingListViewArraySelect) {
			$.tabFacebookButton.setEnabled(false);
			var linkShare = streammingListViewArraySelect.url_share;
			var titleShare = streammingListViewArraySelect.title;
			var Facebook = require("/social/Facebook");
			var facebook = new Facebook();
			facebook.post(linkShare, Ti.Locale.getString("facebook_share_name"), "", titleShare, $.tabFacebookButton);
		}
	});

	$.tabLineButton.addEventListener("click", function() {
		if (streammingListViewArraySelect) {
			var titleShare = encodeURIComponent(streammingListViewArraySelect.title + " ");
			var linkShare = encodeURIComponent(streammingListViewArraySelect.url_share);
			var url = 'line://msg/text/' + titleShare + linkShare;
			Ti.Platform.openURL(url);
		}
	});

	if (Alloy.Globals.OS_IOS) {//IOS
		$.videoTitleToggle.addEventListener("click", function() {
			if (!isTitleToggle) {
				var des = Titanium.Network.decodeURIComponent(description);
				$.videoDesciption.setText(des);
				isTitleToggle = true;
				$.videoTitleViewRight.setImage(imageToggle.toggleOpen);
				$.videoDes.setHeight(Ti.UI.SIZE);

			} else {
				$.videoDesciption.setText("");
				isTitleToggle = false;
				$.videoTitleViewRight.setImage(imageToggle.toggleClose);
				$.videoDes.setHeight(0);
			}
		});
	}

};

var refreshControl = function(e) {
	showProgressBar();
	$.ptr.show();
	getGameChannel();
	setTimeout(function() {
		$.ptr.hide();
		hideProgressBar();
	}, 2000);
};

var openData = function() {
	$.tabMenuLeftView.setOpacity(1.0);
	$.tabMenuRightView.setOpacity(0.5);
	$.tabMenuLeftLabel.setText(Ti.Locale.getString("streaming_tab_streaming_" + Alloy.Globals.LANGUAGE));
	$.tabMenuRightLabel.setText(Ti.Locale.getString("streaming_tab_game_chanel_" + Alloy.Globals.LANGUAGE));
	$.tabGameChannelView.setHeight(0);

	getLiveSteamAll();
};
var checkLiveStreaming = function(_data) {
	////////////Ti.API.info('checkLiveStreaming >>> data :' + _data);

	if (Alloy.Globals.OS_ANDROID) {
		var videoArray = [];
		var videoArray1 = [];
		for (var i in _data) {
			var object = JSON.stringify(_data[i]);
			var data = JSON.parse(object);
			if (data.status == 1) {
				videoArray[videoArray.length] = _data[i];
				videoArray1[videoArray1.length] = _data[i];
			}
		}

		for (var j in _data) {
			var object = JSON.stringify(_data[j]);
			var data = JSON.parse(object);

			if (data.status == 2) {
				videoArray[videoArray.length] = _data[j];
			}
		}

		streammingListViewArray = videoArray;

		////////////Ti.API.info('videoArray >> ' + videoArray.length);
		////////////Ti.API.info('videoArray1 >> ' + videoArray1.length);

		if (videoArray.length > 0) {
			if (videoArray1.length > 0) {
				createStreamingListview(videoArray, 0);
			} else {
				createStreamingListview(videoArray, null);
			}
		};

	} else {

		var videoArray1 = [];
		var videoArray2 = [];
		var videoArray3 = [];

		for (var i in _data) {
			var object = JSON.stringify(_data[i]);
			var data = JSON.parse(object);

			if (data.status == 1) {
				videoArray1[videoArray1.length] = _data[i];
			} else if (data.status == 2) {
				videoArray2[videoArray2.length] = _data[i];
			} else {
				videoArray3[videoArray3.length] = _data[i];
			}
		}

		//////////////////Ti.API.info('checkLiveStreaming >>> videoArray1.length : ' + videoArray1.length);
		//////////////////Ti.API.info('checkLiveStreaming >>> videoArray2.length : ' + videoArray2.length);
		//////////////////Ti.API.info('checkLiveStreaming >>> videoArray3.length : ' + videoArray3.length);

		if (videoArray1.length > 0) {
			streammingScrollviewArray = videoArray1;
			//4

			//////////////////Ti.API.info('checkLiveStreaming >>> videoArray1.length : ' + videoArray1.length);
			//////////////////Ti.API.info('checkLiveStreaming >>> streammingScrollviewArray.length1 : ' + streammingScrollviewArray.length);

			showProgressBar();
			var lowSelect = 0;
			createStreamingScrollview(videoArray1, lowSelect);
			hideProgressBar();
		} else {
			$.videoView.setHeight(0);
			$.videoSocialView.setHeight(0);
			$.videoTitleToggle.setHeight(0);
			$.streamingTabView.setHeight(0);
			$.streamingScrollview.setHeight(0);
		}

		if (videoArray2.length > 0) {
			//////////////////Ti.API.info('checkLiveStreaming >>> streammingScrollviewArray.length2 : ' + streammingScrollviewArray.length);

			createStreamingListview(videoArray2, streammingScrollviewArray);
			//streamingObject = videoArray3;
		} else {
			$.waitStreamingTabView.setHeight(0);
			$.streammingListView.setHeight(0);
		}
	}

};
///////////////////////////////////////////////
var createStreamingListview = function(_data, _idselect) {
	//////////////////Ti.API.info('createStreamingListview >>> streammingListViewArray : ' + streammingListViewArray);
	//////////////////Ti.API.info('createStreamingListview >>> streammingScrollviewArray : ' + streammingScrollviewArray);
	//////////////////Ti.API.info('createStreamingListview >>> _idSelect : ' + _idSelect);

	if (Alloy.Globals.OS_IOS) {
		streammingListViewArray = _data;

		if (streammingListViewArray) {

			if (streammingScrollviewArray.length == 0) {
				var object = JSON.stringify(_data[0]);
				var data = JSON.parse(object);
				createVideoPlayer(data);
			}

			if (streammingScrollviewArray.length == 0 && streammingListViewArray.length == 1) {
				$.waitStreamingTabView.setHeight(0);
				$.streammingListView.setHeight(0);

			} else {
				var items = [];
				var arrayLength = _data.length;

				for (var i in _data) {
					var object = JSON.stringify(_data[i]);
					var data = JSON.parse(object);
					var tamplateStyle = null;

					if (i == 0) {
						tamplateStyle = "templateStart";
					} else if (i == (arrayLength - 1)) {
						tamplateStyle = "templateEnd";
					} else {
						tamplateStyle = "template";
					}
					////////////////////////////////////Ti.API.info('data : ' + data.title);

					var date = DateTime.getStreamingDateTime(data.date, DateTime.MODE_DATE);
					var time = DateTime.getStreamingDateTime(data.date, DateTime.MODE_TIME);
					items.push({
						template : tamplateStyle, // set the template
						streamingImageView : {
							image : data.image
						},
						streamingNameLabel : {
							text : data.title
						},
						streamingDateLabel : {
							text : date
						},
						streamingTimeLabel : {
							text : time
						}
					});
				}
				$.streamSection.setItems(items);
			}
		};

	} else {

		if (!streammingListViewArray) {
			streammingListViewArray = _data;
		}

		var items = [];
		var start1 = null;
		var start2 = null;
		var arrayLength = _data.length;
		newStreammingListViewArray = [];

		/////////////////////////////

		$.videoView.setHeight(Alloy.Globals.VideoViewHeight);
		$.videoSocialView.setHeight(45);

		if (_idselect != undefined) {

			var object1 = JSON.stringify(_data[_idselect]);
			var data1 = JSON.parse(object1);
			_idselect = data1.id;
		}

		for (var k in streammingListViewArray) {
			////////////////Ti.API.info('streammingListViewArray  k :' + k);

			var object = JSON.stringify(streammingListViewArray[k]);
			var data = JSON.parse(object);
			var id1 = data.id;
			////////////////Ti.API.info('id1 :' + id1);

			if (id1 == _idselect) {
				////////////////Ti.API.info('_idselect : ' + _idselect + ", data.id : " + data.id);
				streamingObjectIdSelect = streammingListViewArray[k];
			} else {
				newStreammingListViewArray[newStreammingListViewArray.length] = streammingListViewArray[k];
				////////////////Ti.API.info('_data[k] : ' + streammingListViewArray[k]);
			}
		}

		if (_idselect != undefined) {
			if (streamingObjectIdSelect) {
				createVideoPlayer(streamingObjectIdSelect);
				var object = JSON.stringify(streamingObjectIdSelect);
				var data = JSON.parse(object);
				var date = DateTime.getStreamingDateTime(data.date, DateTime.MODE_DATE);

				items.push({
					template : "templateToggle",
					videoTitleText : {
						text : data.title
					},
					videoTitleViewsText : {
						text : date
					},
					videoDes : {
						height : 0
					},
					videoDesciption : {
					}
				});
			}

		} else {
			$.videoView.setHeight(0);
			$.videoSocialView.setHeight(0);
		}

		for (var i in newStreammingListViewArray) {
			var object = JSON.stringify(newStreammingListViewArray[i]);
			var data = JSON.parse(object);
			if (data.status == 1) {
				start1 = i;
				break;
			}
		}
		for (var j in newStreammingListViewArray) {
			var object = JSON.stringify(newStreammingListViewArray[j]);
			var data = JSON.parse(object);
			if (data.status == 2) {
				start2 = j;
				break;
			}
		}
		//////////////////Ti.API.info('newStreammingListViewArray : ' + newStreammingListViewArray.length);
		for (var i in newStreammingListViewArray) {
			//////////////////Ti.API.info('newStreammingListViewArray : ' + newStreammingListViewArray + "i : " + i);
			var object = JSON.stringify(newStreammingListViewArray[i]);
			var data = JSON.parse(object);
			var tamplateStyle = null;
			var tabTitle = "";
			var tabColor = "";

			if (i == start1) {
				tabTitle = Ti.Locale.getString("streaming_tab1");
				tamplateStyle = "templateStart";
				tabColor = "#333333";
			} else if (i == start2) {
				tabTitle = Ti.Locale.getString("streaming_tab2");
				tamplateStyle = "templateStart";
				tabColor = "#ff860b";
			} else if (i == (start2 - 1) || i == _data.length) {
				tamplateStyle = "templateEnd";
			} else {
				tamplateStyle = "template";
			}
			//////////////////////////////Ti.API.info('data : ' + data.title);
			var date = DateTime.getStreamingDateTime(data.date, DateTime.MODE_DATE);
			var time = DateTime.getStreamingDateTime(data.date, DateTime.MODE_TIME);
			var imageEncoded = encodeURI(data.image);

			items.push({
				template : tamplateStyle, // set the template
				streamingImageView : {
					//image : data.image
					image : imageEncoded
				},
				streamingTab : {
					backgroundColor : tabColor
				},
				streamingTabText : {
					text : tabTitle
				},
				streamingNameLabel : {
					text : data.title
				},
				streamingDateLabel : {
					text : date
				},
				streamingTimeLabel : {
					text : time
				}
			});
		}
		$.streamSection.setItems(items);

	}

};
///////////////////////////////////////////////

////////////////////////////////////////////
var createListview = function(_data) {
	var items = [];

	for (var i in _data) {
		var object = JSON.stringify(_data[i]);
		var data = JSON.parse(object);

		var template = "template";
		if (i == 0) {
			template = "templateStart";
		}

		var views = FormatCurrency.getFormatCurrency(data.countviews);

		items.push({
			template : template, // set the template
			gameChannelThumbImage : {
				image : data.picgame
			},
			gameChannelNameLabel : {
				text : data.name
			},
			gameChannelViewerLabel : {
				text : views
			}
		});

	}
	$.section.setItems(items);

	setTimeout(function() {
		$.gameChannelListview.scrollToItem(0, 0, Titanium.UI.iPhone.RowAnimationStyle.TOP);
	}, 500);

	//hideProgressBar();
};

//////get streaming//////////
var getLiveSteamAll = function() {
	//////////////////////////////Ti.API.info('streamingAll : Now');
	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		PlayParkMobileAPI.getLiveSteamAll(getLiveSteamAllCallback, 50, 50);
	} else {
		hideProgressBar();
		alert(Ti.Locale.getString("network_none_" + Alloy.Globals.LANGUAGE));
	}
};

var getLiveSteamAllCallback = function(_data) {
	//////////////////////////////Ti.API.info('getLiveSteamAllCallback >> data : ' + _data.code);
	if (_data) {
		if (_data.code == PlayParkMobileAPI.SUCCESS) {
			var message = _data.result.message;
			var objectStreaming = _data.result.steaming;
			//TODO checkLivew
			checkLiveStreaming(objectStreaming);

			/*	var streamingArray = createStreamingJson();
			 //////////////////Ti.API.info('streamingArray : ' + streamingArray.length);
			 checkLiveStreaming(streamingArray);*/

			hideProgressBar();

		} else {
			hideProgressBar();
			alert(Ti.Locale.getString("data_not_found_" + Alloy.Globals.LANGUAGE));
		}
	} else {
		hideProgressBar();
	}

};

var createVideoPlayer = function(_data) {
	streammingListViewArraySelect = _data;
	var url;

	if (Alloy.Globals.OS_IOS) {
		if (Alloy.Globals.OS_IOS_IPHONE) {
			$.videoView.setHeight(180);
			$.videoSocialView.setHeight(45);
		} else {
			$.videoView.setHeight(427);
			$.videoSocialView.setHeight(55);
		}
	} else {
		$.videoView.setHeight(Alloy.Globals.VideoViewHeight);
		$.videoSocialView.setHeight(45);
	}
	//contentOffset
	var object = JSON.stringify(_data);
	var data = JSON.parse(object);
	var views = FormatCurrency.getFormatCurrency(_data.views);
	description = _data.detail;
	var dataUrl = _data.url;
	url = dataUrl.split("=");

	if (Alloy.Globals.OS_IOS) {
		$.videoTitleText.setText(_data.title);
		$.socialViewerLabel.setText(views);
		$.videoTitleToggle.setHeight(Ti.UI.SIZE);
		$.videoView.removeAllChildren();
	}

	//////////////////////Ti.API.info('url[1] : ' + url[1]);
	VideoYoutube.h264videosWithYoutubeURL(url[1], function(_video, _image) {

		if (_video) {

			var videoPlayer = Titanium.Media.createVideoPlayer({
				ns : Ti.Media,
				height : Titanium.UI.FILL,
				width : Titanium.UI.FILL,
				autoplay : false,
				backgroundColor : "black",
				top : 0,
				left : 0,
				scalingMode : Titanium.Media.VIDEO_SCALING_ASPECT_FIT,
				mediaControlMode : Titanium.Media.VIDEO_CONTROL_NONE,
				autoplay : true,
				url : _video,
				zIndex : 8
			});
			$.videoView.add(videoPlayer);

			if (Alloy.Globals.OS_ANDROID) {

				videoPlayer.addEventListener("load", function(e) {
					setTimeout(function() {
						////////Ti.API.info('load');
						$.videoView.remove(videoOpenView);
					}, 1000);
				});
			}
		} else {
			if (Alloy.Globals.OS_ANDROID) {
				$.videoView.remove(videoOpenView);
			}
			var image = Ti.UI.createImageView({
				image : _image,
				height : Ti.UI.FILL,
				width : Ti.UI.FILL,
				backgroundColor : "black"

			});
			$.videoView.add(image);

		}
	}, function(e) {
		alert(Ti.Locale.getString("data_not_found_" + Alloy.Globals.LANGUAGE));
	});
	if (Alloy.Globals.OS_IOS) {
		setTimeout(function() {
			$.scrollViewContainer.setContentOffset({
				x : 0,
				y : 0
			}, {
				animated : true
			});
		}, 500);
	}

};

////////////GetGameChannel////////////
var getGameChannel = function(itemId) {

	if (Ti.Network.networkType != Ti.Network.NETWORK_NONE) {
		$.gameChannelListview.removeAllChildren();
		PlayParkMobileAPI.getGameChannel(getGameChannelCallback);
	} else {
		hideProgressBar();
		alert(Ti.Locale.getString("network_none_" + Alloy.Globals.LANGUAGE));
	}
};
var getGameChannelCallback = function(_data) {
	////////////////////////Ti.API.info('getGameChannelCallback >> data : ' + _data.code);
	if (_data) {
		if (_data.code == PlayParkMobileAPI.SUCCESS) {
			var message = _data.result.message;
			streamingObject = _data.result.gamechannel;
			createListview(streamingObject);
			hideProgressBar();
		} else {
			hideProgressBar();
			alert(Ti.Locale.getString("data_not_found_" + Alloy.Globals.LANGUAGE));
		}
	} else {
		hideProgressBar();
	}
};
var showProgressBar = function() {
	_progressBar.show();
};

var hideProgressBar = function() {
	if (_progressBar != null) {
		setTimeout(function() {
			_progressBar.hide();
		}, 1000);
	}
};
/*
 var createStreamingJson = function() {
 var jsonObject = {
 "steaming" : [{
 "id" : "11",
 "status" : 1,
 "title" : "hhhhhhhh",
 "url" : "http://www.youtube.com/watch?v=Ql3E1XW8xZE",
 "views" : "702",
 "image" : "http://img.youtube.com/vi/zBAwxUPIDac/0.jpg",
 "date" : "2014-10-04 18:50:06",
 "detail" : "hhhhhhhhh",
 "url_share" : "http://live.playpark.com/th/stream.php?id=270"

 }, {
 "id" : "22",
 "status" : 1,
 "title" : "avcdaf;adsfjasl",
 "url" : "http://www.youtube.com/watch?v=Ql3E1XW8xZE",
 "views" : "702",
 "image" : "http://img.youtube.com/vi/zBAwxUPIDac/0.jpg",
 "date" : "2014-10-04 18:50:06",
 "detail" : "hhhhhhhhh",
 "url_share" : "http://live.playpark.com/th/stream.php?id=270"

 }, {
 "id" : "235",
 "status" : 1,
 "title" : "hhhhhhhh",
 "url" : "http://www.youtube.com/watch?v=Ql3E1XW8xZE",
 "views" : "702",
 "image" : "http://img.youtube.com/vi/zBAwxUPIDac/0.jpg",
 "date" : "2014-10-04 18:50:06",
 "detail" : "hhhhhhhhh",
 "url_share" : "http://live.playpark.com/th/stream.php?id=270"

 }, {
 "id" : "667",
 "status" : 2,
 "title" : "ggggggggg",
 "url" : "http://www.youtube.com/watch?v=Ql3E1XW8xZE",
 "views" : "28",
 "image" : "http://img.youtube.com/vi/zBAwxUPIDac/0.jpg",
 "date" : "2014-11-05 17:55:29",
 "detail" : "ggggggggggggg",
 "url_share" : "http://live.playpark.com/th/stream.php?id=270",

 }, {
 "id" : "999",
 "status" : 2,
 "title" : "ffffffff",
 "url" : "http://www.youtube.com/watch?v=Ql3E1XW8xZE",
 "views" : "28",
 "image" : "http://img.youtube.com/vi/zBAwxUPIDac/0.jpg",
 "date" : "2014-11-05 17:55:29",
 "detail" : "ffffffff",
 "url_share" : "http://live.playpark.com/th/stream.php?id=270"
 }, {
 "id" : "887",
 "status" : 2,
 "title" : "dfadfadfadfadfadfa",
 "url" : "http://www.youtube.com/watch?v=Ql3E1XW8xZE",
 "image" : "http://img.youtube.com/vi/YFocv8_EwUM/0.jpg",
 "views" : "7",
 "date" : "2014-11-18 16:08:19",
 "detail" : "รายการ หัวหกก้นขวิด...ซ๊ด สด!! ประจำวันที่ 12 พฤศจิกายน 2557 เวลา 18.00 - 19.00 น. พร้อมก...",
 "url_share" : "http://live.playpark.com/th/stream.php?id=283"
 }, {
 "id" : "678",
 "status" : 2,
 "title" : "หัวหกก้นขวิด...ซ๊ด สด!! 12 พฤศจิกายน 2557 เวลา 18.00 - 19.00 น.",
 "url" : "http://www.youtube.com/watch?v=Ql3E1XW8xZE",
 "image" : "http://img.youtube.com/vi/YFocv8_EwUM/0.jpg",
 "views" : "7",
 "date" : "2014-11-18 16:08:19",
 "detail" : "รายการ หัวหกก้นขวิด...ซ๊ด สด!! ประจำวันที่ 12 พฤศจิกายน 2557 เวลา 18.00 - 19.00 น. พร้อมก...",
 "url_share" : "http://live.playpark.com/th/stream.php?id=283"

 }]
 };

 var myObjectString = JSON.stringify(jsonObject);
 var myNewObject = JSON.parse(myObjectString);
 var newsArray = myNewObject.steaming;
 return newsArray;
 };
 */
var createStreamingScrollview = function(_streamingView, _idSelect) {

	//////////////////////Ti.API.info('_streamingView : ' + _streamingView + ", _idSelect : " + _idSelect);
	var iconViewWidth = 120;
	var imageHeight = 73;
	var iconViewHeight = 130;
	var iconViewStart = 15;
	var iconViewLeftPadding = 17;
	var listViewBoxViewMargin = 8;
	var labelHeight = 20;

	if (Alloy.Globals.OS_IOS_IPAD) {
		iconViewWidth = 180;
		imageHeight = 109;
		iconViewHeight = 195;
		iconViewStart = 22;
		iconViewLeftPadding = 22;
		listViewBoxViewMargin = 19;
		labelHeight = 25;
	}

	var _scrollviewArray = [];

	if (_idSelect != undefined) {
		for (var k in _streamingView) {
			var object = JSON.stringify(_streamingView[k]);
			var data = JSON.parse(object);
			var idStreaming = _streamingView[k];
			if (_idSelect == k) {
				createVideoPlayer(_streamingView[k]);
			} else {
				_scrollviewArray[_scrollviewArray.length] = _streamingView[k];
			}
		}
	} else if (streamingObjectIdSelect != undefined) {

		for (var k in _streamingView) {
			var object = JSON.stringify(_streamingView[k]);
			var data = JSON.parse(object);
			if (streamingObjectIdSelect == data.id) {
				createVideoPlayer(_streamingView[k]);
			} else {
				_scrollviewArray[_scrollviewArray.length] = _streamingView[k];
			}
		}
	} else {
		_scrollviewArray = _streamingView;
	}

	if (Alloy.Globals.OS_IOS) {
		$.streamingScrollview.removeAllChildren();
	}

	if (_scrollviewArray.length > 0) {
		$.streamingTabView.setHeight(30);
		if (Alloy.Globals.OS_IOS) {

			$.streamingScrollview.setHeight(Titanium.UI.SIZE);
		}

		var iconLength = _scrollviewArray.length;
		var scrollViewWidth = iconViewStart + (iconViewWidth * iconLength) + (iconViewLeftPadding * iconLength);
		$.streamingScrollview.setContentWidth(scrollViewWidth);

		for (var j in _scrollviewArray) {
			var object = JSON.stringify(_scrollviewArray[j]);
			var data = JSON.parse(object);

			var iconPaddingLeft = iconViewStart + (iconViewWidth * j) + (iconViewLeftPadding * j);
			var iconStreamingView;

			if (Alloy.Globals.OS_IOS) {
				iconStreamingView = Ti.UI.createButton({
					left : iconPaddingLeft,
					top : 5,
					bottom : 5,
					right : listViewBoxViewMargin,
					width : iconViewWidth,
					height : iconViewHeight,
					layout : "vertical",
					_object : data
				});
			} else {
				iconStreamingView = Ti.UI.createView({
					left : iconPaddingLeft,
					top : 5,
					bottom : 5,
					right : listViewBoxViewMargin,
					width : iconViewWidth,
					height : iconViewHeight,
					layout : "vertical",
					_object : data
				});
			}

			////////////////////////////////Ti.API.info(' data.image : '+ data.image);
			var iconImageView = Ti.UI.createImageView({
				image : data.image,
				width : Ti.UI.FILL,
				height : imageHeight,
				backgroundColor : "#333333"
			});

			var iconStatus = Ti.UI.createView({
				width : Ti.UI.FILL,
				height : labelHeight,
				left : 0
			});

			var iconStatusImage = Ti.UI.createImageView({
				width : Ti.UI.SIZE,
				height : Ti.UI.SIZE,
				image : "/images/stream/stream_status_live.png",
				left : 0
			});

			var iconGameLabel = Ti.UI.createLabel({
				width : Ti.UI.FILL,
				height : labelHeight,
				left : 15,
				textAlign : "left",
				text : data.title,
				color : "#444444",
				font : {
					fontSize : Alloy.Globals.FONT_SIZE_22,
					fontFamily : Alloy.Globals.FONT_DEFAULT,
					fontWeight : "bold"
				},
				wordWrap : true

			});

			iconStatus.add(iconStatusImage);
			iconStatus.add(iconGameLabel);

			var date = DateTime.getStreamingDateTime(data.date, DateTime.MODE_DATE);
			var time = DateTime.getStreamingDateTime(data.date, DateTime.MODE_TIME);

			var iconDateLabel = Ti.UI.createLabel({
				width : Ti.UI.FILL,
				height : labelHeight,
				text : date,
				textAlign : "left",
				color : "#999999",
				font : {
					fontSize : Alloy.Globals.DEVICE_FONT_18SP,
					fontFamily : Alloy.Globals.customFontThaiSansNeue
				},
				left : 0
			});

			var iconTimeView = Ti.UI.createView({
				width : Ti.UI.FILL,
				height : Ti.UI.SIZE,
				left : 0,
				layout : "horizontal"
			});

			var iconClockImage = Ti.UI.createImageView({
				width : Ti.UI.SIZE,
				height : Ti.UI.SIZE,
				image : "/images/stream/stream_clock.png",
				right : 5

			});

			var iconTimeLabel = Ti.UI.createLabel({
				width : Ti.UI.FILL,
				height : labelHeight,
				left : 0,
				text : time,
				textAlign : "left",
				color : "#999999",
				font : {
					fontSize : Alloy.Globals.DEVICE_FONT_18SP,
					fontFamily : Alloy.Globals.customFontThaiSansNeue
				},
				left : 0
			});

			iconTimeView.add(iconClockImage);
			iconTimeView.add(iconTimeLabel);

			iconStreamingView.add(iconImageView);
			iconStreamingView.add(iconStatus);
			iconStreamingView.add(iconDateLabel);
			iconStreamingView.add(iconTimeView);

			iconStreamingView.addEventListener("click", function(e) {
				var _data = e.source._object;
				streamingObjectIdSelect = _data.id;
				createStreamingScrollview(streammingScrollviewArray);
			});
			$.streamingScrollview.add(iconStreamingView);
		}

	} else {
		$.streamingTabView.setHeight(0);
		$.streamingScrollview.setHeight(0);
	}
};

init();
