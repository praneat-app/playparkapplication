//---- CONSTANT ----
Alloy.Globals.DEBUG = false;
Alloy.Globals.build = 100;
Alloy.Globals.OS_IOS = (Ti.Platform.osname == "android") ? false : true;
Alloy.Globals.OS_ANDROID = (Alloy.Globals.OS_IOS) ? false : true;
Alloy.Globals.OS_IOS_IPHONE = (Ti.Platform.osname == "ipad") ? false : true;
Alloy.Globals.OS_IOS_IPAD = (Ti.Platform.osname == "ipad") ? true : false;

Alloy.Globals.build = 100;

if (Alloy.Globals.OS_ANDROID) {
	Ti.Android.currentActivity.setRequestedOrientation(Ti.Android.SCREEN_ORIENTATION_PORTRAIT);
}

Alloy.Globals.HOST_API = "http://www.playpark.com/th/function/";

Alloy.Globals.LANGUAGE_EN = "en";
Alloy.Globals.LANGUAGE_TH = "th";

/** Network **/
Alloy.Globals.HTTP_TIME_OUT = 20000;

Alloy.Globals.OS_VERSION = parseFloat(Ti.Platform.version);

Alloy.Globals.KEYBOARD_HEIGHT = 216;

if (Ti.Platform.displayCaps.platformWidth > Ti.Platform.displayCaps.platformHeight) {
	Alloy.Globals.SCREEN_WIDTH = Ti.Platform.displayCaps.platformHeight;
	Alloy.Globals.SCREEN_HEIGHT = Ti.Platform.displayCaps.platformWidth;
} else {
	Alloy.Globals.SCREEN_WIDTH = Ti.Platform.displayCaps.platformWidth;
	Alloy.Globals.SCREEN_HEIGHT = Ti.Platform.displayCaps.platformHeight;
}

Alloy.Globals.DENSITY_SCALE = (Ti.Platform.displayCaps.dpi / 160);

if (Alloy.Globals.OS_IOS) {

	Alloy.Globals.HIGHLIGHT_BUTTON_WIDTH = Alloy.Globals.SCREEN_WIDTH / 3;

	Alloy.Globals.STATUS_BAR_HEIGHT = 20;
	Alloy.Globals.TIME_DURATION = 300;

	////image iphone === > ipad
	Alloy.Globals.NAV_HEIGHT = 45;

	Alloy.Globals.TITLE_CONTAINER_HIGTH = 35;
	Alloy.Globals.CONTAINER_HIGTH = 45;

	Alloy.Globals.LISTVIEW_DOUBLETAB_TOP = Alloy.Globals.TITLE_CONTAINER_HIGTH + Alloy.Globals.TITLE_CONTAINER_HIGTH;
	////font iphone
	Alloy.Globals.DEVICE_FONT_NAV_LABEL = "26sp";

	Alloy.Globals.DEVICE_FONT_25SP = "25sp";
	Alloy.Globals.DEVICE_FONT_22SP = "22sp";
	Alloy.Globals.DEVICE_FONT_20SP = "20sp";
	Alloy.Globals.DEVICE_FONT_18SP = "18sp";
	Alloy.Globals.DEVICE_FONT_16SP = "16sp";
	Alloy.Globals.DEVICE_FONT_15SP = "15sp";

	Alloy.Globals.BUTTON_HALF_WIDTH = Alloy.Globals.SCREEN_WIDTH / 2;

	if (Alloy.Globals.OS_IOS_IPAD) {
		////image ipad
		Alloy.Globals.NAV_HEIGHT = 48;

		Alloy.Globals.TITLE_CONTAINER_HIGTH = 45;
		Alloy.Globals.CONTAINER_HIGTH = 55;

		Alloy.Globals.LISTVIEW_DOUBLETAB_TOP = Alloy.Globals.TITLE_CONTAINER_HIGTH + Alloy.Globals.TITLE_CONTAINER_HIGTH;

		////font iphone === > ipad
		Alloy.Globals.DEVICE_FONT_NAV_LABEL = "32sp";
		Alloy.Globals.DEVICE_FONT_25SP = "30sp";
		Alloy.Globals.DEVICE_FONT_22SP = "27sp";
		Alloy.Globals.DEVICE_FONT_20SP = "25sp";
		Alloy.Globals.DEVICE_FONT_18SP = "22sp";
		Alloy.Globals.DEVICE_FONT_16SP = "21sp";
		Alloy.Globals.DEVICE_FONT_15SP = "20sp";

	}

} else {//Android

	///////////
	var dimen = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.dimenssss);
	////////////////Ti.API.info('dimenssss : ' + dimen);
	///////////

	var StringAdapter = require("/utils/StringAdapter");

	Alloy.Globals.SCREEN_WIDTH = Alloy.Globals.SCREEN_WIDTH / Alloy.Globals.DENSITY_SCALE;
	Alloy.Globals.SCREEN_HEIGHT = Alloy.Globals.SCREEN_HEIGHT / Alloy.Globals.DENSITY_SCALE;

	Alloy.Globals.BUTTON_HALF_WIDTH = Alloy.Globals.SCREEN_WIDTH / 2;
	Alloy.Globals.HIGHLIGHT_BUTTON_WIDTH = Math.floor(Alloy.Globals.SCREEN_WIDTH / 3);

	Alloy.Globals.NAV_HEIGHT = StringAdapter.getDimension(Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.nav_height));

	var navLogoHeight = StringAdapter.getDimension(Alloy.Globals.NAV_HEIGHT) - (StringAdapter.getDimension(Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.nav_logo_margin_tb)) * 2);

	Alloy.Globals.NAV_LOGO_HEIGHT = navLogoHeight + "dp";
	Alloy.Globals.NAV_LOGO_WIDTH = ((navLogoHeight * 307) / 79) + "dp";

	Alloy.Globals.TIME_DURATION = 300;
	Alloy.Globals.TITLE_CONTAINER_HIGTH = StringAdapter.getDimension(Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.title_container_height));
	Alloy.Globals.CONTAINER_HIGTH = StringAdapter.getDimension(Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.container_height));
	Alloy.Globals.TAB_MENU_IMAGE_SIZE = StringAdapter.getDimension(Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.tab_menu_image_size));
	Alloy.Globals.LISTVIEW_DOUBLETAB_TOP = Alloy.Globals.TITLE_CONTAINER_HIGTH + Alloy.Globals.TITLE_CONTAINER_HIGTH;

	Alloy.Globals.DEVICE_FONT_NAV_LABEL = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.DEVICE_FONT_NAV_LABEL);
	Alloy.Globals.DEVICE_FONT_25SP = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.DEVICE_FONT_25SP);
	Alloy.Globals.DEVICE_FONT_24SP = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.DEVICE_FONT_24SP);
	Alloy.Globals.DEVICE_FONT_23SP = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.DEVICE_FONT_23SP);
	Alloy.Globals.DEVICE_FONT_22SP = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.DEVICE_FONT_22SP);
	Alloy.Globals.DEVICE_FONT_21SP = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.DEVICE_FONT_21SP);
	Alloy.Globals.DEVICE_FONT_20SP = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.DEVICE_FONT_20SP);
	Alloy.Globals.DEVICE_FONT_19SP = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.DEVICE_FONT_19SP);
	Alloy.Globals.DEVICE_FONT_18SP = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.DEVICE_FONT_18SP);
	Alloy.Globals.DEVICE_FONT_16SP = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.DEVICE_FONT_16SP);
	Alloy.Globals.DEVICE_FONT_15SP = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.DEVICE_FONT_15SP);

	Alloy.Globals.FONT_SIZE_TITLE_LISTVIEW = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.FONT_SIZE_TITLE_LISTVIEW);
	Alloy.Globals.FONT_SIZE_NAME_LISTVIEW = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.FONT_SIZE_NAME_LISTVIEW);

	/**************/
	switch (Ti.Platform.displayCaps.dpi) {
	case 160:
		Alloy.Globals.STATUS_BAR_HEIGHT = 25;
		break;
	case 120:
		Alloy.Globals.STATUS_BAR_HEIGHT = 19;
		break;
	case 240:
		Alloy.Globals.STATUS_BAR_HEIGHT = 38;
		break;
	case 320:
		Alloy.Globals.STATUS_BAR_HEIGHT = 50;
		break;
	case 480:
		Alloy.Globals.STATUS_BAR_HEIGHT = 75;
		break;
	case 640:
		Alloy.Globals.STATUS_BAR_HEIGHT = 100;
		break;
	default:
		Alloy.Globals.STATUS_BAR_HEIGHT = 50;
		break;
	}

	/******  ******/
	var paddingLeft = 8;
	/******  ******/
	Alloy.Globals.MAX_TITLE_LENGTH = StringAdapter.getDimension(Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.MAX_TITLE_LENGTH));
	Alloy.Globals.itemlistview_margin_lr = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.itemlistview_margin_lr);
	Alloy.Globals.itemlistview_margin_lr_div2 = (StringAdapter.getDimension(Alloy.Globals.itemlistview_margin_lr)) / 2;
	Alloy.Globals.tab_menu_text_padding_left = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.tab_menu_text_padding_left);

	Alloy.Globals.VideoViewHeight = (Alloy.Globals.SCREEN_WIDTH * 3) / 5;

	Alloy.Globals.itemlistview_margin_tb = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.itemlistview_margin_tb);
	Alloy.Globals.social_socialView_height = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.social_socialView_height);
	Alloy.Globals.itemlistview_listViewTitleIcon_wh = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.itemlistview_listViewTitleIcon_wh);
	Alloy.Globals.itemlistview_listViewImageView_height = ((Alloy.Globals.SCREEN_WIDTH - (StringAdapter.getDimension(Alloy.Globals.itemlistview_margin_lr) * 4)) * 143) / 285;
	Alloy.Globals.itemlistview_title_height = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.itemlistview_title_height);
	Alloy.Globals.itemlistview_title_text_time_left = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.itemlistview_title_text_time_left);
	Alloy.Globals.itemlistview_title_text_view_left = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.itemlistview_title_text_view_left);
	Alloy.Globals.itemlistview_eventListviewContainer_clock_height_width = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.itemlistview_eventListviewContainer_clock_height_width);

	Alloy.Globals.itemlistview_eventTimeContainer_height = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.itemlistview_eventTimeContainer_height);
	Alloy.Globals.itemlistview_eventListviewContainer_title_height = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.itemlistview_eventListviewContainer_title_height);

	/*******other*****/
	Alloy.Globals.itemlistview_listViewContentContainer_height = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.itemlistview_listViewContentContainer_height);

	/*******tabgame*****/
	Alloy.Globals.tabgame_title_padding_lr = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.tabgame_title_padding_lr);

	/*******social*****/
	Alloy.Globals.video_socialview_height = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.video_socialview_height);
	Alloy.Globals.video_socialview_viewer_image_padding_right = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.video_socialview_viewer_image_padding_right);

	/****** video ******/
	Alloy.Globals.video_imageview_height = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.video_imageview_height);
	Alloy.Globals.video_imageview_width = (StringAdapter.getDimension(Alloy.Globals.video_imageview_height) * 120) / 73;
	Alloy.Globals.video_listview_label_container_left = StringAdapter.getDimension(Alloy.Globals.video_imageview_width) + paddingLeft;

}

Alloy.Globals.COUNTRY_THAILAND = 2;
Alloy.Globals.COUNTRY_SINGAPORE = 3;
Alloy.Globals.COUNTRY_VIETNAM = 5;
Alloy.Globals.COUNTRY_INDONESIA = 9;
Alloy.Globals.COUNTRY_PHILIPPINES = 13;
Alloy.Globals.COUNTRY_MALAYSIA = 15;
Alloy.Globals.CountryArray = null;

Alloy.Globals.TAB_Z_INDEX = 10;
Alloy.Globals.TAB_VIEW_Z_INDEX = 1000;
Alloy.Globals.PROGRESS_BAR_Z_INDEX = 9999;
Alloy.Globals.MODAL_Z_INDEX = 10000;

Alloy.Globals.IOS_KEYBOARD_HEIGHT = 216;

Alloy.Globals.HIGHLIGHT_ACTIVITY = 1;
Alloy.Globals.NEWS_ACTIVITY = 2;
Alloy.Globals.PROMOTION_ACTIVITY = 3;
Alloy.Globals.EVENT_ACTIVITY = 4;
Alloy.Globals.VIDEO_ACTIVITY = 5;
Alloy.Globals.STREAMING_ACTIVITY = 6;
Alloy.Globals.GAME_ACTIVITY = 7;
Alloy.Globals.CONTACT_ACTIVITY = 8;
Alloy.Globals.SETTING_ACTIVITY = 9;

Alloy.Globals.SIDEMENU_ACTIVITY = 10;

//---- Globals variable ----

//Custom Fonts
Alloy.Globals.customFontThaiSansNeue = "ThaiSans Neue";
Alloy.Globals.customFontVietnam = "Thonburi";

Alloy.Globals.countryCode = Ti.App.Properties.getString('country');
if (Alloy.Globals.countryCode == Alloy.Globals.COUNTRY_THAILAND) {
	Alloy.Globals.LANGUAGE = Alloy.Globals.LANGUAGE_TH;
} else {
	Alloy.Globals.LANGUAGE = Alloy.Globals.LANGUAGE_EN;
}

//---- Global view  ----
Alloy.Globals.isFirstTimeLoad = true;
Alloy.Globals.FONT_DEFAULT = null;
var Country = require("/utils/Country");
Country.setVietnameFontSize();

//default
Alloy.Globals.ds = $.ds;

Alloy.Globals.currentViewRight = null;
Alloy.Globals.currentController = null;

Alloy.Globals.navLabel = Ti.UI.createLabel({
	width : Ti.UI.SIZE,
	height : Ti.UI.SIZE,
	textAlign : "center",
	color : "#555555",
	font : {
		fontSize : Alloy.Globals.DEVICE_FONT_NAV_LABEL,
		fontFamily : Alloy.Globals.customFontThaiSansNeue,
		fontWeight : "bold"

	}
});

Alloy.Globals.navLogo = Ti.UI.createImageView({
	width : 153,
	height : 39,
	image : "/images/main_nav_logo.png",
});

Alloy.Globals.ID_CALENDAR = Ti.App.Properties.getString('idcalendar');

Alloy.Globals.ALERT_NOT_FOUND = "Not found data";
Alloy.Globals.ALERT_NETWORK_NONE = "Cannot Connect to the Internet";
Alloy.Globals.deviceToken = null;

function init() {

	/////test////////////
	var PushNotification = require("utils/Pushnotification");
	PushNotification.checkPushnotification();
	
	
	/////////test///////

	Titanium.App.addEventListener('resume', function(e) {
		//Ti.API.info('resume');
		Alloy.Globals.createSideMenu();
	});

	if (Alloy.Globals.OS_IOS) {
		Titanium.UI.iPhone.setAppBadge(null);
	};

	///////////Notification//////////
	var lastTimeArray = Ti.App.Properties.getList('lastTimeArray');
	////////////////////Ti.API.info('lastTimeArray : ' + lastTimeArray);
	if (!lastTimeArray) {
		var dateTimeNow = new Date();
		var DateTime = require("/utils/DateTime");
		dateTimeNow = DateTime.getDateTimeForNotification(dateTimeNow);
		lastTimeArray = [dateTimeNow, dateTimeNow, dateTimeNow, dateTimeNow, dateTimeNow];
		Ti.App.Properties.setList('lastTimeArray', lastTimeArray);
	};

	///////////////////////////////
	Alloy.Globals.ds.init();

	if (Alloy.Globals.OS_IOS) {
		var Calendar = require("/utils/Calendar");
		Calendar.initCalendar();
	}

	if (!Alloy.Globals.countryCode) {
		var countryView = Alloy.createController('view/CountryView');
		if (Alloy.Globals.OS_IOS) {
			countryView.getView().open({
				transition : Titanium.UI.iPhone.AnimationStyle.FLIP_FROM_LEFT
			});
		} else {
			countryView.getView().open();
		}

	} else {
		initHighlightActivity(true);
	}
};

var initHighlightActivity = function(isFirstActivity) {

	//start
	Alloy.Globals.currentController = Alloy.createController("HighLightActivity");
	Alloy.Globals.currentViewRight = Alloy.Globals.currentController.getView();
	Alloy.Globals.ds.navTitle.add(Alloy.Globals.navLogo);

	var countryImage = "/images/setting/setting_country_" + Alloy.Globals.countryCode + ".png";
	Alloy.Globals.SideMenuButton = Alloy.Globals.ds.SIDEMENU_COUNTRY;
	Alloy.Globals.ds.rightButton.setBackgroundImage(countryImage);

	//menu
	var currentViewLeft = Alloy.createController("view/SideMenu").getView();
	Alloy.Globals.ds.leftView.add(currentViewLeft);
	Alloy.Globals.ds.contentview.add(Alloy.Globals.currentViewRight);

	$.win.addEventListener('focus', self.onFocus);
	$.win.addEventListener('blur', self.onBlur);
	$.win.addEventListener('close', function(e) {
		////////////////////Ti.API.info("----index close-----");
		$.win.removeEventListener('focus', self.onFocus);
		$.win.removeEventListener('blur', self.onBlur);
	});

	if (Alloy.Globals.OS_ANDROID) {
		$.win.exitOnClose = true;
	}

	if (Alloy.Globals.OS_IOS && isFirstActivity) {
		$.win.open({
			transition : Titanium.UI.iPhone.AnimationStyle.FLIP_FROM_LEFT
		});

	} else {
		$.win.open();
	}
};

Alloy.Globals.initHighlightActivity = initHighlightActivity;

function openNewPage(_page) {
	var newCurrentView = Alloy.createController(_page).getView();
	if (newCurrentView != Alloy.Globals.currentViewRight) {
		Alloy.Globals.ds.contentview.remove(Alloy.Globals.currentViewRight);
		Alloy.Globals.currentViewRight = newCurrentView;
		Alloy.Globals.ds.contentview.add(Alloy.Globals.currentViewRight);
	}
}

//life cycle
var self = this;

this.onFocus = function(e) {

	////////////////////Ti.API.info('Alloy.Globals.currentController.name : ' + Alloy.Globals.currentController.name);
	////////////////////Ti.API.info('index.onFocus ');
	if (Alloy.Globals.currentController.name == Alloy.Globals.HIGHLIGHT_ACTIVITY && !Alloy.Globals.isFirstTimeLoad) {

		var Country = require("utils/Country");
		Country.setVietnameFontSize();

		Alloy.Globals.addNavTitle(0);
		openNewPage("HighLightActivity");

	} else if (Alloy.Globals.currentController.name == Alloy.Globals.SETTING_ACTIVITY) {

		Alloy.Globals.currentController.contryText.setText(Ti.Locale.getString("setting_country_select_" + Alloy.Globals.LANGUAGE));
		Alloy.Globals.currentController.notiText.setText(Ti.Locale.getString("setting_notification_" + Alloy.Globals.LANGUAGE));
		//Alloy.Globals.currentController.reminderText.setText(Ti.Locale.getString("setting_event_reminder_" + Alloy.Globals.LANGUAGE));

		if (Alloy.Globals.currentController.countryLabel) {
			var Country = require("utils/Country");
			var countryText = Country.getCountry();
			Country.setVietnameFontSize();
			Alloy.Globals.currentController.countryLabel.setText(countryText);
		}////

		if (Alloy.Globals.LANGUAGE == Alloy.Globals.LANGUAGE_TH) {
			Alloy.Globals.addNavTitle(7);
		} else {
			Alloy.Globals.addNavTitle(6);
		}
	} else if (Alloy.Globals.currentController.name == Alloy.Globals.SIDEMENU_ACTIVITY) {
		////////////////////Ti.API.info('Alloy.Globals.SIDEMENU_ACTIVITY');
	}
	Alloy.Globals.isFirstTimeLoad = false;
};
this.onBlur = function(e) {
	/////////////////////////Ti.API.info('index.onBlur ');
};

$.win.addEventListener('android:back', function() {

	var dialog = Ti.UI.createAlertDialog({
		title : 'Playpark Application',
		message : 'Do you want to close application?',
		cancel : 1,
		buttonNames : ['OK', 'Cancel'],

	});
	dialog.addEventListener('click', function(e) {
		if (e.index != e.source.cancel) {

			var intent = Ti.Android.createIntent({
				action : Ti.Android.ACTION_MAIN
			});
			intent.addCategory(Ti.Android.CATEGORY_HOME);
			Ti.Android.currentActivity.startActivity(intent);
		}
	});
	dialog.show();
});

init();
