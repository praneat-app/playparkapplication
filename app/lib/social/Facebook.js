function Facebook() {

	var self = this;

	var _button = null;
	var callback = function() {
	};

	//init
	var _facebook = require("facebook");
	_facebook.appid = Alloy.CFG.FACEBOOK_APP_ID;
	_facebook.forceDialogAuth = false;

	_facebook.permissions = [];
	/*if (OS_IOS) {
	 _facebook.permissions = [];
	 } else {
	 _facebook.permissions = ["publish_actions"];
	 }*/

	_facebook.addEventListener("login", function(e) {

		if (e.success) {

			//////////////////////////Ti.API.info"Facebook login success");
			if (callback != null && callback !== undefined) {

				callback();
				callback = function() {
				};
			} else {
				enabledButton(_button);
			}
		} else if (e.error) {

			//////////////////////////Ti.API.info"Facebook login error: " + e.error);
			enabledButton(_button);
		} else if (e.cancelled) {

			//////////////////////////Ti.API.info"Facebook login cancelled: " + e.cancelled);
			if (_facebook !== null) {
				_facebook.logout();
			}
			enabledButton(_button);
		}
	});

	_facebook.addEventListener("logout", function(e) {

		//////////////////////////Ti.API.info"Facebook logout success");
		var client = Titanium.Network.createHTTPClient();
		client.clearCookies('https://login.facebook.com');
		if (_facebook != null) {
			_facebook.accessToken = null;
			_facebook.uid = null;
		}
	});

	//public method
	this.getIsLogin = function() {

		return _facebook.getLoggedIn();
	};

	this.login = function(callbackArg, button) {

		//////////////////////////Ti.API.info"Facebook ==> login, getLoggedIn: " + _facebook.getLoggedIn());
		//////////////////////////Ti.API.info"Facebook ==> login, callbackArg: " + callbackArg + ", button : " + button);

		if (!_facebook.getLoggedIn()) {
			_button = button;
			callback = callbackArg;
			_facebook.authorize();
		} else {
			enabledButton(button);
		}
	};

	this.logout = function() {
		//////////////////////////Ti.API.info'Facebook ==> logout: ' + _facebook);
		if (_facebook !== null) {
			_facebook.logout();
		}
	};

	this.post = function(link, name, message, desc, button) {

		//////////////////////////Ti.API.info'Facebook ==> post: ' + message);
	    //////////////////////////Ti.API.info'Facebook ==> _facebook: ' + _facebook);

		if (_facebook != null) {

			var postWithFeedDialog = function() {

				//////////////////////////Ti.API.info'Facebook ==> post.postWithFeedDialog');

				var data = {
					link : link,
					name : name,
					message : message,
					description : desc,
					// picture : Alloy.CFG.FACEBOOK_LOGO,
					//caption : desc
				};
				_facebook.dialog("feed", data, function(e) {

					if (e.success && e.result) {
						var currentWindow = Ti.UI.currentWindow;
						//////////////////////////Ti.API.info"Facebook ==> post.postWithFeedDialog success: " + e.result);

						var dialog = Ti.UI.createAlertDialog({
							buttonNames : ['OK'],
							title : Ti.Locale.getString("facebook_share_success_title"),
							message : Ti.Locale.getString("facebook_share_success_detail"),
						});
						dialog.show();

						if (button != null) {
							button.setEnabled(true);
						}
					} else if (e.cancelled) {

						//////////////////////////Ti.API.info"Facebook ==> post.postWithFeedDialog cancelled");

						if (button != null) {
							button.setEnabled(true);
						}
					} else {

						//////////////////////////Ti.API.info"Facebook ==> post.postWithFeedDialog failed: " + e.error);
						//////////////////////////Ti.API.info'_facebook.getLoggedIn() : ' + _facebook.getLoggedIn());
						if (_facebook.getLoggedIn()) {

							var dialog = Ti.UI.createAlertDialog({
								buttonNames : ['OK'],
								title : Ti.Locale.getString("facebook_share_failed_title"),
								message : Ti.Locale.getString("facebook_share_failed_detail"),
							});
							dialog.show();
						} else {
							self.login(postWithFeedDialog);
						}
					}
				});
			};

			var postWithGraph = function() {

				//////////////////////////Ti.API.info'Facebook ==> post.postWithGraph');
				_facebook.requestWithGraphPath("me/feed", {
					link : link,
					name : name,
					message : message,
					description : desc,
					picture : Alloy.CFG.FACEBOOK_LOGO
				}, "POST", function(e) {

					if (e.success && e.result) {

						var currentWindow = Ti.UI.currentWindow;
						//////////////////////////Ti.API.info"Facebook ==> post.postWithGraph success: " + e.result);

						var dialog = Ti.UI.createAlertDialog({
							buttonNames : ['OK'],
							title : Ti.Locale.getString("facebook_share_success_title"),
							message : Ti.Locale.getString("facebook_share_success_detail"),
						});
						dialog.show();

						if (button != null) {
							button.setEnabled(true);
						}
					} else {

						//////////////////////////Ti.API.info"Facebook ==> post.postWithGraph failed: " + e.error);
						if (_facebook.getLoggedIn()) {

							var dialog = Ti.UI.createAlertDialog({
								buttonNames : ['OK'],
								title : Ti.Locale.getString("facebook_share_failed_title"),
								message : Ti.Locale.getString("facebook_share_failed_detail"),
							});
							dialog.show();

							if (button != null) {
								button.setEnabled(true);
							}
						} else {
							self.login(reauthorizePublishAction, button);
						}
					}
				});
			};

			var checkPublishAction = function() {

				//////////////////////////Ti.API.info"Facebook ==> post.checkPublishAction");
				_facebook.requestWithGraphPath("me/permissions", {}, "GET", function(e) {

					if (e.success && e.result) {

						var isHavePublishAction = false;

						var jsonObject = JSON.parse(e.result);
						////////////////////////////Ti.API.info'jsonObject.data; ' + jsonObject.data);
						if (jsonObject.data) {
							for (var i = 0,
							    iLength = jsonObject.data.length; i < iLength; i++) {
								var data = jsonObject.data[i];
								//////////////////////////Ti.API.info'permission: ' + data.permission + " ,status: " + data.status);
								if (data.permission == "publish_actions" && data.status == "granted") {
									isHavePublishAction = true;
									break;
								}
							}
						}
						//////////////////////////Ti.API.info"Facebook ==> post.checkPublishAction success: " + e.result + " ,isHavePublishAction: " + isHavePublishAction);

						if (isHavePublishAction) {
							// postWithGraph();
							postWithFeedDialog();
						} else {
							reauthorizePublishAction();
						}

					} else {
						////////////////////////////Ti.API.info"Facebook ==> post.checkPublishAction failed: " + e.error);
						reauthorizePublishAction();
					}
				});
			};

			var reauthorizePublishAction = function() {

				//////////////////////////Ti.API.info'Facebook ==> post.reauthorizePublishAction: accessToken: ' + _facebook.accessToken);
				_facebook.reauthorize(['publish_actions'], 'me', function(e) {

					if (e.success) {
						// postWithGraph();
						postWithFeedDialog();
					} else {

						//////////////////////////Ti.API.info"Facebook ==> post.reauthorizePublishAction failed: " + e.error + " ,isLoggedIn: " + _facebook.getLoggedIn());
						if (_facebook.getLoggedIn()) {

							var dialog = Ti.UI.createAlertDialog({
								buttonNames : ['OK'],
								title : Ti.Locale.getString("facebook_share_failed_title"),
								message : Ti.Locale.getString("facebook_share_failed_detail"),
							});
							dialog.show();

							if (button != null) {
								button.setEnabled(true);
							}
						} else {
							self.login(reauthorizePublishAction, button);
						}
					}
				});
			};

			if (!_facebook.getLoggedIn()) {

				//////////////////////////Ti.API.info'Facebook ==>  not logged in');

				self.login(postWithFeedDialog, button);

				// if (OS_IOS) {
				// self.login(checkPublishAction, button);
				// } else {
				// // self.login(postWithGraph, button);
				// self.login(postWithFeedDialog, button);
				// }

			} else {

				//////////////////////////Ti.API.info'Facebook ==> logged in');

				postWithFeedDialog();

				// if (OS_IOS) {
				// checkPublishAction();
				// } else {
				// // postWithGraph();
				// postWithFeedDialog();
				// }
			}
		}
	};

	//private method
	var enabledButton = function(button) {
		if (button != null) {
			button.setEnabled(true);
			button = null;
		}
	};
}

module.exports = Facebook;
