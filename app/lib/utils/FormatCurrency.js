exports.getFormatCurrency = function(num) {
	////////////////////////////////////Ti.API.info(' getFormatCurrency num :'+num);

	if (num) {

		if (num.length > 3) {
			var dec = 0;
			var sep = ",";
			var decChar = ",";
			var pre = "";
			var post = "";
			num = isNaN(num) || num === '' || num === null ? 0.00 : num;
			var n = num.toString().split(decChar);
			return (pre || '') + n[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1" + sep) + (n.length > 1 ? decChar + n[1].substr(0, dec) : '') + (post || '');
		} else {
			return num;
		}
	} else {
		return 0;

	}
};
