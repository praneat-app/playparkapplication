var osname = Ti.Platform.osname;

var events = [];

if (Alloy.Globals.OS_IOS) {
	var calendarmodule = require('com.praneat.calendarmodule');
} else {
	var calendarmodule = require('com.praneat.calendarandroidmodule');
}

exports.initCalendar = function() {

	if (Alloy.Globals.OS_IOS) {
		if (Alloy.Globals.ID_CALENDAR) {
			var calendar = Ti.Calendar.getCalendarById(Alloy.Globals.ID_CALENDAR);
			if (!calendar) {
				var isCreateSuccess = createCalendarAndSetToProperties();
				return isCreateSuccess;
			} else {
				return true;
			}
		} else {
			var isCreateSuccess = createCalendarAndSetToProperties();
			return isCreateSuccess;
		}
	} else {//android
		Alloy.Globals.ID_CALENDAR = 1;
		////////////////////////Ti.API.info('Alloy.Globals.ID_CALENDAR : ' + Alloy.Globals.ID_CALENDAR);
	}
};

exports.createCalendar = function() {// for iPhone
	return createCalendar();
};

exports.getEventForCalendar = function() {
	var currentYear = new Date().getFullYear();
	//////////////////////Ti.API.info('Alloy.Globals.ID_CALENDAR : ' + Alloy.Globals.ID_CALENDAR);

	if (Alloy.Globals.OS_IOS) {
		if (Alloy.Globals.ID_CALENDAR) {
			var calendar = Ti.Calendar.getCalendarById(Alloy.Globals.ID_CALENDAR);
			if (calendar) {
				events = calendar.getEventsInYear(currentYear);
			}
			return events;

		} else {
			var calendar = createCalendarAndSetToProperties();
			if (calendar) {
				events = calendar.getEventsInYear(currentYear);
			}
			return events;
		}
	} else {//android
		Alloy.Globals.ID_CALENDAR = 1;
		var calendar = Ti.Calendar.getCalendarById(Alloy.Globals.ID_CALENDAR);
		events = calendar.getEventsInYear(currentYear);
		//////////////////////Ti.API.info('android events : ' + events);
		return events;
	}
};

exports.addEvent = function(_eventid, _eventBegins, _eventEnds, _eventTitle, _eventDesciption, _eventObject) {

	//////////////////////Ti.API.info('_eventid : ' + _eventid + ", _eventBegins : " + _eventBegins + ", _eventEnds : " + _eventEnds + " ,_eventTitle : " + _eventTitle + ",_eventDesciption : " + _eventDesciption);
	if (Alloy.Globals.OS_ANDROID) {
		Alloy.Globals.ID_CALENDAR = 1;
		var calendar = Ti.Calendar.getCalendarById(Alloy.Globals.ID_CALENDAR);

		var details = {
			title : _eventTitle,
			description : _eventDesciption,
			begin : _eventBegins,
			end : _eventEnds
		};

		var myEvent = calendar.createEvent(details);
		var reminderDetails = {
			minutes : 1,
			method : Ti.Calendar.METHOD_ALERT
		};
		myEvent.createReminder(reminderDetails);

		//add Properties eventDatabaseArray
		var eventDatabaseArray = Ti.App.Properties.getList('eventDatabaseArray');

		if (!eventDatabaseArray) {
			eventDatabaseArray = [];
		}
		
		//////////////////Ti.API.info('myEvent.id : ' + myEvent.id);

		var eventObject = {
			"eventId" : myEvent.id,
			"eventCalendar" : _eventObject
		};

		eventDatabaseArray.push(eventObject);
		Ti.App.Properties.setList("eventDatabaseArray", eventDatabaseArray);

		////////////////////////////////////////////////////////////////////////////////
		/*
		var eventDatabaseArrayNew = Ti.App.Properties.getList('eventDatabaseArray');
		//////////////////Ti.API.info('addEvent eventDatabaseArrayNew : ' + eventDatabaseArrayNew.length);
		for (var i = 0; i < eventDatabaseArrayNew.length; i++) {
			//////////////////Ti.API.info('addEvent eventId : ' + eventDatabaseArrayNew[i].eventId);
			//////////////////Ti.API.info('addEvent eventCalendarId : ' + eventDatabaseArrayNew[i].eventCalendar);

			var object = eventDatabaseArrayNew[i].eventCalendar;
			//////////////////Ti.API.info('addEvent eventDatabaseArrayNew id : ' + object.id);
			//////////////////Ti.API.info('addEvent eventDatabaseArrayNew title : ' + object.title);
		}
		*/
		////////////////////////////////////////////////////////////////////////////////

	}
	else {

		////////////////////////////////Ti.API.info('addEvent ==> ID_CALENDAR : ' + Alloy.Globals.ID_CALENDAR);
		var
		defCalendar;
		if (!Alloy.Globals.ID_CALENDAR) {
			defCalendar = createCalendarAndSetToProperties();
			////////////////////////////////Ti.API.info('addEvent ==> defCalendar1 : ' + defCalendar);
		}
		else {
			defCalendar = Ti.Calendar.getCalendarById(Alloy.Globals.ID_CALENDAR);
			////////////////////////////////Ti.API.info('addEvent ==> defCalendar2 : ' + defCalendar);

			if (!defCalendar) {
				defCalendar = createCalendarAndSetToProperties();
				////////////////////////////////Ti.API.info('defCalendar : ' + defCalendar);
			}
		}

		if (defCalendar) {
			//var start_date = new Date(year,month,day,hour,minute,second,millisecond);

			var myEvent = defCalendar.createEvent({
				title : _eventTitle,
				notes : _eventDesciption,
				location : 'Playpark',
				begin : _eventBegins,
				end : _eventEnds,
				availability : Ti.Calendar.AVAILABILITY_FREE,
				allDay : false,
			});

			myEvent.save(Ti.Calendar.SPAN_THISEVENT);

			//add Properties eventDatabaseArray
			var eventDatabaseArray = Ti.App.Properties.getList('eventDatabaseArray');

			if (!eventDatabaseArray) {
				eventDatabaseArray = [];
			}

			var eventObject = {
				"eventId" : myEvent.id,
				"eventCalendar" : _eventObject
			};

			eventDatabaseArray.push(eventObject);
			Ti.App.Properties.setList("eventDatabaseArray", eventDatabaseArray);

			/*
			 var eventDatabaseArrayNew = Ti.App.Properties.getList('eventDatabaseArray');
			 //////////////////////////////////Ti.API.info('eventDatabaseArrayNew : ' + eventDatabaseArrayNew.length);
			 for (var i = 0; i < eventDatabaseArrayNew.length; i++) {
			 //////////////////////////////////Ti.API.info('eventId : ' + eventDatabaseArrayNew[i].eventId);
			 //////////////////////////////////Ti.API.info('eventCalendarId : ' + eventDatabaseArrayNew[i].eventCalendar);

			 var object = eventDatabaseArrayNew[i].eventCalendar;
			 //////////////////////////////////Ti.API.info('id : ' + object.id);
			 //////////////////////////////////Ti.API.info('title : ' + object.title);
			 }*/

		}

	}

};

exports.deleteEvent = function(_row) {

	//////////////////Ti.API.info('deleteEvent events : ' + _row);

	if (Alloy.Globals.OS_ANDROID) {
		var eventId = events[_row].id;
		var eventTitle = events[_row].title;

		calendarmodule.deleteEvent(eventId);
		events.splice(_row, 1);

	} else {

		events[_row].remove();
		events.splice(_row, 1);
	}
};

exports.deleteEventForId = function(_rowData) {

	if (Alloy.Globals.OS_IOS) {

		var currentYear = new Date().getFullYear();
		////////////////////Ti.API.info('deleteEventForId ==> Alloy.Globals.ID_CALENDAR : ' + Alloy.Globals.ID_CALENDAR);

		if (Alloy.Globals.ID_CALENDAR) {
			var calendar = Ti.Calendar.getCalendarById(Alloy.Globals.ID_CALENDAR);
			if (calendar) {
				events = calendar.getEventsInYear(currentYear);
			}
			////////////////////Ti.API.info('deleteEventForId ==> calendar : ' + calendar);
		}

		for (var i = 0; i < events.length; i++) {
			////////////////////Ti.API.info('deleteEventForId ==> events[i].id : ' + events[i].id);
			if (events[i].id == _rowData) {
				////////////////////Ti.API.info('deleteEventForId ==> i : ' + i);
				events[i].remove();
				events.splice(i, 1);
			}
		}

	} else {

		if (events) {
			Alloy.Globals.ID_CALENDAR = 1;
			var calendar = Ti.Calendar.getCalendarById(Alloy.Globals.ID_CALENDAR);
			events = calendar.getEventsInYear(currentYear);
		}

		//////////////////Ti.API.info('====> eventId : ' + eventId);
		var eventId = _rowData;
		//////////////////Ti.API.info('====> eventId2 : ' + eventId);

		calendarmodule.deleteEvent(eventId);

		for (var i = 0; i < events.length; i++) {
			//Ti.API.info('====> before events : ' + events[i].id);
		}

		//Ti.API.info('====> events : ' + events);

		if (events) {
			for (var i = 0; i < events.length; i++) {
				//////////////////Ti.API.info('events[i].id  : ' + events[i].id + ", eventId : " + eventId);
				if (events[i].id == eventId) {
					//////////////////Ti.API.info('====> for i events : ' + events[i].id);
					events.splice(i, 1);
				}
			}
		}

	}

};

function createCalendar() {

	var cal = "playplakCalendar";
	var idcalendar = calendarmodule.createCal(cal);
	//Ti.API.info('createCalendar ==> idcalendar : ' + idcalendar);
	return idcalendar;
}

function createCalendarAndSetToProperties() {

	//////////////////////////////////////Ti.API.info('createCalendarAndSetToProperties');
	Alloy.Globals.ID_CALENDAR = createCalendar();
	Ti.App.Properties.setString('idcalendar', Alloy.Globals.ID_CALENDAR);
	var calendar = Ti.Calendar.getCalendarById(Alloy.Globals.ID_CALENDAR);

	return calendar;
}