if (OS_ANDROID) {
	var CloudPush = require('ti.cloudpush');
}

var Cloud = require("ti.cloud");

Alloy.Globals.deviceToken = null;
var MODE_SUBSCRIBETOKEN = 100;
var MODE_UNSUBSCRIBETOKEN = 200;

exports.MODE_SUBSCRIBETOKEN = MODE_SUBSCRIBETOKEN;
exports.MODE_UNSUBSCRIBETOKEN = MODE_UNSUBSCRIBETOKEN;

exports.registerPushNotification = function(_mode, _resubscribetokenbool) {

	//////////////////////////////////Ti.API.info('registerPushNotification');
	getDeviceToken(_mode, _resubscribetokenbool);
	if (OS_ANDROID) {
		CloudPush.addEventListener('callback', function(e) {
			alert("callback : " + JSON.stringify(e));
			// Ti.API.info("callback : " + JSON.stringify(e));
		});
		CloudPush.addEventListener('trayClickLaunchedApp', function(e) {
			alert("trayClickLaunchedApp : " + JSON.stringify(e));
			// Ti.API.info("trayClickLaunchedApp : " + JSON.stringify(e));

		});
		CloudPush.addEventListener('trayClickFocusedApp', function(e) {
			alert("trayClickFocusedApp : " + JSON.stringify(e));
			// Ti.API.info("trayClickFocusedApp : " + JSON.stringify(e));

		});
	}
};

exports.checkPushnotification = function() {

	if (OS_ANDROID) {
		CloudPush.addEventListener('callback', function(e) {
			alert("checkPushnotification callback : " + JSON.stringify(e));
			// Ti.API.info("callback : " + JSON.stringify(e));
		});
		CloudPush.addEventListener('trayClickLaunchedApp', function(e) {
			alert("checkPushnotification trayClickLaunchedApp : " + JSON.stringify(e));
			// Ti.API.info("trayClickLaunchedApp : " + JSON.stringify(e));
		});
		CloudPush.addEventListener('trayClickFocusedApp', function(e) {
			alert("checkPushnotification trayClickFocusedApp : " + JSON.stringify(e));
			// Ti.API.info("trayClickFocusedApp : " + JSON.stringify(e));
		});
	}

};

function getDeviceToken(_mode, _resubscribetokenbool) {

	if (OS_ANDROID) {
		try {
			CloudPush.retrieveDeviceToken({
				success : function deviceTokenSuccess(e) {
					Alloy.Globals.deviceToken = e.deviceToken;
					subscribeToken();

					if (_mode == MODE_SUBSCRIBETOKEN) {
						subscribeToken();
					} else {
						unsubscribeToken();
					}

				},
				error : function deviceTokenError(e) {
				}
			});

		} catch(e) {
			//Ti.API.error('RetrieveDeviceToken.error.exception: ' + e);
		}
	} else {

		if (OS_IOS && Alloy.Globals.OS_VERSION >= 8.0) {
			function registerForPush() {
				Ti.Network.registerForPushNotifications({
					success : function deviceTokenSuccess(e) {
						//	////////////////////////////////Ti.API.info('success');
						Alloy.Globals.deviceToken = e.deviceToken;

						if (_mode == MODE_SUBSCRIBETOKEN) {
							subscribeToken();
						} else {
							unsubscribeToken(_resubscribetokenbool);
						}

					},
					error : function deviceTokenError(e) {
						Ti.API.log(e.error);
						//alert("error : " + e.error);
					},
					callback : function receivePush(e) {
						Ti.API.log(JSON.stringify(e.data));
						//alert(JSON.stringify(e.data.alert));
						var massage = JSON.stringify(e.data.alert);
						var dialog = Ti.UI.createAlertDialog({
							title : 'Notification',
							message : massage,
							ok : 'OK'
						});
						dialog.show();

						Titanium.UI.iPhone.appBadge = 0;
					}
				});
				Ti.App.iOS.removeEventListener('usernotificationsettings', registerForPush);
			};

			Ti.App.iOS.addEventListener('usernotificationsettings', registerForPush);

			Ti.App.iOS.registerUserNotificationSettings({
				types : [Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT, Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND, Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE]
			});

		} else {
			// Ti.API.log("==== iOS7 detected ====");
			Ti.Network.registerForPushNotifications({
				types : [Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT, Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND, Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE],
				success : function deviceTokenSuccess(e) {
					//////////////////////////////////Ti.API.info('success');
					Alloy.Globals.deviceToken = e.deviceToken;
					if (_mode == MODE_SUBSCRIBETOKEN) {
						subscribeToken();
					} else {
						unsubscribeToken(_resubscribetokenbool);
					}

				},
				error : function deviceTokenError(e) {
					Ti.API.log(e.error);
					//alert("error : " + e.error);
				},
				callback : function receivePush(e) {
					//Ti.API.log(JSON.stringify(e.data));
					//alert(JSON.stringify(e.data.alert));

					var massage = JSON.stringify(e.data.alert);
					var dialog = Ti.UI.createAlertDialog({
						title : 'Notification',
						message : massage,
						ok : 'OK'
					});
					dialog.show();

					Titanium.UI.iPhone.appBadge = 0;
				},
			});
		}

	}
}

function subscribeToken() {
	//////////////////////////////////Ti.API.info('device_token : ' + Alloy.Globals.deviceToken);
	var countryChannel = "BELL";
	// if (Alloy.Globals.countryCode == Alloy.Globals.COUNTRY_THAILAND) {
		// countryChannel = "THA";
	// } else if (Alloy.Globals.countryCode == Alloy.Globals.COUNTRY_SINGAPORE) {
		// countryChannel = "SGP";
	// } else if (Alloy.Globals.countryCode == Alloy.Globals.COUNTRY_VIETNAM) {
		// countryChannel = "VNM";
	// } else if (Alloy.Globals.countryCode == Alloy.Globals.COUNTRY_INDONESIA) {
		// countryChannel = "IDN";
	// } else if (Alloy.Globals.countryCode == Alloy.Globals.COUNTRY_PHILIPPINES) {
		// countryChannel = "PHL";
	// } else {
		// countryChannel = "MAL";
	// }
	
	Cloud.PushNotifications.subscribeToken({
		device_token : Alloy.Globals.deviceToken,
		channel : countryChannel,
		type : OS_ANDROID ? 'android' : 'ios'
	}, function(e) {
		if (e.success) {
			alert("e.success : " + e.success + "ตั้งค่าการแจ้งเตือนเรียบร้อย \n e.deviceTokenn : " + Alloy.Globals.deviceToken);
			alert("ตั้งค่าการแจ้งเตือนเรียบร้อย");

		} else {
			Ti.API.error('error:\n' + ((e.error && e.message) || JSON.stringify(e)));
			alert('error:\n' + ((e.error && e.message) || JSON.stringify(e)));
		}
	});
};

function unsubscribeToken(unsubscribeToken) {
	//////////////////////////////////Ti.API.info('device_token : ' + Alloy.Globals.deviceToken);
	Cloud.PushNotifications.unsubscribeToken({//unsubscribeToken
		device_token : Alloy.Globals.deviceToken,
		//channel : Alloy.Globals.countryCode,
		type : OS_ANDROID ? 'android' : 'ios'
	}, function(e) {
		if (e.success) {
			if (unsubscribeToken) {
				subscribeToken();

			}
			//alert("e.success : " + e.success + "ตั้งค่าการแจ้งเตือนเรียบร้อย \n e.deviceTokenn : " + Alloy.Globals.deviceToken);
			//	alert("ยกเลิกการตั้งค่าแจ้งเตือนเรียบร้อย");

		} else {
			Ti.API.error('error:\n' + ((e.error && e.message) || JSON.stringify(e)));
			//alert('unsubscribed.error:\n' + ((e.error && e.message) || JSON.stringify(e)));
		}
	});
};

