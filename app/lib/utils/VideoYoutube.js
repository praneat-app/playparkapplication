exports.h264videosWithYoutubeURL = function(_youtubeId, _callbackOk, _callbackError) {
	var youtubeInfoUrl = 'http://www.youtube.com/get_video_info?video_id=' + _youtubeId;
	var request = Titanium.Network.createHTTPClient({
		timeout : 10000
	});
	request.open("GET", youtubeInfoUrl);
	request.onerror = function(_event) {
		//////////////////////Ti.API.info('_event : ' + _event.error);
		if (_callbackError)
			_callbackError({
				status : this.status,
				error : _event.error
			});
	};

	request.onload = function(_event) {
		var qualities = {};
		var response = this.responseText;
		//var uriD = Titanium.Network.decodeURIComponent(response);
		////////////////////////Ti.API.info('uriD : ' + uriD);

		var args = getURLArgs(response);
		if (!args.hasOwnProperty('url_encoded_fmt_stream_map')) {
			if (_callbackError)
				_callbackError();
		} else {
			var fmtstring = args['url_encoded_fmt_stream_map'];
			//////////////////////Ti.API.info('fmtstring : ' + fmtstring);

			if (args['url_encoded_fmt_stream_map']) {
				var fmtarray = fmtstring.split(',');
				var numVideo = 1;
				for (var i = 0,
				    j = fmtarray.length; i < j; i++) {
					var args2 = getURLArgs(fmtarray[i]);
					var type = decodeURIComponent(args2['type']);
					if (type.indexOf('mp4') >= 0) {
						var url = decodeURIComponent(args2['url']);
						var quality = decodeURIComponent(args2['quality']);

						if (qualities[quality]) {
							numVideo++;
							var key = quality + "_" + numVideo;
							qualities[key] = url;
							//////////////////////Ti.API.info('qualities : ' + qualities + ", key : " + key);
						} else {
							qualities[quality] = url;
							//////////////////////Ti.API.info('qualities : ' + qualities + ", quality : " + quality);
							numVideo = 1;
						}
					}
				}
				//////////////////////Ti.API.info('qualities : ' + qualities);

				if (_callbackOk) {
					//////////////////////Ti.API.info('medium : ' + qualities.medium);

					if (qualities.medium) {
						_callbackOk(qualities.medium, null);

					} else {
						//////////////////////Ti.API.info('small : ' + qualities.small);

						_callbackOk(qualities.small, null);
					}
				}

			} else if (args['hlsvp']) {
				var hlsvp = args['hlsvp'];
				////////////////////////Ti.API.info('hlsvp :' + hlsvp);
				if (_callbackOk) {
					_callbackOk(hlsvp, null);
				}

			} else {
				var iurl = args['iurl'];
				////////////////////////Ti.API.info('iurl : ' + iurl);
				if (_callbackOk) {
					_callbackOk(null, iurl);
				}
			}

		}
	};
	request.send();
};

function getURLArgs(_string) {
	////////////////////////////////////Ti.API.info('_string : ' + _string);
	var args = {};
	var pairs = _string.split("&");
	for (var i = 0; i < pairs.length; i++) {
		var pos = pairs[i].indexOf('=');
		if (pos == -1)
			continue;
		var argname = pairs[i].substring(0, pos);
		var value = pairs[i].substring(pos + 1);
		args[argname] = unescape(value);
	}
	return args;
}

var openYouTubeVideo = function(appFile) {
	var tmpFile = undefined,
	    newPath =
	    undefined;

	try {
		if (Ti.Android.currentActivity == null) {
		}
		var intent = Ti.Android.createIntent({
			action : Ti.Android.ACTION_VIEW,
			data : 'vnd.youtube:' + appFile
		});

		Ti.Android.currentActivity.startActivity(intent);

	} catch (err) {

		var alertDialog = Titanium.UI.createAlertDialog({
			title : 'No Youtube player',
			message : 'We tried to open a Youtube but failed. Do you want to search the marketplace for a Youtube viewer?',
			buttonNames : ['Yes', 'No'],
			cancel : 1
		});
		alertDialog.show();
		alertDialog.addEventListener('click', function(evt) {
			if (evt.index == 0) {
				Ti.Platform.openURL('http://search?q=pdf');
			}
		});
	}
};
