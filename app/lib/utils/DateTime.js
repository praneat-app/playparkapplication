var MODE_DATE = "1";
var MODE_TIME = "2";

exports.MODE_DATE = MODE_DATE;
exports.MODE_TIME = MODE_TIME;

exports.getNormalDate = function(date) {

	var result = date;
	var dateArray = date.split("-");
	////////////////////////////////////////Ti.API.info('dateArray.length : ' + dateArray.length);
	if (dateArray.length == 3) {
		var day = dateArray[2];
		var month = dateArray[1];
		var year = dateArray[0];

		result = day + "." + month + "." + year;
		return result;
	}
	return result;
};

exports.getEventDateTime = function(dateTime, mode) {

	var result = dateTime;
	var dateTimeArray = dateTime.split(" ");

	if (mode == MODE_DATE) {

		var dateArray = dateTimeArray[0].split("-");

		var day = dateArray[2];
		var month = dateArray[1];
		var year = dateArray[0];

		result = day + "." + month + "." + year;
		return result;

	} else {

		var timeArray = dateTimeArray[1].split(":");

		var hour = timeArray[0];
		var minute = timeArray[1];

		result = hour + "." + minute + " น.";
		return result;
	}

	return result;
};

exports.getStreamingDateTime = function(dateTime, mode) {

	var result = dateTime;
	var dateTimeArray = dateTime.split(" ");

	if (mode == MODE_DATE) {

		var dateArray = dateTimeArray[0].split("-");

		var day = dateArray[2];
		var month = dateArray[1];
		var year = dateArray[0];

		result = day + "." + month + "." + year;
		return result;

	} else {

		var timeArray = dateTimeArray[1].split(":");

		var hour = timeArray[0];
		var minute = timeArray[1];

		result = hour + ":" + minute;
		return result;
	}

	return result;
};

exports.setFormatDateEvent = function(_date) {
	var moment = require('alloy/moment');
	var momentDatetime = moment(_date);
	var newDate = momentDatetime.format("DD.M.YYYY | HH.mm");
	return newDate;

};

exports.getDateForAddEvent = function(_dateTime) {
	var result = _dateTime;

	var moment = require('alloy/moment');
	var momentDatetime = moment(_dateTime);
	result = new Date(momentDatetime);
	return result;

};

exports.getDateTimeForNotification = function(_dateTime) {

	var result = _dateTime;

	var moment = require('alloy/moment');
	var momentDatetime = moment(_dateTime);
	//20150112-0214
	var newDate = momentDatetime.format("YYYYMMDD-HHmm");
	return newDate;

};

