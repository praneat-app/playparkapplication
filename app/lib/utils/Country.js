exports.getCountry = function() {

	var countryText = Ti.Locale.getString("setting_country_th_" + Alloy.Globals.LANGUAGE);

	if (Alloy.Globals.countryCode == Alloy.Globals.COUNTRY_THAILAND) {
		countryText = Ti.Locale.getString("setting_country_th_" + Alloy.Globals.LANGUAGE);
	} else if (Alloy.Globals.countryCode == Alloy.Globals.COUNTRY_SINGAPORE) {
		countryText = Ti.Locale.getString("setting_country_sg_" + Alloy.Globals.LANGUAGE);
	} else if (Alloy.Globals.countryCode == Alloy.Globals.COUNTRY_VIETNAM) {
		countryText = Ti.Locale.getString("setting_country_vn_" + Alloy.Globals.LANGUAGE);
	} else if (Alloy.Globals.countryCode == Alloy.Globals.COUNTRY_INDONESIA) {
		countryText = Ti.Locale.getString("setting_country_id_" + Alloy.Globals.LANGUAGE);
	} else if (Alloy.Globals.countryCode == Alloy.Globals.COUNTRY_PHILIPPINES) {
		countryText = Ti.Locale.getString("setting_country_pl_" + Alloy.Globals.LANGUAGE);
	} else {
		countryText = Ti.Locale.getString("setting_country_ms_" + Alloy.Globals.LANGUAGE);
	}
	return countryText;
};

exports.setVietnameFontSize = function() {

	if (Alloy.Globals.countryCode == Alloy.Globals.COUNTRY_VIETNAM) {//Vietnam
		Alloy.Globals.FONT_DEFAULT = Alloy.Globals.customFontVietnam;

		if (Alloy.Globals.OS_IOS) {

			if (Alloy.Globals.OS_IOS_IPHONE) {//IPhone
				Alloy.Globals.FONT_SIZE_24 = "20sp";
				Alloy.Globals.FONT_SIZE_23 = "19sp";
				Alloy.Globals.FONT_SIZE_22 = "18sp";
				Alloy.Globals.FONT_SIZE_21 = "17sp";
				Alloy.Globals.FONT_SIZE_20 = "16sp";
				Alloy.Globals.FONT_SIZE_19 = "15sp";
				Alloy.Globals.FONT_SIZE_18 = "14sp";
				Alloy.Globals.FONT_SIZE_16 = "13sp";

				Alloy.Globals.FONT_SIZE_TITLE_LISTVIEW = "16sp";

			} else {//IPad
				Alloy.Globals.FONT_SIZE_24 = "24sp";
				Alloy.Globals.FONT_SIZE_23 = "23sp";
				Alloy.Globals.FONT_SIZE_22 = "22sp";
				Alloy.Globals.FONT_SIZE_21 = "21sp";
				Alloy.Globals.FONT_SIZE_20 = "20sp";
				Alloy.Globals.FONT_SIZE_19 = "19sp";
				Alloy.Globals.FONT_SIZE_18 = "18sp";
				Alloy.Globals.FONT_SIZE_16 = "16sp";

				Alloy.Globals.FONT_SIZE_TITLE_LISTVIEW = "25sp";
			}
		} else {//Android

			Alloy.Globals.FONT_SIZE_24 = Alloy.Globals.DEVICE_FONT_24SP;
			Alloy.Globals.FONT_SIZE_23 = Alloy.Globals.DEVICE_FONT_23SP;
			Alloy.Globals.FONT_SIZE_22 = Alloy.Globals.DEVICE_FONT_22SP;
			Alloy.Globals.FONT_SIZE_21 = Alloy.Globals.DEVICE_FONT_21SP;
			Alloy.Globals.FONT_SIZE_20 = Alloy.Globals.DEVICE_FONT_20SP;
			Alloy.Globals.FONT_SIZE_19 = Alloy.Globals.DEVICE_FONT_19SP;
			Alloy.Globals.FONT_SIZE_18 = Alloy.Globals.DEVICE_FONT_18SP;
			Alloy.Globals.FONT_SIZE_16 = Alloy.Globals.DEVICE_FONT_16SP;

			Alloy.Globals.FONT_SIZE_TITLE_LISTVIEW = Alloy.Globals.FONT_SIZE_TITLE_LISTVIEW;

		}

	} else {

		Alloy.Globals.FONT_DEFAULT = Alloy.Globals.customFontThaiSansNeue;
		if (Alloy.Globals.OS_IOS) {

			if (Alloy.Globals.OS_IOS_IPHONE) {//IPhone

				Alloy.Globals.FONT_SIZE_24 = "24sp";
				Alloy.Globals.FONT_SIZE_23 = "23sp";
				Alloy.Globals.FONT_SIZE_22 = "22sp";
				Alloy.Globals.FONT_SIZE_21 = "21sp";
				Alloy.Globals.FONT_SIZE_20 = "20sp";
				Alloy.Globals.FONT_SIZE_19 = "19sp";
				Alloy.Globals.FONT_SIZE_18 = "18sp";
				Alloy.Globals.FONT_SIZE_16 = "16sp";

				Alloy.Globals.FONT_SIZE_TITLE_LISTVIEW = "20sp";

			} else {//IPad
				Alloy.Globals.FONT_SIZE_24 = "29sp";
				Alloy.Globals.FONT_SIZE_23 = "28sp";
				Alloy.Globals.FONT_SIZE_22 = "27sp";
				Alloy.Globals.FONT_SIZE_21 = "26sp";
				Alloy.Globals.FONT_SIZE_20 = "25sp";
				Alloy.Globals.FONT_SIZE_19 = "24sp";
				Alloy.Globals.FONT_SIZE_18 = "23sp";
				Alloy.Globals.FONT_SIZE_16 = "22sp";

				Alloy.Globals.FONT_SIZE_TITLE_LISTVIEW = "32sp";

			}
		} else {

			Alloy.Globals.FONT_SIZE_24 = Alloy.Globals.DEVICE_FONT_24SP;
			Alloy.Globals.FONT_SIZE_23 = Alloy.Globals.DEVICE_FONT_23SP;
			Alloy.Globals.FONT_SIZE_22 = Alloy.Globals.DEVICE_FONT_22SP;
			Alloy.Globals.FONT_SIZE_21 = Alloy.Globals.DEVICE_FONT_21SP;
			Alloy.Globals.FONT_SIZE_20 = Alloy.Globals.DEVICE_FONT_20SP;
			Alloy.Globals.FONT_SIZE_19 = Alloy.Globals.DEVICE_FONT_19SP;
			Alloy.Globals.FONT_SIZE_18 = Alloy.Globals.DEVICE_FONT_18SP;
			Alloy.Globals.FONT_SIZE_16 = Alloy.Globals.DEVICE_FONT_16SP;

			Alloy.Globals.FONT_SIZE_TITLE_LISTVIEW = Alloy.Globals.FONT_SIZE_TITLE_LISTVIEW;

		}

	}

};
