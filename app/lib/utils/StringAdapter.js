exports.getCurrency = function(str) {

	var result = "";
	if(str != null && str !== undefined){
		
		result = str;
		var currency = String.formatCurrency(parseInt(str));
	
		if (currency !== undefined && currency != null) {
			result = currency.substring(1).split(".")[0];
		}
	}
	
	return result;
};

exports.getDimension = function(number) {

	if(!isNaN(parseFloat(number)) && isFinite(number)) {
		return number;
	}
	
	var result = 0;
	if(number != null && number !== undefined){
		var tempString = number.toString();
		
		if (tempString.split("dp").length > 0) {
			
			result = parseFloat(tempString.split("dp")[0]);
		} else if (tempString.split("dip").length > 0) {
			
			result = parseFloat(tempString.split("dip")[0]);
		}
	}

	return result;
};

