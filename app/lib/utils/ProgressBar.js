var progressArray = [];

for ( i = 0; i < 23; i++) {
	var j = i + 1;
	progressArray[i] = '/images/progress/progress_bg' + j + '.png';
}

//////////////////////////////////////Ti.API.info('progressArray ' + progressArray);

function ProgressBar(args) {
	//////////////////////////////////////Ti.API.info('args.parentView : ' + args.parentView);

	var style;

	var progressBarView = Titanium.UI.createImageView({
		url : progressArray[0],
		images : progressArray,
		//width:'150',
		width : 80,
		height : 80,
		touchEnabled : true,
		duration : 24,
		repeatCount : 0
	});

	var progressBgView = Titanium.UI.createView({
		width : Ti.UI.FILL,
		height : Ti.UI.FILL,
		backgroundColor : "transparent"

	});
	progressBgView.add(progressBarView);
	//progressBarContainerView.add(_progressBar);
	this.show = function() {
		//_progressBar.show();
		progressBarView.start();
		args.parentView.add(progressBgView);
		//////////////////////////////////////Ti.API.info('show progressBar');
	};

	this.hide = function() {
		//_progressBar.hide();
		progressBarView.stop();
		args.parentView.remove(progressBgView);
		//////////////////////////////////////Ti.API.info('hide progressBar');

	};
}

module.exports = ProgressBar;
