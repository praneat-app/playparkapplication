exports.getIdEventFromDatabase = function(_id) {
	var eventDatabaseArray = Ti.App.Properties.getList('eventDatabaseArray');
	if (eventDatabaseArray != undefined) {
		for (var j = 0; j < eventDatabaseArray.length; j++) {
			var eventDatabaseCalendar = eventDatabaseArray[j].eventCalendar;
			if (_id == eventDatabaseCalendar.id) {
				var dataIdEvent = eventDatabaseArray[j].eventId;
				var eventData = {
					_dataIdEvent : dataIdEvent,
					_row : j
				};
				return eventData;
			}
		}
	}
};
exports.deleteEventFromIdEvent = function(_idEvent, _rowEvent) {
	
	var eventDatabaseArray = Ti.App.Properties.getList('eventDatabaseArray');
	var Calendar = require("/utils/Calendar");
	if (Alloy.Globals.OS_IOS) {
		Calendar.deleteEventForId(_idEvent);
		eventDatabaseArray.splice(_rowEvent, 1);
		Ti.App.Properties.setList("eventDatabaseArray", eventDatabaseArray);
	} else {
		Calendar.deleteEventForId(_idEvent);
		eventDatabaseArray.splice(_rowEvent, 1);
		Ti.App.Properties.setList("eventDatabaseArray", eventDatabaseArray);
	}

};
exports.addEvent = function(_Object) {
	var eventDatabaseArray = Ti.App.Properties.getList('eventDatabaseArray');

	if (_Object) {
		var id = _Object.id;
		var dateStart = _Object.date_start;
		var dateEnd = _Object.date_end;
		var title = _Object.gametype;
		var des = _Object.title;
		var object = _Object;

		var DateTime = require("/utils/DateTime");
		dateStart = DateTime.getDateForAddEvent(dateStart);
		dateEnd = DateTime.getDateForAddEvent(dateEnd);

		var Calendar = require("/utils/Calendar");
		Calendar.addEvent(id, dateStart, dateEnd, title, des, object);
	}
};

