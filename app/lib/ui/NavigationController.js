function NavigationController(navWindow) {

	////---- private variable ----
	var ANIMATE_TIME = 200;

	var self = this;

	var _windowStack = new Array();
	var _navWindow = null;

	if (navWindow !== undefined && navWindow != null) {
		_navWindow = navWindow;
	}

	////---- public method ----
	this.getNavWindow = function() {

		return _navWindow;
	};

	this.open = function(win, settings, callback) {

		_windowStack.push(win);

		if (settings === undefined || settings == null) {
			settings = {};
		}

		if (Alloy.Globals.OS_IOS) {

			//////////////////////////////////////Ti.API.info("NavigationController ==> iOS open: " + win + " ,setting: " + settings + " ,length: " + _windowStack.length);

			if (Alloy.Globals.OS_VERSION >= 7.0) {
				win.setTop(Alloy.Globals.STATUS_BAR_HEIGHT);
			}

			(_navWindow) ? _navWindow.openWindow(win, settings) : win.open(settings);

		} else {//Android

			//////////////////////////////////////Ti.API.info("NavigationController ==> Android open: " + win);
			win.open();
		}

		if (callback !== undefined && callback != null) {
			callback(true);
		}
	};

	this.close = function(settings) {

		if (settings === undefined || settings == null) {

			if (Alloy.Globals.OS_IOS) {
				settings = {
					animated : false
				};
			} else {
				settings = {};
			}
		}

		//////////////////////////////////////Ti.API.info("NavigationController ==> close()  length: " + _windowStack.length);
		if (_windowStack.length > 1) {

			if (Alloy.Globals.OS_IOS) {
				var win = _windowStack[_windowStack.length - 1];
				(_navWindow) ? _navWindow.closeWindow(win, settings) : win.close(settings);
				_windowStack.pop();
				//////////////////////////////////////Ti.API.info("NavigationController ==> close() window: " + win + " ,length: " + _windowStack.length);
			} else {
				var win = _windowStack[_windowStack.length - 1];
				win.close();
				_windowStack.pop();
				//////////////////////////////////////Ti.API.info("NavigationController ==> close() window: " + win + " ,length: " + _windowStack.length);
			}
		}
	};

	
	this.home = function() {

		//////////////////////////////////////Ti.API.info("NavigationController ==> home() ,total windows: " + _windowStack.length);

		var highLightActivity = Alloy.createController("HighLightActivity", {
			hideBackButton : true
		}).getView();

		///open new homeActivty
		if (Alloy.Globals.OS_IOS) {

			if (Alloy.Globals.OS_VERSION >= 7.0) {
				highLightActivity.setTop(Alloy.Globals.STATUS_BAR_HEIGHT);
			}

			(_navWindow) ? _navWindow.openWindow(highLightActivity, {
				animated : false
			}) : homeActivity.open({
				animated : false
			});

		} else {//Android

			highLightActivity.open();
		}

		///close old activity in stack
		for (var i = 0, length = _windowStack.length.length; i < length; i++) {

			var win = _windowStack[i];
			if (Alloy.Globals.OS_IOS) {
				(_navWindow) ? _navWindow.closeWindow(win, {
					animated : false
				}) : win.close({
					animated : false
				});
			} else {
				win.close();
			}
		};

		_windowStack = new Array();
		//////////////////////////////////////Ti.API.info("NavigationController ==> home() after ,total windows: " + _windowStack.length);

		///push to stack
		Alloy.Globals.isRefreshDataOnResume = true;
		_windowStack.push(highLightActivity);
	};
}

module.exports = NavigationController; 