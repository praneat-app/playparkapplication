var BUTTON_OK = 1;
var BUTTON_CANCEL = 2;
var BUTTON_CALL = 3;
var BUTTON_POST = 4;

function AlertDialog(settings) {

	var ANIMATE_SHOW_TIME = 300;
	var ANIMATE_HIDE_TIME = 200;

	var self = this;

	var _settings = settings;
	var _parentView = _settings.parentView;
	var _alertDialogView = null;
	var _dimmerBg = null;

	////---- public method ----
	this.getView = function(){
		return _alertDialogView;
	};
	
	this.show = function() {
		
		if (_parentView !== undefined && _parentView != null && _alertDialogView != null) {

			_alertDialogView.setOpacity(0);
			_parentView.add(_alertDialogView);
			_alertDialogView.animate({
				opacity : 1,
				curve : Ti.UI.ANIMATION_CURVE_EASE_IN_OUT,
				duration : ANIMATE_SHOW_TIME
			});
		}
	};

	this.hide = function() {
	
		if (_parentView !== undefined && _parentView != null && _alertDialogView != null) {
			_alertDialogView.animate({
				opacity : 0,
				curve : Ti.UI.ANIMATION_CURVE_EASE_OUT,
				duration : ANIMATE_HIDE_TIME
			}, function() {
				_parentView.remove(_alertDialogView);
				_alertDialogView = null;
			});
		}
	};
	
	////---- private method ----
	var initComponent = function() {

		_alertDialogView = Ti.UI.createView({

			width : Ti.UI.FILL,
			height : Ti.UI.FILL,
			zIndex : Alloy.Globals.MODAL_Z_INDEX
		});

		var dimmerBg = Ti.UI.createView({

			backgroundColor : "#000000",
			opacity : 0.80,
			width : Ti.UI.FILL,
			height : Ti.UI.FILL
		});

		var contentView = null;
		var titleLabel = null;
		var messageLabel = null;
		var message2Label = null;
		var buttonView = null;
		

		if (Alloy.Globals.OS_IOS) {

			contentView = Ti.UI.createView({

				backgroundColor : "#ffffff",
				width : Ti.UI.FILL,
				height : Ti.UI.SIZE,
				//top : 140,
				layout : "vertical"
			});

			if (_settings.title !== undefined && _settings.title != null) {

				titleLabel = Ti.UI.createLabel({

					text : _settings.title,
					width : Ti.UI.FILL,
					height : Ti.UI.SIZE,
					textAlign : "center",
					color : "#ee1939",
					font : {
						fontSize : "30sp",
						fontFamily : Alloy.Globals.customFontThaiSansNeue,
						fontWeight : "bold"
					},
					top : 15,
					right : 5,
					left : 5
				});
				contentView.add(titleLabel);
			}
			
			

			if (_settings.message !== undefined && _settings.message != null) {

				messageLabel = Ti.UI.createLabel({
					text : _settings.message,
					width : Ti.UI.FILL,
					height : Ti.UI.SIZE,
					textAlign : "center",
					color : "#222222",
					font : {
						fontSize : "18sp",
						fontFamily : Alloy.Globals.customFontThaiSansNeue,
						fontWeight : "bold"
					},
					top : 8,
					right : 5,
					left : 5
				
				});
				contentView.add(messageLabel);
			}

			if (_settings.message2 !== undefined && _settings.message2 != null) {

				message2Label = Ti.UI.createLabel({

					text : _settings.message2,
					width : Ti.UI.FILL,
					height : Ti.UI.SIZE,
					textAlign : "center",
					color : "#888888",
					font : {
						fontSize : "18sp",
						fontFamily : Alloy.Globals.customFontThaiSansNeue,
						fontWeight : "bold"
					},
					top : 0,
					right : 5,
					left : 5
				});
				contentView.add(message2Label);
			}

			buttonView = Ti.UI.createView({

				width : Ti.UI.SIZE,
				height : Ti.UI.SIZE,
				top : 22,
				bottom : 25,
				layout : "horizontal"
			});
			contentView.add(buttonView);
		}
		else {//Android
			
			contentView = Ti.UI.createView({

				backgroundColor : "#ffffff",
				width : Ti.UI.FILL,
				height : Ti.UI.SIZE,
				layout : "vertical"
			});

			if (_settings.title !== undefined && _settings.title != null) {

				titleLabel = Ti.UI.createLabel({

					text : _settings.title,
					width : Ti.UI.FILL,
					height : Ti.UI.SIZE,
					textAlign : "center",
					color : "#ee1939",
					font : {
						fontSize : Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.text_dialog_title_label_size),
						fontFamily : Alloy.Globals.customFontThaiSansNeue,
						fontWeight : "bold"
					},
					top : Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.dialog_title_label_margin_top),
					right : "5dp",
					left : "5dp"
				});
				contentView.add(titleLabel);
			}
			
			
			if (_settings.message !== undefined && _settings.message != null) {

				messageLabel = Ti.UI.createLabel({
					text : _settings.message,
					width : Ti.UI.FILL,
					height : Ti.UI.SIZE,
					textAlign : "center",
					color : "#222222",
					font : {
						fontSize : Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.text_dialog_message_label_size),
						fontFamily : Alloy.Globals.customFontThaiSansNeue,
						fontWeight : "bold"
					},
					top : Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.dialog_message_label_margin_top),
					right : "5dp",
					left : "5dp"
				
				});
				contentView.add(messageLabel);
			}

			if (_settings.message2 !== undefined && _settings.message2 != null) {

				message2Label = Ti.UI.createLabel({

					text : _settings.message2,
					width : Ti.UI.FILL,
					height : Ti.UI.SIZE,
					textAlign : "center",
					color : "#888888",
					font : {
						fontSize : Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.text_dialog_message_label_size),
						fontFamily : Alloy.Globals.customFontThaiSansNeue,
						fontWeight : "bold"
					},
					top : 0,
					right : "5dp",
					left : "5dp"
				});
				contentView.add(message2Label);
			}
			
			buttonView = Ti.UI.createView({

				width : Ti.UI.SIZE,
				height : Ti.UI.SIZE,
				top : Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.dialog_button_view_margin_top),
				bottom : Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.dialog_button_view_margin_bottom),
				layout : "horizontal"
			});
			contentView.add(buttonView);
		}
		
		if (_settings.buttonNames != null && _settings.buttonNames !== undefined) {
			var length = _settings.buttonNames.length;
			for (var i = 0; i < length; i++) {
				
				var buttonName = _settings.buttonNames[i];
				var button = null;

				switch(buttonName) {

					case BUTTON_OK :
					
						if(Alloy.Globals.OS_IOS) {
							
							button = Ti.UI.createButton({
	
								backgroundImage : "/images/dialog/dialog_ok_button.png",
								backgroundSelectedImage : "/images/dialog/dialog_ok_button_selected.png",
								backgroundDisabledImage : "/images/dialog/dialog_ok_button_selected.png",
								width : 127,
								height : 49,
								_buttonName : BUTTON_OK
							});
						} else {
							
							button = Alloy.createWidget("Button", {
								
								title : Ti.Locale.getString("dialog_ok"),
								image : "/images/button/icon_ok_button.png",
								backgroundImage : "/images/button/red_button.png",
								backgroundSelectedImage : "/images/button/red_button_selected.png",
								backgroundDisabledImage : "/images/button/red_button_selected.png",
								width : Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.dialog_button_width),
								height : Alloy.Globals.button_height,
								color : "#ffffff",
								font : {
									fontSize : Alloy.Globals.text_long_button_size,
									fontFamily : Alloy.Globals.customFontThaiSansNeue,
									fontWeight : "bold"
								},
								imageTop : Alloy.Globals.long_button_image_margin_tb,
								imageRight : 0,
								imageBottom : Alloy.Globals.long_button_image_margin_tb,
								imageLeft : Alloy.Globals.long_button_image_margin_rl,
								_buttonName : BUTTON_OK
							});
						}
						
						break;

					case BUTTON_CANCEL :
					
						if(Alloy.Globals.OS_IOS) {
							button = Ti.UI.createButton({
	
								backgroundImage : "/images/dialog/dialog_cancel_button.png",
								backgroundSelectedImage : "/images/dialog/dialog_cancel_button_selected.png",
								backgroundDisabledImage : "/images/dialog/dialog_cancel_button_selected.png",
								width : 127,
								height : 49,
								_buttonName : BUTTON_CANCEL
							});
						} else {
							button = Alloy.createWidget("Button", {
								
								title : Ti.Locale.getString("dialog_cancel"),
								image : "/images/button/icon_cancel_button.png",
								backgroundImage : "/images/button/grey_button.png",
								backgroundSelectedImage : "/images/button/grey_button_selected.png",
								backgroundDisabledImage : "/images/button/grey_button_selected.png",
								width : Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.dialog_button_width),
								height : Alloy.Globals.button_height,
								color : "#ffffff",
								font : {
									fontSize : Alloy.Globals.text_long_button_size,
									fontFamily : Alloy.Globals.customFontThaiSansNeue,
									fontWeight : "bold"
								},
								imageTop : Alloy.Globals.long_button_image_margin_tb,
								imageRight : 0,
								imageBottom : Alloy.Globals.long_button_image_margin_tb,
								imageLeft : Alloy.Globals.long_button_image_margin_rl,
								_buttonName : BUTTON_CANCEL
							});
						}
						
						break;
					
					case BUTTON_CALL :
					
						if(Alloy.Globals.OS_IOS) {
							button = Ti.UI.createButton({
								
								backgroundImage : "/images/dialog/dialog_call_button.png",
								backgroundSelectedImage : "/images/dialog/dialog_call_button_selected.png",
								backgroundDisabledImage : "/images/dialog/dialog_call_button_selected.png",
								width : 127,
								height : 49,
								_buttonName : BUTTON_CALL
							});
						} else {
							button = Alloy.createWidget("Button", {
								
								title : Ti.Locale.getString("dialog_call"),
								image : "/images/button/icon_call_button.png",
								backgroundImage : "/images/button/green_button.png",
								backgroundSelectedImage : "/images/button/green_button_selected.png",
								backgroundDisabledImage : "/images/button/green_button_selected.png",
								width : Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.dialog_button_width),
								height : Alloy.Globals.button_height,
								color : "#ffffff",
								font : {
									fontSize : Alloy.Globals.text_long_button_size,
									fontFamily : Alloy.Globals.customFontThaiSansNeue,
									fontWeight : "bold"
								},
								imageTop : Alloy.Globals.long_button_image_margin_tb,
								imageRight : 0,
								imageBottom : Alloy.Globals.long_button_image_margin_tb,
								imageLeft : Alloy.Globals.long_button_image_margin_rl,
								_buttonName : BUTTON_CALL
							});
						}
						
						break;
					
					case BUTTON_POST :
					
						if(Alloy.Globals.OS_IOS) {
							button = Ti.UI.createButton({
								
								backgroundImage : "/images/dialog/dialog_edit_profile_button.png",
								backgroundSelectedImage : "/images/dialog/dialog_edit_profile_button_selected.png",
								backgroundDisabledImage : "/images/dialog/dialog_edit_profile_button_selected.png",
								width : 277,
								height : 49,
								_buttonName : BUTTON_POST
							});
						} else {
							
							//////////////////////////////////////Ti.API.info('BUTTON_POST');
							button = Alloy.createWidget("Button", {
								
								title : Ti.Locale.getString("dialog_post"),
								image : "/images/button/icon_post_button.png",
								backgroundImage : "/images/button/yellow_button.png",
								backgroundSelectedImage : "/images/button/yellow_button_selected.png",
								backgroundDisabledImage : "/images/button/yellow_button_selected.png",
								width : Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.dialog_long_button_width),
								height : Alloy.Globals.button_height,
								color : "#ffffff",
								font : {
									fontSize : Alloy.Globals.text_long_button_size,
									fontFamily : Alloy.Globals.customFontThaiSansNeue,
									fontWeight : "bold"
								},
								imageTop : Alloy.Globals.long_button_image_margin_tb,
								imageRight : Alloy.Globals.long_button_image_margin_rl,
								imageBottom : Alloy.Globals.long_button_image_margin_tb,
								imageLeft : Alloy.Globals.long_button_image_margin_rl,
								_buttonName : BUTTON_POST
							});
						}
						
						break;
						
				}

				if (button != null) {
					
					button.addEventListener("click", function(e){
						self.hide();
					});
					
					var numChild = buttonView.getChildren().length;
					if (numChild < 2) {
						
						//////////////////////////////////////Ti.API.info('AlertDialog buttonName: ' + buttonName + " ,button: " + button);
						
						if(buttonName != BUTTON_POST) {
							
							var marginRL = 10;
							if(Alloy.Globals.OS_ANDROID) {
								marginRL = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.dialog_button_margin_rl);
							}
							
							if (numChild == 0) {
								button.setRight(marginRL);
							} else if (numChild == 1) {
								button.setLeft(marginRL);
							}
						}
						
						if(Alloy.Globals.OS_IOS) {
							buttonView.add(button);
						} else {
							buttonView.add(button.getView());							
						}
					}
				}
			};
		}
			
		_alertDialogView.add(dimmerBg);
		_alertDialogView.add(contentView);
	};
	
	initComponent();
};

AlertDialog.BUTTON_OK = BUTTON_OK;
AlertDialog.BUTTON_CANCEL = BUTTON_CANCEL;
AlertDialog.BUTTON_CALL = BUTTON_CALL;
AlertDialog.BUTTON_POST = BUTTON_POST;

module.exports = AlertDialog;