function GoogleAnalytics() {

	if (Alloy.Globals.OS_IOS) {
		var GoogleAnalytics = require('ti.googleanalytics'),
		    tracker = GoogleAnalytics.getTracker(Alloy.CFG.GOOGLE_ANALYTICS_ID),
		    Fields = GoogleAnalytics.getFields(),
		    MapBuilder = GoogleAnalytics.getMapBuilder();

		this.trackScreen = function(_screenName) {
			tracker.send(MapBuilder.createAppView().set(Fields.SCREEN_NAME, _screenName).build());
			//////////////////Ti.API.info('tracker ios : ' + tracker);
		};

	} else {
		var GA = require('analytics.google');
		GA.dryRun = false;
		GA.dispatchInterval = 900;
		//GA.dryRun = true;//debug
		var tracker = GA.getTracker(Alloy.CFG.GOOGLE_ANALYTICS_ID);

		this.trackScreen = function(_screenName) {
			tracker.trackScreen({
				screenName : _screenName
			});
		};
	}

};

module.exports = GoogleAnalytics;
