var SUCCESS = "200";
var INVALID_API_KEY = "100";
var INVALID_AUTH_TOKEN = "101";
var NOT_FOUND = "404";
var TIME_OUT = "1000";
var LIMIT = 80;

exports.SUCCESS = SUCCESS;
exports.INVALID_API_KEY = INVALID_API_KEY;
exports.INVALID_AUTH_TOKEN = INVALID_AUTH_TOKEN;
exports.NOT_FOUND = NOT_FOUND;
exports.TIME_OUT = TIME_OUT;
exports.LIMIT = LIMIT;

exports.getBanner = function(callback) {

	var isTimeOut = false;

	//getBanner HighLight
	var url = Alloy.Globals.HOST_API + "get_banner.php?location=" + Alloy.Globals.countryCode;

	Ti.API.info("==== PlayParkMobileApi.getBanner() ====");
	Ti.API.info("request: " + url);
	Ti.API.info("========");

	var httpClient = Ti.Network.createHTTPClient();
	httpClient.onload = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			var data = eval("(" + this.responseText + ")");
			Ti.API.info("PlayParkMobileApi.getBanner data: " + this.responseText.toString());
			callback(data);
		}
	};

	httpClient.onerror = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			callback({
				code : TIME_OUT
			});
			Ti.API.info("PlayParkMobileApi.getBanner error: " + e.error);
		}
	};

	httpClient.setTimeout(Alloy.Globals.HTTP_TIME_OUT);
	var timeoutInterval = setInterval(function() {

		if (httpClient.readyState == 4) {
			isTimeOut = true;
			clearInterval(timeoutInterval);
			timeoutInterval = null;
			callback({
				code : TIME_OUT
			});
		}
	}, Alloy.Globals.HTTP_TIME_OUT);

	httpClient.open("POST", url);
	httpClient.send({
		format : "json",
		version : Alloy.Globals.build
	});
};
exports.getCountry = function(callback) {

	var isTimeOut = false;

	var url = Alloy.Globals.HOST_API + "get_location.php";

	Ti.API.info("==== News.getCountry() ====");
	Ti.API.info("request: " + url);
	Ti.API.info("========");

	var httpClient = Ti.Network.createHTTPClient();
	httpClient.onload = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			var data = eval("(" + this.responseText + ")");
			callback(data);
		}
	};

	httpClient.onerror = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			callback({
				code : TIME_OUT
			});
			Ti.API.info("News.getLightBox error: " + e.error);
		}
	};

	httpClient.setTimeout(Alloy.Globals.HTTP_TIME_OUT);
	var timeoutInterval = setInterval(function() {

		if (httpClient.readyState == 4) {
			isTimeOut = true;
			clearInterval(timeoutInterval);
			timeoutInterval = null;
			callback({
				code : TIME_OUT
			});
		}
	}, Alloy.Globals.HTTP_TIME_OUT);

	httpClient.open("POST", url);
	httpClient.send({
		format : "json",
		version : Alloy.Globals.build
	});
};

exports.getMenugame = function(callback) {

	var isTimeOut = false;
	var url = Alloy.Globals.HOST_API + "get_menugame.php?location=" + Alloy.Globals.countryCode;

	Ti.API.info("==== PlayParkMobileApi.getMenugame() ====");
	Ti.API.info("request: " + url);
	Ti.API.info("========");

	var httpClient = Ti.Network.createHTTPClient();
	httpClient.onload = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			var data = eval("(" + this.responseText + ")");
			Ti.API.info("PlayParkMobileApi.getMenugame data: " + this.responseText.toString());
			callback(data);
		}
	};

	httpClient.onerror = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			callback({
				code : TIME_OUT
			});
			Ti.API.info("PlayParkMobileApi.getMenugame error: " + e.error);
		}
	};

	httpClient.setTimeout(Alloy.Globals.HTTP_TIME_OUT);
	var timeoutInterval = setInterval(function() {

		if (httpClient.readyState == 4) {
			isTimeOut = true;
			clearInterval(timeoutInterval);
			timeoutInterval = null;
			callback({
				code : TIME_OUT
			});
		}
	}, Alloy.Globals.HTTP_TIME_OUT);

	httpClient.open("POST", url);
	httpClient.send({
		format : "json",
		version : Alloy.Globals.build
	});
};

exports.getMenuCateNews = function(callback) {

	var isTimeOut = false;
	var url = Alloy.Globals.HOST_API + "get_menucatenews.php?location=" + Alloy.Globals.countryCode;

	Ti.API.info("==== PlayParkMobileApi.getMenuCateNews() ====");
	Ti.API.info("request: " + url);
	Ti.API.info("========");

	var httpClient = Ti.Network.createHTTPClient();
	httpClient.onload = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			var data = eval("(" + this.responseText + ")");
			Ti.API.info("PlayParkMobileApi.getMenuCateNews data: " + this.responseText.toString());
			callback(data);
		}
	};

	httpClient.onerror = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			callback({
				code : TIME_OUT
			});
			Ti.API.info("PlayParkMobileApi.getLightBox error: " + e.error);
		}
	};

	httpClient.setTimeout(Alloy.Globals.HTTP_TIME_OUT);
	var timeoutInterval = setInterval(function() {

		if (httpClient.readyState == 4) {
			isTimeOut = true;
			clearInterval(timeoutInterval);
			timeoutInterval = null;
			callback({
				code : TIME_OUT
			});
		}
	}, Alloy.Globals.HTTP_TIME_OUT);

	httpClient.open("POST", url);
	httpClient.send({
		format : "json",
		version : Alloy.Globals.build
	});
};

exports.getNewsList = function(callback, gameId, limit, to, from) {

	if (limit == null) {
		limit = LIMIT;
	};

	var isTimeOut = false;

	var url = null;

	if (!to || !from) {
		url = Alloy.Globals.HOST_API + "get_newslist.php?location=" + Alloy.Globals.countryCode + "&gameid=" + gameId + "&limit=" + limit;
	} else {
		//http://www.playpark.com/th/function/get_newslist.php?location=2&gameid=&from=11&to=11
		url = Alloy.Globals.HOST_API + "get_newslist.php?location=" + Alloy.Globals.countryCode + "&gameid=" + gameId + "&from=" + to + "&to=" + from;
	}

	Ti.API.info("==== PlayParkMobileApi.getNewsList() ====");
	Ti.API.info("request: " + url);
	Ti.API.info("========");

	var httpClient = Ti.Network.createHTTPClient();
	httpClient.onload = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			var data = eval("(" + this.responseText + ")");
			Ti.API.info("PlayParkMobileApi.getNewsList data: " + this.responseText.toString());
			Ti.API.info('data : ' + data);
			callback(data);
		}
	};

	httpClient.onerror = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			callback({
				code : TIME_OUT
			});
			Ti.API.info("PlayParkMobileApi.getNewsList error: " + e.error);
		}
	};

	httpClient.setTimeout(Alloy.Globals.HTTP_TIME_OUT);
	var timeoutInterval = setInterval(function() {

		if (httpClient.readyState == 4) {
			isTimeOut = true;
			clearInterval(timeoutInterval);
			timeoutInterval = null;
			callback({
				code : TIME_OUT
			});
		}
	}, Alloy.Globals.HTTP_TIME_OUT);

	httpClient.open("POST", url);
	httpClient.send({
		format : "json",
		version : Alloy.Globals.build
	});
};

exports.getOtherNewList = function(callback, cateId, from, to) {

	var isTimeOut = false;

	//var url = Alloy.Globals.HOST_API + "get_othernewslist.php?location=" + Alloy.Globals.countryCode + "&cate=" + cateId + "&limit=" + LIMIT;

	var url = null;

	if (!to || !from) {
		url = Alloy.Globals.HOST_API + "get_othernewslist.php?location=" + Alloy.Globals.countryCode + "&cate=" + cateId + "&limit=" + LIMIT;
	} else {
		url = Alloy.Globals.HOST_API + "get_othernewslist.php?location=" + Alloy.Globals.countryCode + "&cate=" + cateId + "&from=" + from + "&to=" + to;
	}

	Ti.API.info("==== PlayParkMobileApi.getOtherNewList() ====");
	Ti.API.info("request: " + url);
	Ti.API.info("========");

	var httpClient = Ti.Network.createHTTPClient();
	httpClient.onload = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			var data = eval("(" + this.responseText + ")");
			callback(data);
		}
	};

	httpClient.onerror = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			callback({
				code : TIME_OUT
			});
			Ti.API.info("PlayParkMobileApi.getOtherNewList error: " + e.error);
		}
	};

	httpClient.setTimeout(Alloy.Globals.HTTP_TIME_OUT);
	var timeoutInterval = setInterval(function() {

		if (httpClient.readyState == 4) {
			isTimeOut = true;
			clearInterval(timeoutInterval);
			timeoutInterval = null;
			callback({
				code : TIME_OUT
			});
		}
	}, Alloy.Globals.HTTP_TIME_OUT);

	httpClient.open("POST", url);
	httpClient.send({
		format : "json",
		version : Alloy.Globals.build
	});
};
exports.getPromotion = function(callback, gameId, limit, to, from) {

	var isTimeOut = false;

	if (limit == null) {
		limit = LIMIT;
	};

	var url = "";
	// = Alloy.Globals.HOST_API + "get_promotion.php?location=" + Alloy.Globals.countryCode + "&gameid=" + gameId + "&limit=" + limit;

	if (!to || !from) {
		url = Alloy.Globals.HOST_API + "get_promotion.php?location=" + Alloy.Globals.countryCode + "&gameid=" + gameId + "&limit=" + limit;
	} else {
		url = Alloy.Globals.HOST_API + "get_promotion.php?location=" + Alloy.Globals.countryCode + "&gameid=" + gameId + "&from=" + to + "&to=" + from;
	}

	Ti.API.info("==== PlayParkMobileApi.getPromotion() ====");
	Ti.API.info("request: " + url);
	Ti.API.info("========");

	var httpClient = Ti.Network.createHTTPClient();
	httpClient.onload = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			var data = eval("(" + this.responseText + ")");
			Ti.API.info("PlayParkMobileApi.getPromotion data: " + this.responseText.toString());
			callback(data);
		}
	};

	httpClient.onerror = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			callback({
				code : TIME_OUT
			});
			Ti.API.info("PlayParkMobileApi.getPromotion error: " + e.error);
		}
	};

	httpClient.setTimeout(Alloy.Globals.HTTP_TIME_OUT);
	var timeoutInterval = setInterval(function() {

		if (httpClient.readyState == 4) {
			isTimeOut = true;
			clearInterval(timeoutInterval);
			timeoutInterval = null;
			callback({
				code : TIME_OUT
			});
		}
	}, Alloy.Globals.HTTP_TIME_OUT);

	httpClient.open("POST", url);
	httpClient.send({
		format : "json",
		version : Alloy.Globals.build
	});
};

exports.getEvent = function(callback, gameId, limit, to, from) {

	var isTimeOut = false;

	if (limit == null) {
		limit = LIMIT;
	};

	var url = "";

	if (!to || !from) {
		url = Alloy.Globals.HOST_API + "get_event.php?location=" + Alloy.Globals.countryCode + "&gameid=" + gameId + "&limit=" + limit;
	} else {
		url = Alloy.Globals.HOST_API + "get_event.php?location=" + Alloy.Globals.countryCode + "&gameid=" + gameId + "&from=" + to + "&to=" + from;
	}

	Ti.API.info("==== PlayParkMobileApi.getEvent() ====");
	Ti.API.info("request: " + url);
	Ti.API.info("========");

	var httpClient = Ti.Network.createHTTPClient();
	httpClient.onload = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			var data = eval("(" + this.responseText + ")");
			Ti.API.info("PlayParkMobileApi.getEvent data: " + this.responseText.toString());
			callback(data);
		}
	};

	httpClient.onerror = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			callback({
				code : TIME_OUT
			});
			Ti.API.info("PlayParkMobileApi.getEvent error: " + e.error);
		}
	};

	httpClient.setTimeout(Alloy.Globals.HTTP_TIME_OUT);
	var timeoutInterval = setInterval(function() {

		if (httpClient.readyState == 4) {
			isTimeOut = true;
			clearInterval(timeoutInterval);
			timeoutInterval = null;
			callback({
				code : TIME_OUT
			});
		}
	}, Alloy.Globals.HTTP_TIME_OUT);

	httpClient.open("POST", url);
	httpClient.send({
		format : "json",
		version : Alloy.Globals.build
	});
};

exports.getLiveSteamAll = function(callback, currentLimit, nextLimit) {

	var isTimeOut = false;

	//http://www.playpark.com/th/function/get_livesteamall.php?location=2&current_limit=5&next_limit=5
	var url = Alloy.Globals.HOST_API + "get_livesteamall.php?location=" + Alloy.Globals.countryCode + "&current_limit=" + currentLimit + "&next_limit=" + nextLimit;

	Ti.API.info("==== PlayParkMobileApi.getLiveSteamAll() ====");
	Ti.API.info("request: " + url);
	Ti.API.info("========");

	var httpClient = Ti.Network.createHTTPClient();
	httpClient.onload = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			var data = eval("(" + this.responseText + ")");
			Ti.API.info("PlayParkMobileApi.getLiveSteamAll data: " + this.responseText.toString());
			callback(data);
		}
	};

	httpClient.onerror = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			callback({
				code : TIME_OUT
			});
			Ti.API.info("PlayParkMobileApi.getLiveSteamAll error: " + e.error);
		}
	};

	httpClient.setTimeout(Alloy.Globals.HTTP_TIME_OUT);
	var timeoutInterval = setInterval(function() {

		if (httpClient.readyState == 4) {
			isTimeOut = true;
			clearInterval(timeoutInterval);
			timeoutInterval = null;
			callback({
				code : TIME_OUT
			});
		}
	}, Alloy.Globals.HTTP_TIME_OUT);

	httpClient.open("POST", url);
	httpClient.send({
		format : "json",
		version : Alloy.Globals.build
	});
};

exports.getGameChannelDetail = function(callback, gameId) {

	var isTimeOut = false;

	//getGameChannelDetail
	var limitL = 10;

	var url = Alloy.Globals.HOST_API + "get_gamechanneldetail.php?location=" + Alloy.Globals.countryCode + "&gameid=" + gameId + "&limit=" + limitL;

	Ti.API.info("==== PlayParkMobileApi.getGameChannelDetail() ====");
	Ti.API.info("request: " + url);
	Ti.API.info("========");

	var httpClient = Ti.Network.createHTTPClient();
	httpClient.onload = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			var data = eval("(" + this.responseText + ")");
			Ti.API.info(" PlayParkMobileApi.getGameChannelDetail data: " + this.responseText.toString());
			callback(data);
		}
	};

	httpClient.onerror = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			callback({
				code : TIME_OUT
			});
			Ti.API.info(" PlayParkMobileApi.getGameChannelDetail error: " + e.error);
		}
	};

	httpClient.setTimeout(Alloy.Globals.HTTP_TIME_OUT);
	var timeoutInterval = setInterval(function() {

		if (httpClient.readyState == 4) {
			isTimeOut = true;
			clearInterval(timeoutInterval);
			timeoutInterval = null;
			callback({
				code : TIME_OUT
			});
		}
	}, Alloy.Globals.HTTP_TIME_OUT);

	httpClient.open("POST", url);
	httpClient.send({
		format : "json",
		version : Alloy.Globals.build
	});
};

exports.getGameApp = function(callback) {

	var isTimeOut = false;

	var url = Alloy.Globals.HOST_API + "get_gameapp.php?location=" + Alloy.Globals.countryCode;

	Ti.API.info("====  PlayParkMobileApi.getGameChannelDetail.getGameApp() ====");
	Ti.API.info("request: " + url);
	Ti.API.info("========");

	var httpClient = Ti.Network.createHTTPClient();
	httpClient.onload = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			var data = eval("(" + this.responseText + ")");
			Ti.API.info("PlayParkMobileApi.getGameApp data: " + this.responseText.toString());
			callback(data);
		}
	};

	httpClient.onerror = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			callback({
				code : TIME_OUT
			});
			Ti.API.info("PlayParkMobileApi.getGameApp error: " + e.error);
		}
	};

	httpClient.setTimeout(Alloy.Globals.HTTP_TIME_OUT);
	var timeoutInterval = setInterval(function() {

		if (httpClient.readyState == 4) {
			isTimeOut = true;
			clearInterval(timeoutInterval);
			timeoutInterval = null;
			callback({
				code : TIME_OUT
			});
		}
	}, Alloy.Globals.HTTP_TIME_OUT);

	httpClient.open("POST", url);
	httpClient.send({
		format : "json",
		version : Alloy.Globals.build
	});
};

exports.getVideoList = function(callback, gameId, from, to) {

	var isTimeOut = false;

	var url;

	if (!to || !from) {
		url = Alloy.Globals.HOST_API + "get_videolist.php?location=" + Alloy.Globals.countryCode + "&limit=" + LIMIT + "&gameid=" + gameId;
	} else {
		url = Alloy.Globals.HOST_API + "get_videolist.php?location=" + Alloy.Globals.countryCode + "&gameid=" + gameId + "&from=" + from + "&to=" + to;
	}

	Ti.API.info("====  PlayParkMobileApi.getVideoList() ====");
	Ti.API.info("request: " + url);
	Ti.API.info("========");

	var httpClient = Ti.Network.createHTTPClient();
	httpClient.onload = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			var data = eval("(" + this.responseText + ")");
			Ti.API.info("PlayParkMobileApi.getGameApp data: " + this.responseText.toString());
			callback(data);
		}
	};

	httpClient.onerror = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			callback({
				code : TIME_OUT
			});
			Ti.API.info("PlayParkMobileApi.getGameApp error: " + e.error);
		}
	};

	httpClient.setTimeout(Alloy.Globals.HTTP_TIME_OUT);
	var timeoutInterval = setInterval(function() {

		if (httpClient.readyState == 4) {
			isTimeOut = true;
			clearInterval(timeoutInterval);
			timeoutInterval = null;
			callback({
				code : TIME_OUT
			});
		}
	}, Alloy.Globals.HTTP_TIME_OUT);

	httpClient.open("POST", url);
	httpClient.send({
		format : "json",
		version : Alloy.Globals.build
	});
};

exports.getOtherVideo = function(callback, from, to) {

	var isTimeOut = false;

	/*
	URL: http://www.playpark.com/th/function/get_videolist.php?location=2&limit=10&type=1
	ขอเพิ่ม
	URL: http://www.playpark.com/th/function/get_videolist.php?location=2&type=1from=0&to=20
	*/
	Ti.API.info('from : ' + from + ", to : " + to);
	var url;
	if (!to || !from) {
		url = Alloy.Globals.HOST_API + "get_videolist.php?location=" + Alloy.Globals.countryCode + "&limit=" + LIMIT + "&type=1";
	} else {
		url = Alloy.Globals.HOST_API + "get_videolist.php?location=" + Alloy.Globals.countryCode + "&type=1&from=" + from + "&to=" + to;
	}

	Ti.API.info("====  PlayParkMobileApi.getOtherVideo() ====");
	Ti.API.info("request: " + url);
	Ti.API.info("========");

	var httpClient = Ti.Network.createHTTPClient();
	httpClient.onload = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			var data = eval("(" + this.responseText + ")");
			Ti.API.info("PlayParkMobileApi.getGameApp data: " + this.responseText.toString());
			callback(data);
		}
	};

	httpClient.onerror = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			callback({
				code : TIME_OUT
			});
			Ti.API.info("PlayParkMobileApi.getGameApp error: " + e.error);
		}
	};

	httpClient.setTimeout(Alloy.Globals.HTTP_TIME_OUT);
	var timeoutInterval = setInterval(function() {

		if (httpClient.readyState == 4) {
			isTimeOut = true;
			clearInterval(timeoutInterval);
			timeoutInterval = null;
			callback({
				code : TIME_OUT
			});
		}
	}, Alloy.Globals.HTTP_TIME_OUT);

	httpClient.open("POST", url);
	httpClient.send({
		format : "json",
		version : Alloy.Globals.build
	});
};

exports.getVideoRelateById = function(callback, id) {

	var isTimeOut = false;

	//http://www.playpark.com/th/function/get_videorelateByid.php?id=110471&limit=10

	var limitT = 10;

	var url = Alloy.Globals.HOST_API + "get_videorelateByid.php?id=" + id + "&limit=" + limitT;

	Ti.API.info("====  PlayParkMobileApi.getVideoRelateById() ====");
	Ti.API.info("request: " + url);
	Ti.API.info("========");

	var httpClient = Ti.Network.createHTTPClient();
	httpClient.onload = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			var data = eval("(" + this.responseText + ")");
			Ti.API.info("PlayParkMobileApi.getVideoRelateById data: " + this.responseText.toString());
			callback(data);
		}
	};

	httpClient.onerror = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			callback({
				code : TIME_OUT
			});
			Ti.API.info("PlayParkMobileApi.getVideoRelateById error: " + e.error);
		}
	};

	httpClient.setTimeout(Alloy.Globals.HTTP_TIME_OUT);
	var timeoutInterval = setInterval(function() {

		if (httpClient.readyState == 4) {
			isTimeOut = true;
			clearInterval(timeoutInterval);
			timeoutInterval = null;
			callback({
				code : TIME_OUT
			});
		}
	}, Alloy.Globals.HTTP_TIME_OUT);

	httpClient.open("POST", url);
	httpClient.send({
		format : "json",
		version : Alloy.Globals.build
	});
};

exports.getGameChannel = function(callback) {

	var isTimeOut = false;

	//URL:http://www.playpark.com/th/function/get_gamechannel.php?location=2

	var url = Alloy.Globals.HOST_API + "get_gamechannel.php?location=" + Alloy.Globals.countryCode;

	Ti.API.info("====  PlayParkMobileApi.getGameChannel() ====");
	Ti.API.info("request: " + url);
	Ti.API.info("========");

	var httpClient = Ti.Network.createHTTPClient();
	httpClient.onload = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			Ti.API.info("PlayParkMobileApi.getGameChannel responseText: " + this.responseText);
			var data = eval("(" + this.responseText + ")");
			Ti.API.info("PlayParkMobileApi.getGameChannel responseText.toString(): " + this.responseText.toString());
			callback(data);
		}
	};

	httpClient.onerror = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			callback({
				code : TIME_OUT
			});
			Ti.API.info("PlayParkMobileApi.getGameChannel error: " + e.error);
		}
	};

	httpClient.setTimeout(Alloy.Globals.HTTP_TIME_OUT);
	var timeoutInterval = setInterval(function() {

		if (httpClient.readyState == 4) {
			isTimeOut = true;
			clearInterval(timeoutInterval);
			timeoutInterval = null;
			callback({
				code : TIME_OUT
			});
		}
	}, Alloy.Globals.HTTP_TIME_OUT);

	httpClient.open("POST", url);
	httpClient.send({
		format : "json",
		version : Alloy.Globals.build
	});
};
////new
exports.getSearchVideoList = function(callback, search, from, to) {

	var isTimeOut = false;

	//var url = Alloy.Globals.HOST_API + "get_searchvideolist.php?location=" + Alloy.Globals.countryCode + "&search=" + search + "&limit=" + limit;
	var url;
	if (!to || !from) {
		url = Alloy.Globals.HOST_API + "get_searchvideolist.php?location=" + Alloy.Globals.countryCode + "&search=" + search + "&from=0&to=" + LIMIT;
	} else {
		url = Alloy.Globals.HOST_API + "get_searchvideolist.php?location=" + Alloy.Globals.countryCode + "&search=" + search + "&from=" + from + "&to=" + to;
	}

	Ti.API.info("====  PlayParkMobileApi.getSearchVideoList() ====");
	Ti.API.info("request: " + url);
	Ti.API.info("========");

	var httpClient = Ti.Network.createHTTPClient();
	httpClient.onload = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			Ti.API.info("PlayParkMobileApi.getSearchVideoList responseText: " + this.responseText);
			var data = eval("(" + this.responseText + ")");
			Ti.API.info("PlayParkMobileApi.getSearchVideoList responseText.toString(): " + this.responseText.toString());
			callback(data);
		}
	};

	httpClient.onerror = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			callback({
				code : TIME_OUT
			});
			Ti.API.info("PlayParkMobileApi.getSearchVideoList error: " + e.error);
		}
	};

	httpClient.setTimeout(Alloy.Globals.HTTP_TIME_OUT);
	var timeoutInterval = setInterval(function() {

		if (httpClient.readyState == 4) {
			isTimeOut = true;
			clearInterval(timeoutInterval);
			timeoutInterval = null;
			callback({
				code : TIME_OUT
			});
		}
	}, Alloy.Globals.HTTP_TIME_OUT);

	httpClient.open("POST", url);
	httpClient.send({
		format : "json",
		version : Alloy.Globals.build
	});
};

exports.getNotification = function(callback, lastTimeArray) {

	var isTimeOut = false;

	var url = Alloy.Globals.HOST_API + "get_notification.php?location=" + Alloy.Globals.countryCode + "&lastTime1=" + lastTimeArray[0] + "&lastTime2=" + lastTimeArray[1] + "&lastTime3=" + lastTimeArray[2] + "&lastTime4=" + lastTimeArray[3] + "&lastTime5=" + lastTimeArray[4];

	/////////////////////////////////////////////////////////////
	Ti.API.info("====  PlayParkMobileApi.getNotification() ====");
	Ti.API.info("request: " + url);
	Ti.API.info("========");
	/////////////////////////////////////////////////////////////

	var httpClient = Ti.Network.createHTTPClient();
	httpClient.onload = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			Ti.API.info("PlayParkMobileApi.getNotification responseText: " + this.responseText);
			var data = eval("(" + this.responseText + ")");
			Ti.API.info("PlayParkMobileApi.getNotification responseText.toString(): " + this.responseText.toString());
			callback(data);
		}
	};

	httpClient.onerror = function(e) {

		if (!isTimeOut) {
			if (timeoutInterval != null) {
				clearInterval(timeoutInterval);
				timeoutInterval = null;
			}
			callback({
				code : TIME_OUT
			});
			Ti.API.info("PlayParkMobileApi.getNotification error: " + e.error);
		}
	};

	httpClient.setTimeout(Alloy.Globals.HTTP_TIME_OUT);
	var timeoutInterval = setInterval(function() {

		if (httpClient.readyState == 4) {
			isTimeOut = true;
			clearInterval(timeoutInterval);
			timeoutInterval = null;
			callback({
				code : TIME_OUT
			});
		}
	}, Alloy.Globals.HTTP_TIME_OUT);

	httpClient.open("POST", url);
	httpClient.send({
		format : "json",
		version : Alloy.Globals.build
	});
};

