/** side menu **/
var ANIMATE_DURATION = 150;

var SIDEMENU_COUNTRY = 0;
var SIDEMENU_VIDEO = 1;
var SIDEMENU_OTHER = 2;

exports.SIDEMENU_COUNTRY = SIDEMENU_COUNTRY;
exports.SIDEMENU_VIDEO = SIDEMENU_VIDEO;
exports.SIDEMENU_OTHER = SIDEMENU_OTHER;

Alloy.Globals.SideMenuButton = SIDEMENU_COUNTRY;

var _isSlideable = true;
var _isTouchRightStarted = false;
var _isButtonPressed = false;
var _hasSlided = false;

var _menuLeftSnapOffsetX = 0;
var _minOffsetXToStartSlide = 10;
var _minOffsetXToUpdateSlide = 5;
var _touchStartX = 0;
var _oldLeft = 0;

var _direction = "reset";
var _animateRight;
var _animateReset;

var _touchView;

var onAnimateFinish = function() {

	//////////////////////Ti.API.info('onAnimateFinish');
	_oldLeft = $.movableview.left;
	$.leftButton.touchEnabled = true;
	_isButtonPressed = false;
};

exports.init = function() {

	// ////////////////////Ti.API.info('slideMenu init');

	_touchView = $.navView;

	if (Alloy.Globals.OS_IOS) {

		if (Alloy.Globals.OS_IOS_IPHONE) {
			// ////////////////////Ti.API.info('iphone');
			Alloy.Globals.menuLeftWidth = Alloy.Globals.SCREEN_WIDTH - 40;
			_menuLeftSnapOffsetX = Alloy.Globals.menuLeftWidth / 2;
		} else {
			// ////////////////////Ti.API.info('ipad');
			Alloy.Globals.menuLeftWidth = 350;
			//Alloy.Globals.SCREEN_WIDTH - 60;
		}

	} else {

		var StringAdapter = require("/utils/StringAdapter");

		Alloy.Globals.menuLeftWidth = Alloy.Globals.SCREEN_WIDTH - StringAdapter.getDimension(Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.sidemenu_margin_right));

		////////////Ti.API.info('Alloy.Globals.SCREEN_WIDTH : ' + Alloy.Globals.SCREEN_WIDTH);

		$.movableview.setWidth(Alloy.Globals.SCREEN_WIDTH);
		//TODO

		////////////////////Ti.API.info('XXXX Alloy.Globals.NAV_HEIGHT: ' + Alloy.Globals.NAV_HEIGHT);

		$.contentview.setTop(Alloy.Globals.NAV_HEIGHT);
		$.navView.setHeight(Alloy.Globals.NAV_HEIGHT);

		var leftButtonSize = Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.sidemenu_left_button_size);
		$.leftButton.setWidth(leftButtonSize);
		$.leftButton.setHeight(leftButtonSize);
		$.leftButton.setLeft(Ti.Android.currentActivity.getString(Ti.App.Android.R.dimen.sidemenu_left_button_margin_left));
	}

	_menuLeftSnapOffsetX = Alloy.Globals.menuLeftWidth / 2;

	//////////////////////Ti.API.info('Alloy.Globals.menuLeftWidth : ' + Alloy.Globals.menuLeftWidth);
	//////////////////////Ti.API.info('_menuLeftSnapOffsetX : ' + _menuLeftSnapOffsetX);
	//////////////////////Ti.API.info('_touchView : ' + _touchView);

	_animateRight = Ti.UI.createAnimation({
		left : Alloy.Globals.menuLeftWidth,
		curve : Ti.UI.ANIMATION_CURVE_EASE_OUT,
		duration : ANIMATE_DURATION
	});

	_animateReset = Ti.UI.createAnimation({
		left : 0,
		curve : Ti.UI.ANIMATION_CURVE_EASE_OUT,
		duration : ANIMATE_DURATION
	});

	_touchView.addEventListener('touchstart', function(e) {

		if (_isSlideable) {

			_touchStartX = e.x;
			//////////////////////Ti.API.info('touchstart ==> _touchStartX: ' + _touchStartX);
		}
	});

	_touchView.addEventListener('touchmove', function(e) {

		if (_isSlideable) {

			var coords = $.movableview.convertPointToView({
				x : e.x,
				y : e.y
			}, $.containerview);

			//////////////////////Ti.API.info('touchmove ==> coords.x : ' + coords.x);

			var newLeft = 0;
			var diffX = 0;

			if (Alloy.Globals.OS_ANDROID) {

				newLeft = (coords.x - _touchStartX) / Alloy.Globals.DENSITY_SCALE;

			} else {
				newLeft = coords.x - _touchStartX;
			}

			diffX = Math.abs(newLeft - _oldLeft);

			if (diffX < _minOffsetXToUpdateSlide) {
				return;
			}

			//////////////////////Ti.API.info('touchmove ==> diffX: ' + diffX + ' ,newLeft: ' + newLeft + ' ,_oldLeft: ' + _oldLeft);

			if (_isTouchRightStarted) {

				if (0 <= newLeft && newLeft <= Alloy.Globals.menuLeftWidth) {

					$.movableview.left = newLeft;
					//////////////////////Ti.API.info('touchmove ==> normal move');

				}
				else if (newLeft < 0) {

					//////////////////////Ti.API.info('touchmove ==> to start');
					$.movableview.left = 0;

				} else if (newLeft > Alloy.Globals.menuLeftWidth) {

					//////////////////////Ti.API.info('touchmove ==> to end');
					$.movableview.left = Alloy.Globals.menuLeftWidth;
				}

				_oldLeft = $.movableview.left;

			} else if (newLeft > _minOffsetXToStartSlide) {

				//////////////////////Ti.API.info('touchmove ==> start move');

				_isTouchRightStarted = true;

				// Ti.App.fireEvent("sliderToggled", {
				// hasSlided : false,
				// direction : "right"
				// });
			}
		}
	});

	_touchView.addEventListener('touchend', function(e) {

		if (_isSlideable) {

			if (_isButtonPressed) {
				//////////////////////Ti.API.info('touchend ==> _isButtonPressed');
				_isButtonPressed = false;
				return;
			}

			//////////////////////Ti.API.info('touchend ==> _isTouchRightStarted: ' + _isTouchRightStarted);

			if (_isTouchRightStarted && $.movableview.left >= _menuLeftSnapOffsetX) {

				//////////////////////Ti.API.info('touchend ==> to end');

				_direction = "right";
				$.leftButton.touchEnabled = false;
				$.movableview.animate(_animateRight, onAnimateFinish);
				_hasSlided = true;
			} else {

				//////////////////////Ti.API.info('touchend ==> to start');

				_direction = "reset";
				$.leftButton.touchEnabled = true;
				$.movableview.animate(_animateReset, onAnimateFinish);
				_hasSlided = false;
			}

			_isTouchRightStarted = false;
		}
	});

	$.leftButton.addEventListener('touchend', function(e) {
		//////////////////////Ti.API.info('leftButton ==> touchend ' + _isTouchRightStarted);
		if (_isSlideable && !_isTouchRightStarted) {
			_isButtonPressed = true;
			$.toggleLeftSlider();
		}
	});

	$.rightButton.addEventListener("click", function(e) {
		//////////////////Ti.API.info('Alloy.Globals.SideMenuButton : ' + Alloy.Globals.SideMenuButton);
		if (Alloy.Globals.SideMenuButton == SIDEMENU_COUNTRY) {
			var settingCountryActivity = Alloy.createController('detail/SettingCountryActivity');
			settingCountryActivity.getView().open();
		} else if (Alloy.Globals.SideMenuButton == SIDEMENU_VIDEO) {
			var videoSearchActivity = Alloy.createController('detail/VideoSearchActivity');
			videoSearchActivity.getView().open();
		}

	});
};

exports.toggleLeftSlider = function() {

	if (!_hasSlided) {

		//////////////////////Ti.API.info('toggleLeftSlider ==> to end');
		_direction = "right";
		$.leftButton.touchEnabled = false;
		$.movableview.animate(_animateRight, onAnimateFinish);
		_hasSlided = true;
	} else {

		//////////////////////Ti.API.info('toggleLeftSlider ==> to start');
		_direction = "reset";
		$.leftButton.touchEnabled = true;
		$.movableview.animate(_animateReset, onAnimateFinish);
		_hasSlided = false;
	}
};

exports.handleRotation = function() {

	if (_isSlideable) {

		$.movableview.width = $.navView.width = $.contentview.width = Ti.Platform.displayCaps.platformWidth;
		$.movableview.height = $.navView.height = $.contentview.height = Ti.Platform.displayCaps.platformHeight;
	}
};

exports.setSlideable = function(bool) {
	_isSlideable = bool;
	if (!_isSlideable) {
		$.leftButton.setVisible(false);
	}
};

exports.isSlidingView = function() {

	return (_isTouchRightStarted || _isButtonPressed) ? true : false;
};
